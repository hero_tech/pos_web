<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'name', 
        'status'
    ];

    public function scopeActive($builder)
    {
        return $builder->where('status',self::ACTIVE);
    }

    public function users()
    {
    	return $this->hasMany(User::class);
    }
}
