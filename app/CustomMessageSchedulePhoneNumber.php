<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomMessageSchedulePhoneNumber extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'custom_message_schedule_id',
        'phone'
    ];

    public function messageSchedule()
    {
        return $this->belongsTo(CustomMessageSchedule::class, 'custom_message_schedule_id', 'id');
    }
    
    public function getRouteKey()
    {
        $hashids = new Hashids('custom-message-schedule-phone-numbers', 15);

        return $hashids->encode($this->getKey());
    }
}
