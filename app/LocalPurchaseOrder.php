<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalPurchaseOrder extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'supplier_id',
        'number',
        'user_id',
        'status',
        'local_purchase_order_status_id'
    ];

    public function scopeActive($builder)
	{
		return $builder->where('status', true);
    }

    public function lpoStatus()
    {
        return $this->belongsTo(LocalPurchaseOrderStatus::class);
    }

	public function user()
	{
		return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(LocalPurchaseOrderItem::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function statusLogs()
    {
        return $this->hasMany(LocalPurchaseOrderApprovalStatus::class);
    }

    public function supplierInvoice()
    {
        return $this->hasOne(SupplierInvoice::class);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('local-purchase-orders-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
