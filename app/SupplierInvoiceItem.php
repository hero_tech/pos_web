<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierInvoiceItem extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'supplier_invoice_id',
        'product_id',
        'quantity',
        'price'
    ];

    public function invoice()
    {
    	return $this->belongsTo(SupplierInvoice::class);
    }

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
