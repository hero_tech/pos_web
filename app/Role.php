<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name','permissions','status'];


    const PERMISSIONS = [
        'Dashboard' => [
            'dashboard-view-all' => true,
            'view-sales-dashboard' => true,
            'view-crm-dashboard' => true,
            'view-analytics-dashboard' => true,
        ],

        'POS' => [
            'view-pos' => true,
            'change-price' => true,
            'offer-discount' => true,
            'cancel-sale' => true,
            'delete-items' => true,
            'hold-sales-transaction' => true,
        ],
        
        'Suppliers Management' => [
            'view-suppliers-management' => true,
            'view-suppliers' => true,
            'create-suppliers' => true,
            'update-suppliers' => true,
            'delete-suppliers' => true,
            'view-suppliers-invoices' => true,
            'view-suppliers-statememts' => true,
        ],

        'Customers Management' => [
            'view-customers-management' => true,
            'view-customers' => true,
            'create-customers' => true,
            'update-customers' => true,
            'delete-customers' => true,
            'view-customers-invoices' => true,
            'view-customers-loyalties' => true,
            'view-customers-statememts' => true,
        ],

        'CRM' => [
            'view-crm' => true,
            'view-inquiries' => true,
            'create-inquiries' => true,
            'update-inquiries' => true,
            'delete-inquiries' => true,
            'view-inquiries-types' => true,
            'create-inquiries-types' => true,
            'update-inquiries-types' => true,
            'delete-inquiries-types' => true,
            'view-inquiries-status' => true,
            'create-inquiries-status' => true,
            'update-inquiries-status' => true,
            'delete-inquiries-status' => true,
        ],        
        
        'Store Management' => [
            'view-store' => true,
            'view-products' => true,
            'register-products' => true,
            'update-products' => true,
            'delete-products' => true,
            'receive-products' => true,
            'view-products-categories' => true,
            'create-products-categories' => true,
            'update-products-categories' => true,
            'delete-products-categories' => true,
            'view-unit-of-measure' => true,
            'create-unit-of-measure' => true,
            'update-unit-of-measure' => true,
            'delete-unit-of-measure' => true,
        ],
        
        'Bulk SMS' => [
            'view-bulk-sms' => true,
            'send-messages' => true,
            'schedule-messages' => true,
            'view-message-templates' => true,
            'create-message-templates' => true,
            'update-message-templates' => true,
            'delete-message-templates' => true,
            'view-message-groups' => true,
            'create-message-groups' => true,
            'update-message-groups' => true,
            'delete-message-groups' => true,
        ],


        'Procurement' => [
            'view-procurement' => true,
            'view-lpos' => true,
            'create-lpos' => true,
            'update-lpos' => true,
            'delete-lpos' => true,
            'view-srns' => true,
            'create-srns' => true,
            'update-srns' => true,
            'delete-srns' => true,
            'view-selling-prices' => true,
            'update-selling-prices' => true,
        ],

        'Accounts' => [
            'view-accounts' => true,
            'view-invoices' => true,
            'create-invoices' => true,
            'update-invoices' => true,
            'delete-invoices' => true,

            'view-petty-cash' => true,
            'issue-petty-cash' => true,

            'view-creditors' => true,
            'view-deptors' => true,
            'view-cash-flow-statement' => true,

            'view-charts-of-account' => true,
            'create-charts-of-account' => true,
            'update-charts-of-account' => true,
            'delete-charts-of-account' => true,

            'view-bank-statement' => true,
            'view-bank-reconcilation' => true,
            'view-balance-sheet' => true,
            'view-income-statement' => true,
        ],

        'Settings' => [
            'view-settings' => true,

            'view-users' => true,
            'create-users' => true,
            'update-users' => true,
            'delete-users' => true,

            'view-roles' => true,
            'create-roles' => true,
            'update-roles' => true,
            'delete-roles' => true,

            'view-departments' => true,
            'create-department' => true,
            'update-department' => true,
            'delete-department' => true,

            'view-system-backup' => true,
            'create-system-backup' => true,
            'update-system-backup' => true,
            'delete-system-backup' => true,
            
        ]
    ];

    protected $casts = [
        'permissions' => 'array',
    ];

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'role_user',
            'role_id',
            'user_id'
        );
    }

    public function hasAccess(array $permissions) : bool
    {
        foreach ($permissions as $permission) {
            if ($this->hasPermission($permission))
                return true;
        }
        return false;
    }

    private function hasPermission(string $permission) : bool
    {
        return $this->permissions[$permission] ?? false;
    }

    
    public function scopeActive($builder)
    {
        return $builder->where('status', true);
    }
}
