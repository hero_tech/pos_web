<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name', 
        'email', 
        'username', 
        'mobile', 
        'password', 
        'department_id',
        'supervisor_id',
        'status',
        'job_title',
        'avatar_url', 
        'avatar_img',
        'signature_url',
        'key_code',
        'signature'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * [getFullNameAttribute description]
     * @return [type] [string]
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
    
    
    public function getAvatarAttribute()
    {
        return "{$this->avatar_url}{$this->avatar_img}";
    }
   
    public function getUserSignatureAttribute()
    {
        return "{$this->signature_url}/{$this->signature}";
    }

    public function scopeActive($builder)
    {
        return $builder->where('status', true);
    }
    /**
     * [roles description]
     * @return [type] [description]
     */
    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'role_user',
            'user_id',
            'role_id'
        );
    }

    /**
     * Checks if User has access to $permissions.
     */
    public function hasAccess(array $permissions) : bool
    {
        // check if the permission is available in any role
        foreach ($this->roles as $role) {
            if($role->hasAccess($permissions)) {
                return true;
            }
        }
        return false;
    }
    
    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

    public function supervisor()
    {     
        return $this->belongsTo(User::class, 'supervisor_id', 'id');   
    }

    public function scopeHasSignature($builder)
    {
        $check = $builder->where('id', auth()->user()->id)->pluck('signature')->first();
        
        if($check != null) return 1; 

        return 0;
    }
}
