<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $cast = [
        'status' => 'boolean',
    ];

    protected $fillable = [
        'id_number',
        'code',
        'first_name',
        'last_name',
        'email',
        'phone',
        'status'
    ];

    
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
    
    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('customers', 15);

        return $hashids->encode($this->getKey());
    }
}
