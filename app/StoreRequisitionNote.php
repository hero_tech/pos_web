<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreRequisitionNote extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'number',
        'user_id',
        'status',
        'store_requisition_note_status_id'
    ];

    public function scopeActive($builder)
	{
		return $builder->where('status', true);
    }

    public function srnStatus()
    {
        return $this->belongsTo(StoreRequisitionNoteStatus::class, 'store_requisition_note_status_id', 'id');
    }

	public function user()
	{
		return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(StoreRequisitionNoteItem::class);
    }


    public function statusLogs()
    {
        return $this->hasMany(StoreRequisitionNoteApproval::class);
    }

    public function grn()
    {
        return $this->hasOne(GoodIssueNote::class);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('store-requisition-notes-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
