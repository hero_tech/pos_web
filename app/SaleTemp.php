<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleTemp extends Model
{
    
    protected $fillable = [
        'temp_sales_number',
        'product_id',
        'customer_id',
        'product_quantity',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
