<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountType extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'financial_statement_id',
        'code',
        'name',
        'status'
    ];

    public function statement()
    {
        return $this->belongsTo(FinancialStatement::class, 'financial_statement_id', 'id');
    }

    public function types()
    {
        return $this->hasMany(AccountSubType::class);
    }
}
