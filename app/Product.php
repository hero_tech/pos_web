<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;


    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean',
        'vatable' => 'boolean',
        'exempted' => 'boolean'
    ];

    protected $fillable = [
        'product_category_id',
        'unit_of_measure_id',
        'user_id',
        'name',
        'code',
        'picture',
        'cost',
        'discount_allowed',
        'quantity',
        'sales_quantity',
        'vatable',
        'exempted',
        'notify',
        'status',
    ];

    public function scopeActive($builder)
	{
		return $builder->where('status', true);
    }
    
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function category()
	{
		return $this->belongsTo(ProductCategory::class, 'product_category_id', 'id');
	}

	public function unitOfMeasure()
	{
		return $this->belongsTo(UnitOfMeasure::class, 'unit_of_measure_id', 'id');
    }
    
    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('products', 15);

        return $hashids->encode($this->getKey());
    }

}
