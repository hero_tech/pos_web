<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskFrequencyDay extends Model
{    
    protected $fillable = ['name'];
}
