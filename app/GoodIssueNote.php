<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodIssueNote extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'consent' => 'boolean'
    ];

    protected $fillable = [
        'store_requisition_note_id',
        'user_id',
        'consent',
        'comment'
    ];

    public function srn()
    {
        return $this->belongsTo(StoreRequisitionNote::class, 'store_requisition_note_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /** Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('good-issue-notes-salt', 15);

        return $hashids->encode($this->getKey());
    }
}
