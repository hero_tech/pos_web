<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $fillable = [
        'name', 
        'description',
        'image_url',
        'image',
        'user_id',
        'status', 
    ];

    public function scopeActive($builder)
	{
		return $builder->where('status', true);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
