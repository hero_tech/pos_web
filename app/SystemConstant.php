<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemConstant extends Model
{
    use SoftDeletes;

    const VAT = 'VAT';
    const CLP = 'CLP'; // Customer loyalty points
    const CGVEXP = 'CGVEXP'; // Customer Gift Voucher Expires at

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'code',
        'name',
        'value',
        'status'
    ];

    public function scopeActive($builder)
	{
		return $builder->where('status', true);
    }

    public function scopeVat($builder)
    {
        return $builder->where('code', self::VAT)->pluck('value')->first();
    }

    public function scopeCLP($builder)
    {
        return $builder->where('code', self::CLP)->pluck('value')->first();
    }

    public function scopeCGVEXP($builder)
    {
        return $builder->where('code', self::CGVEXP)->pluck('value')->first();
    }

}
