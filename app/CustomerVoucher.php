<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerVoucher extends Model
{
    use SoftDeletes;
    
    protected $dates = [
        'deleted_at',
        'expires_at'
    ];

    protected $fillable = [
        'user_id',
        'code',
        'customer_name',
        'customer_phone',
        'purchased_by',
        'voucher_amount',
        'voucher_balance',
        'expires_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
