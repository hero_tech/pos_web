<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalPurchaseOrderApprovalStatus extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];
    
    protected $fillable = [
        'local_purchase_order_id',
        'local_purchase_order_status_id',
        'local_purchase_order_approver_id',
        'description',
        'comment'
    ];

    public function lpo()
    {
        return $this->belongsTo(LocalPurchaseOrder::class, 'local_purchase_order_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(LocalPurchaseOrderStatus::class, 'local_purchase_order_status_id', 'id');
    }

    public function approver()
    {
        return $this->belongsTo(LocalPurchaseOrderApprover::class, 'local_purchase_order_approver_id', 'user_id');
    }
}
