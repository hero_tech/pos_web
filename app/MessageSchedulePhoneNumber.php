<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageSchedulePhoneNumber extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'message_scheduling_id',
        'phone'
    ];

    public function messageSchedule()
    {
        return $this->belongsTo(MessageScheduling::class, 'message_scheduling_id', 'id');
    }
    
    public function getRouteKey()
    {
        $hashids = new Hashids('message-scheduling-phone-numbers', 15);

        return $hashids->encode($this->getKey());
    }
}
