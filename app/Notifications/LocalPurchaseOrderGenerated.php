<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\LocalPurchaseOrder;

class LocalPurchaseOrderGenerated extends Notification implements ShouldQueue
{
    use Queueable;

    public $lpo;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(LocalPurchaseOrder $lpo)
    {
        $this->lpo = $lpo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $supplier = $this->lpo->supplier->company;

        $subject = sprintf('You\'ve got a new LPO from '.env('APP_NAME'). '('.$this->lpo->number.')');
        $greeting = sprintf('Hello '.$supplier);
        
        return (new MailMessage)
                ->subject($subject)
                ->greeting($greeting)
                ->salutation('Yours Faithfully')
                ->line('The introduction to the notification.')
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
