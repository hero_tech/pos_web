<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerInvoice extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'customer_id',
        'invoice_status_id',
        'invoice_no',
        'amount',
        'balance',
        'tax',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function status()
    {
        return $this->belongsTo(InvoiceStatus::class, 'invoice_status_id', 'id');
    }

    public function items()
    {
        return $this->hasMany(CustomerInvoiceItem::class);
    }

    public function getRouteKey()
    {
        $hashids = new Hashids('customer-invoices-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
