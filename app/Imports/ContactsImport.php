<?php

namespace App\Imports;

use Log;
use App\Contact;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContactsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Contact([
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'phone' => '0'.$row['phone_number'],
            'favourite' => $row['favourite'],
        ]);
    }

}
