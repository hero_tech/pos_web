<?php

use App\SystemConstant;
use App\LocalPurchaseOrder;
use App\LocalPurchaseOrderStatus;
use App\LocalPurchaseOrderApprovalStatus;

function getLpoVat($id){
    $lpo = LocalPurchaseOrder::find($id);
    $tax = SystemConstant::vat();

    $vat = 0;
    foreach($lpo->items as $item){
        if($item->product->vatable){
            $vat += $item->quantity * $item->unit_price * ($tax / 100);
        }
    }

    return $vat;
}

function getLpoSubTotal($id){
    $lpo = LocalPurchaseOrder::find($id);
    
    $subtotal = 0;
    foreach($lpo->items as $item){
        $subtotal += ($item->unit_price * $item->quantity);
    }
    
    return $subtotal;
}

function checkUserIfCurrentLpoApprover($lpo, $user) {
    $check = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
            ->where('local_purchase_order_approver_id', $user)
            ->where('local_purchase_order_status_id', 1)
            ->get();
    
    if(count($check) == null){
        return false;  
    } else{
        return true;
    }
}

function canEditLPO($lpo)
{
    $check = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
            ->where('local_purchase_order_status_id', LocalPurchaseOrderStatus::APPROVED)
            ->get();

    if(count($check) < 1){
        return true;  
    } else{
        return false;
    }
}

function lastApprover($user, $lpo)
{
    $check = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
        ->get();

    $last = $check->last()->local_purchase_order_approver_id;

    // dd($user);

    if($last == $user){
        return 1;
    }else{
        return 0;
    }
}

