<?php

use App\AccountSubType;
use App\SystemConstant;
use App\CustomerInvoice;
use App\SupplierInvoice;

function getTotalAccountBalance($account)
{
    $balance = AccountSubType::where('account_type_id', $account)->sum('balance');

    return $balance;
}

function getVat(){
    $tax = SystemConstant::vat();
    return $tax;
}

function getInvoiceVat($id){
    $invoice = SupplierInvoice::find($id);
    $tax = SystemConstant::vat();

    $vat = 0;
    foreach($invoice->items as $item){
        if($item->product->vatable){
            $vat += $item->quantity * $item->price * ($tax / 100);
        }
    }

    return $vat;
}

function getInvoiceSubTotal($id){
    $invoice = SupplierInvoice::find($id);

    $subtotal = 0;
    foreach($invoice->items as $item){
        $subtotal += ($item->price * $item->quantity);
    }

    return $subtotal;
}

/**
 * getCustomerInvoiceSubTotal
 *
 * @param  mixed $id
 * @return void
 */
//function getCustomerInvoiceSubTotal($id){
//    $invoice = CustomerInvoice::find($id);
//
//    return array_sum(array_map(fn($item) => ($item['price'] * $item['quantity']), $invoice->items->toArray()));
//}


/**
 * getCustomerInvoiceDiscount
 *
 * @param  mixed $id
 * @return void
 */
//function getCustomerInvoiceDiscount($id){
//    $invoice = CustomerInvoice::find($id);
//
//    return array_sum(array_map(fn($item) => ($item['price'] * ($item['discount'] / 100)), $invoice->items->toArray()));
//}

/**
 * getCustomerInvoiceVat
 *
 * @param  mixed $id
 * @return void
 */
//function getCustomerInvoiceVat($id){
//    $invoice = CustomerInvoice::find($id);
//    $tax = $invoice->tax;
//
//    return array_sum(array_map(fn($item) => ($item['price'] * ($tax / 100)), $invoice->items->toArray()));
//}



