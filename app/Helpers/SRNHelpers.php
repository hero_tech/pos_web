<?php

use App\Product;
use App\StoreRequisitionNoteStatus;
use App\StoreRequisitionNoteApproval;

/**
 * srnSupermarketQuantity
 *
 * @param  mixed $product
 * @return void
 */
function srnSupermarketQuantity($product)
{
    $check_quantity = Product::find($product);

    $quantity = $check_quantity['sales_quantity'];
    
    return $quantity;
}

/**
 * srnStoreQuantity
 *
 * @param  mixed $product
 * @return void
 */
function srnStoreQuantity($product)
{    
    $check_quantity = Product::find($product);

    $quantity = $check_quantity['quantity'];    
    
    return $quantity;
}

function canEditSRN($srn)
{
    $check = StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)
            ->where('store_requisition_note_status_id', StoreRequisitionNoteStatus::APPROVED)
            ->get();

    if(count($check) < 1){
        return true;  
    } else{
        return false;
    }
}