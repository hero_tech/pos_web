<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomMessageSchedule extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'name', 
        'message',
        'message_date', 
        'message_time', 
        'status'
    ];

    public function phoneNumbers()
    {
        return $this->hasMany(CustomMessageSchedulePhoneNumber::class);
    }

    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class, 
            'contact_custom_message_schedule', 
            'custom_message_schedule_id', 
            'contact_id'
        )->withTimestamps();
    }

    public function contactGroups()
    {
        return $this->belongsToMany(
            ContactGroup::class, 
            'contact_group_custom_message_schedule', 
            'custom_message_schedule_id', 
            'contact_group_id'
        )->withTimestamps();
    }

    public function scopeActive($builder)
    {
        return $builder->where('status', true);
    }

    public function getRouteKey()
    {
        $hashids = new Hashids('custom-message-schedules-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
