<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InquiryType extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'status'
    ];

    /**
     * [scopeActive description]
     * @param  [type] $builder [description]
     * @return [type]          [description]
     */
    public function scopeActive($builder)
    {
    	return $builder->where('status', true);
    }

}
