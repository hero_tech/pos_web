<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialStatement extends Model
{
    protected $fillable = [
        'name'
    ];

    public function accounts()
    {
        return $this->hasMany(AccountType::class);
    }
}
