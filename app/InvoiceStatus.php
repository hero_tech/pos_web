<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceStatus extends Model
{
    use SoftDeletes;

    const PENDING = 1;
    const PARTIALLY_PAID = 2;
    const FULLY_PAID = 3;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'status'
    ];

}
