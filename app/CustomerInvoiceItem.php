<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerInvoiceItem extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'customer_invoice_id',
        'product_id',
        'quantity',
        'discount',
        'price'
    ];

    public function invoice()
    {
    	return $this->belongsTo(CustomerInvoice::class);
    }

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
