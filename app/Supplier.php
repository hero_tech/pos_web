<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;


class Supplier extends Model
{
    use SoftDeletes, Notifiable;

    protected $dates = [
		'deleted_at'
	];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'company',
        'description',
        'phone',
        'email',
        'location', 
        'address', 
        'status',
        'user_id',
    ];

    public function scopeActive($builder)
	{
		return $builder->where('status', true);
	}


	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function people()
    {
        return $this->belongsToMany(
            SupplierContactPerson::class, 
            'supplier_suppliercontactperson', 
            'supplier_id',
            'supplier_contact_person_id'
        );
    }

    public function lpos()
    {
        return $this->hasMany(LocalPurchaseOrder::class);
    }

    public function invoices()
    {
        return $this->hasMany(SupplierInvoice::class);
    }

}
