<?php

namespace App\Console\Commands;

use Log;
use App\ContactGroup;
use App\MessageScheduling;
use Illuminate\Console\Command;
use App\Http\Controllers\MessageController;

class SendFixedScheduleMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message_schedule:send_fixed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send messages that have a fixed schedule';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $schedules = MessageScheduling::active()->with(['phoneNumbers', 'contacts', 'contactGroups'])->get();

        if(count($schedules) > 0){
           foreach($schedules as $schedule){

               $phone_numbers = $schedule->phoneNumbers;
               $contacts = $schedule->contacts;
               $contactGroups = $schedule->contactGroups;

               $message = $schedule->message;

                $sendMessage = null;

                //check if phone numbers are available
                if($phone_numbers != null){                    
                    foreach ($phone_numbers as $phone) {                               
                        $sendMessage = MessageController::send($id = null,  $phone['phone'], $message);                        
                    }
                }
                
                // check if contact are available
                if($contacts != null) {
                    foreach ($contacts as $contact) {
                        $sendMessage = MessageController::send($contact['id'], $contact['phone'], $message);
                    }
                }

                //Check if groups are available
                if($contactGroups != null) {
                    foreach ($contactGroups as $contactGroup) {
                        $members = ContactGroup::find($contactGroup->id)->contacts->toArray();

                        foreach ($members as $member) {
                            $sendMessage = MessageController::sendGroupMessage($contactGroup->id, $message, $member['phone']);
                        }
                    }
                }

           }
        }
    }
}
