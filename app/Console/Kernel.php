<?php

namespace App\Console;

use Log;
use App\MessageScheduling;
use App\CustomMessageSchedule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $fixed_message_schedules = MessageScheduling::active()->with(['frequency', 'frequencyDay'])->get();
        $custom_message_schedules = CustomMessageSchedule::active()->get();

        
        if(count($fixed_message_schedules) > 0){
            foreach($fixed_message_schedules as $message_schedule){
                $frequency = $message_schedule->frequency->name;
                $frequencyDay = $message_schedule->frequencyDay->name;
                $time = date('H:i', strtotime($message_schedule->message_time));

                $schedule->command('message_schedule:send_fixed')
                    ->$frequency()
                    ->$frequencyDay()
                    ->at($time)
                    ->withoutOverlapping()
                    ->runInBackground();
            }
        }

        if (count($custom_message_schedules) > 0) {            
            foreach($custom_message_schedules as $message_schedule){
                $minute = intval(date('i', strtotime($message_schedule->message_time)));
                $hour = date('G', strtotime($message_schedule->message_time));
                $day = date('j', strtotime($message_schedule->message_date));
                $month = date('n', strtotime($message_schedule->message_date));

                $schedule->command('message_schedule:send_custom')
                    ->cron(''.$minute.' '.$hour.' '.$day.' '.$month.' *')
                    ->withoutOverlapping()
                    ->runInBackground();
            }
        }

        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
