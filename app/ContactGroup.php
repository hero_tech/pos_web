<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactGroup extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean',
        'favourite' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'description',
        'favourite',
        'status'
    ];

    public function scopeActive($builder)
    {
        return $builder->where('status', true);
    }

    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class, 
            'contact_contactgroup', 
            'contact_group_id', 
            'contact_id'
        )->withTimestamps();
    }

    public function messageSchedules()
    {
        return $this->belongsToMany(
            MessageScheduling::class, 
            'contact_group_message_scheduling', 
            'contact_group_id',
            'message_scheduling_id'
        )->withTimestamps();
    }
    
}
