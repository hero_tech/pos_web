<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalPurchaseOrderStatus extends Model
{
    use SoftDeletes;

    const PENDING = 1;
    const CANCELED = 2;
    const REJECTED = 3;
    const APPROVED = 4;


    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'name',
        'status'
    ];

    public function scopeActive($builder)
	{
		return $builder->where('status', true);
    }

    public function lpos()
    {
        return $this->hasMany(LocalPurchaseOrder::class);
    }

}
