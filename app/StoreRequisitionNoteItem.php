<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreRequisitionNoteItem extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'store_requisition_note_id',
        'product_id',
        'quantity',
    ];

    public function lpo()
    {
        return $this->belongsTo(StoreRequisitionNote::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

     /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('store-requisition-note-items-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
