<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JournalEntry extends Model
{
    protected $dates = ['transaction_date'];
    
    protected $fillable = [
        'transaction_date',
        'transaction_desc',
        'folio',
        'debit',
        'credit'
    ];
}
