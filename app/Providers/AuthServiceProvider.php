<?php

namespace App\Providers;

use App\Role;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //Register permissions 
        $this->registerPermissions();
    }

    public function registerPermissions()
    {
        $permissions = Role::PERMISSIONS;


        foreach ($permissions as  $permission) {

            foreach ($permission as $key => $value) {
                $role = $key;
                $access = [$key, $value];
                Gate::define($role, function ($user) use ($access) {
                    return $user->hasAccess($access);
                });
            }
            
        }

    }
}
