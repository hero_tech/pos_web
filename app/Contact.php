<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean',
        'favourite' => 'boolean'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'favourite',
        'phone',
        'status'
    ];
    
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function scopeActive($builder)
    {
        return $builder->where('status', true);
    }

    public function groups()
    {
        return $this->belongsToMany(
            ContactGroup::class, 
            'contact_contactgroup', 
            'contact_id',
            'contact_group_id'
        )->withTimestamps();
    }

    public function messageSchedules()
    {
        return $this->belongsToMany(
            MessageScheduling::class, 
            'contact_message_scheduling', 
            'contact_id',
            'message_scheduling_id'
        )->withTimestamps();
    }
}
