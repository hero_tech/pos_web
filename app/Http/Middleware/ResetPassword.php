<?php

namespace App\Http\Middleware;

use Closure;

class ResetPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->reset_password == false){
            return redirect()->route('reset-account-password');
        }

        return $next($request);
    }
}
