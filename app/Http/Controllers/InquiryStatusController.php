<?php

namespace App\Http\Controllers;

use App\InquiryStatus;
use Illuminate\Http\Request;

class InquiryStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inquiryStatuses = InquiryStatus::all();

        return view('inquiry-statuses.index', [
            'inquiryStatuses' => $inquiryStatuses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InquiryStatus  $inquiryStatus
     * @return \Illuminate\Http\Response
     */
    public function show(InquiryStatus $inquiryStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InquiryStatus  $inquiryStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(InquiryStatus $inquiryStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InquiryStatus  $inquiryStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InquiryStatus $inquiryStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InquiryStatus  $inquiryStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(InquiryStatus $inquiryStatus)
    {
        //
    }
}
