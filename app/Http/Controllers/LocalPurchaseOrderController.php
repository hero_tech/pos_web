<?php

namespace App\Http\Controllers;

use Log;
use PDF;
use App\User;
use App\Product;
use App\Supplier;
use Hashids\Hashids;
use App\SystemConstant;
use App\LocalPurchaseOrder;
use Illuminate\Http\Request;
use App\LocalPurchaseOrderItem;
use App\LocalPurchaseOrderStatus;
use App\LocalPurchaseOrderApprover;
use App\LocalPurchaseOrderApprovalStatus;
use App\Notifications\LocalPurchaseOrderGenerated;

class LocalPurchaseOrderController extends Controller
{    
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('local-purchase-orders-salt', 15);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $lpos = LocalPurchaseOrder::with(['user', 'items', 'supplier', 'statusLogs'])->get();
        $statuses = LocalPurchaseOrderStatus::get();
        $users = User::active()->get();
        
        return view('lpo.index', [
            'lpos' => $lpos,
            'statuses' => $statuses,
            'users' => $users,
        ]);
    }
    
    /**
     * getData
     *
     * @return void
     */
    public function getData()
    {
        
        $products = Product::active()->select('id', 'name')->latest()->get();
        $suppliers = Supplier::active()->select('id', 'company')->latest()->get();
        $statuses = LocalPurchaseOrderStatus::where('id', 1)->select('id', 'name')->get();
        $hasSignature = User::hasSignature();
       
        return response()->json([
            'data' => [
                'products' =>$products,
                'suppliers' => $suppliers,
                'statuses' => $statuses,
                'hasSignature' => $hasSignature
            ]
        ]);
    }

    
    /**
     * getLpoData
     *
     * @param  mixed $id
     * @return void
     */
    public function getLpoData($id)
    {     
        // Return products not in lpo items
        $lpo = LocalPurchaseOrder::find($id);
        $items = collect($lpo->items)->toArray();

        $ids = array_map(fn($item) => $item['product_id'], $items);

        $products = Product::active()->select('id', 'name')
            ->whereNotIn('id', $ids)
            ->latest()
            ->get();

        return response()->json([
            'data' => [
                'products' => $products
            ]
        ]);

    }
        
    /**
     * statusLogs
     *
     * @param  mixed $lpo
     * @return void
     */
    public function statusLogs($id)
    {
        $lpoStatus = LocalPurchaseOrder::where('id', $id)->first();
        $status = $lpoStatus->local_purchase_order_status_id;

        $statusLogs = LocalPurchaseOrderApprovalStatus::with([
                'status', 
                'approver', 
                'approver.user'
            ])->where('local_purchase_order_id', $id)->get();
        
        $firstApprover = LocalPurchaseOrderApproverController::checkLpoFirstApprover($id);
        // check if the previous approver has approve the LPO
        $isApproved = LocalPurchaseOrderApproverController::checkIfPreviousApproverHasApproved($id);
        
        $currentUserData = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $id)
            ->where('local_purchase_order_approver_id', auth()->user()->id)
            ->where('local_purchase_order_status_id', LocalPurchaseOrderStatus::PENDING)
            ->get();
        
        $currentApprover = null;


        
        if($firstApprover == 0 && count($currentUserData) > 0){
            $currentApprover = 1;
        }
        
        if( $isApproved == 0 && count($currentUserData) == 1){
            // Log::info($isApproved);
            $currentApprover = 1;
        }else{
            $currentApprover = 0;
        }  
        // dd();
        

        $user = LocalPurchaseOrder::find($id)->first();
        $originator = null;
        if($user->user_id == auth()->user()->id){
            $originator = 1;
        }else{
            $originator = 0;
        }
        
        $hasSignature = User::hasSignature();
        $nextApprover = LocalPurchaseOrderApproverController::checkLpoLastApprover($id);

        return response()->json([
            'data' => [
                'statusLogs' => $statusLogs,
                'approver' => $currentApprover,
                'status' => $status,
                'next' => $nextApprover,
                'originator' => $originator,
                'hasSignature' => $hasSignature
            ]
        ]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lpo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $lpo_number = $this->generateLPONumber();
        $supplier_id = $data['supplier_id'];
        $status_id = $data['status_id'];
        $lpo_products = $data['lpo_products'];

        $products = collect();
        $allProducts = array();
        
        foreach ($lpo_products as $product) {
            $products->put('product_id', $product['id']);
            $products->put('quantity', $product['quantity']);
            $products->put('unit_price', $product['price']);

            array_push($allProducts, $products->toArray());
        }

        
        $lpo = LocalPurchaseOrder::create([
            'supplier_id' => $supplier_id,
            'local_purchase_order_status_id' => $status_id,
            'number' => $lpo_number,
            'user_id' => auth()->user()->id
        ]);
        
        $approvers = LocalPurchaseOrderApprover::all();
        $description = null;
        foreach($approvers as $approver){
            if($approver->sequence == 1){
                $description = 'Pending Approval';
            }else{
                $description = 'Created';
            }

            LocalPurchaseOrderApprovalStatus::create([
                'local_purchase_order_id' => $lpo->id,
                'local_purchase_order_status_id' => $status_id,
                'local_purchase_order_approver_id' => $approver->user_id,
                'description' => $description,
            ]);
            
        }
        
        foreach($allProducts as $product){
            LocalPurchaseOrderItem::create([
                'local_purchase_order_id' => $lpo->id,
                'product_id' => $product['product_id'],
                'quantity' => $product['quantity'],
                'unit_price' => $product['unit_price'],
            ]);
        }
        
        return response()->json(['data' =>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lpo_id = $this->hashids->decode($id)[0];
        
        $products = Product::active()->get();
        $lpo = LocalPurchaseOrder::with([
                'lpoStatus',
                'user',
                'items',
                'supplier',
                'statusLogs'
            ])->find($lpo_id);

        return view('lpo.show', [
            'lpo' => $lpo,
            'products' => $products
        ]);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $lpo_id = $this->hashids->decode($id)[0];

        dd($lpo_id);
    }

    //generate lpo number
    public function generateLPONumber(){
        $lpo_number = '';
        $last_number = LocalPurchaseOrder::pluck('number')->last();
        if ($last_number != null) {
            $lpo_number = substr($last_number, -5);
            $new_number = $lpo_number + 1;
            if (strlen($new_number) == 1) {
                $lpo_number = "LPO0000".$new_number;
                return $lpo_number;
            }elseif (strlen($new_number) == 2) {
                $lpo_number = "LPO000".$new_number;
                return $lpo_number;
            }elseif (strlen($new_number) == 3) {
                $lpo_number = "LPO00".$new_number;
                return $lpo_number;
            }elseif (strlen($new_number) == 4) {
                $lpo_number = "LPO0".$new_number;
                return $lpo_number;
            }elseif (strlen($new_number) == 5) {
                $lpo_number = "LPO".$new_number;
                return $lpo_number;
            }
        }else{
            $lpo_number =  "LPO00001";
            return $lpo_number;
        }
    }

    public function downloadLPO($lpo)
    {        
        $id = $this->hashids->decode($lpo)[0];
        
        $data = LocalPurchaseOrder::find($id);
        $tax = SystemConstant::vat();

        $vat = 0;
        $subtotal = 0;
        foreach($data->items as $item){
            if($item->product->vatable){
                $vat += $item->quantity * $item->unit_price * ($tax / 100);
            }
            $subtotal += ($item->unit_price * $item->quantity);
        }
        
        $prepared_by = User::find(auth()->user()->id)->first();

        $approvers = $data->statusLogs;

        $pdf = PDF::loadView('lpo.lpopdf', compact(
            'data',
            'prepared_by',
            'vat', 
            'subtotal',
            'approvers'
        ));

        return $pdf->stream();
        // return $pdf->download('customers.pdf');
    }
}
