<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Message;
use App\ContactGroup;
use Illuminate\Http\Request;
use DB;
use Log;

class SendMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messagesBalance = MessageController::getBalance();
        $messagesSent = Message::count();
        $totalContacts = Contact::count();
        $totalGroups = ContactGroup::count();

        $messageAnalysis = Message::select(DB::raw('count(id)as value, date(created_at) as date'))
                ->groupBy('date')
                ->get()->toArray();

        return view('send-message.index', [
            'messagesBalance' => $messagesBalance,
            'messagesSent' => $messagesSent,
            'totalContacts' => $totalContacts,
            'totalGroups'  => $totalGroups,
            'messageAnalysis' => $messageAnalysis
        ]);
    }

        
    /**
     * getContactsGroups
     *
     * @return void
     */
    public function getContactsGroups()
    {
        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))->latest()->get();
        $groups = ContactGroup::active()->has('contacts')->select('id', 'name')->latest()->get();
        
        return response()->json([
            'data' => [
                'contacts' => $contacts,
                'groups' => $groups
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messageData = $request->all();
        $contacts = $messageData['contacts'];
        $groups = $messageData['groups'];
        $message = $messageData['message'];
        $phone_numbers = $messageData['phone_numbers'];
        

        $sendMessage = null;

        //check if phone numbers array is present
        if($phone_numbers != null){
            $phone_numbers_array = explode(",", $phone_numbers);
            
            foreach ($phone_numbers_array as $value) {
                
                $phone = trim(preg_replace('/\s+/', ' ', $value));

                if(strlen($phone) != 10){
                    return response()->json(['data' => 'error']);
                }else{         
                    $sendMessage = MessageController::send($id = null,  $phone, $message);
                }
            }
        }
        
        // check if contact are provided
        if($contacts != null) {
            $contact_id = array_map(fn($contact) => $contact['id'], $contacts);
            $people = Contact::whereIn('id', $contact_id)->get()->toArray();

            foreach ($people as $person) {
                $sendMessage = MessageController::send($person['id'], $person['phone'], $message);
            }
        }

        //Check id groups are provided
        if($groups != null) {            
            $group_id = array_map(fn($group) => $group['id'], $groups);

            foreach ($group_id as $groupid) {
                $members = ContactGroup::find($groupid)->contacts->toArray();

                foreach ($members as $member) {
                    $sendMessage = MessageController::sendGroupMessage($groupid, $message, $member['phone']);
                }
            }
        }

        if($sendMessage){
            return response()->json(['data' => 'success']);
        }else{
            return response()->json(['data' => 'error']);
        }
    }

}
