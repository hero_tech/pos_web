<?php

namespace App\Http\Controllers;

use App\StoreRequisitionNoteApproval;
use Illuminate\Http\Request;

class StoreRequisitionNoteApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StoreRequisitionNoteApproval  $storeRequisitionNoteApproval
     * @return \Illuminate\Http\Response
     */
    public function show(StoreRequisitionNoteApproval $storeRequisitionNoteApproval)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StoreRequisitionNoteApproval  $storeRequisitionNoteApproval
     * @return \Illuminate\Http\Response
     */
    public function edit(StoreRequisitionNoteApproval $storeRequisitionNoteApproval)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StoreRequisitionNoteApproval  $storeRequisitionNoteApproval
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoreRequisitionNoteApproval $storeRequisitionNoteApproval)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoreRequisitionNoteApproval  $storeRequisitionNoteApproval
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoreRequisitionNoteApproval $storeRequisitionNoteApproval)
    {
        //
    }
}
