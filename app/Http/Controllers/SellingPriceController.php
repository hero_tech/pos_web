<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SellingPriceController extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cost = $this->validate($request, [
            'cost' => 'required|numeric'
        ]);

        Product::where('id', $id)->update($cost);


        return redirect()->back()->with('success', 'Product selling cost have been updated successfully');
    }
}
