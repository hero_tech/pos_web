<?php

namespace App\Http\Controllers;

use Log;
use App\Contact;
use App\ContactGroup;
use Illuminate\Http\Request;
use DB;

class ContactGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = ContactGroup::latest()->paginate(8);
        
        return response()->json($groups);
    }
    
    /**
     * getFilteredGroups
     *
     * @param  mixed $search
     * @return void
     */
    public function getFilteredGroups($search)
    {
        $groups = ContactGroup::where(function($query) use ($search){
            $query->where('name','LIKE',"%$search%")
                ->orWhere('description','LIKE',"%$search%");
        })->latest()->paginate(8);       

        return response()->json($groups);
    }

        
    /**
     * groupMembers
     *
     * @param  mixed $id
     * @return void
     */
    public function groupMembers($id)
    {
        $members = ContactGroup::find($id)->contacts()->paginate(5);

        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))->get();

        return response()->json([
            'data' => [
                'members' => $members,
                'contacts' => $contacts
            ]
        ]);
    }
        
    /**
     * getGroupMembers
     *
     * @param  mixed $id
     * @return void
     */
    public function getGroupMembers($id)
    {
        $members = ContactGroup::find($id)->contacts;

        return response()->json([
            'data' => [
                'members' => $members,
            ]
        ]);
    }

    /**
     * getContacts
     *
     * @return void
     */
    public function getContacts()
    {
        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))->get();
        
        return response()->json([
            'data' => [
                'contacts' => $contacts
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = $request->all();

        $contacts = $group['contacts'];

        
        $createGroup = ContactGroup::create([
            'name' => $group['name'],
            'description' => $group['description'],
            'favourite' => $group['favourite']
        ]);

        if($contacts != null) {
            $new_contacts = array_map(fn($contact) => $contact['id'], $contacts);
            $createGroup->contacts()->attach($new_contacts);
        }

        return response()->json(['data' => 'success']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactGroup  $contactGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = $request->all();
        
        ContactGroup::where('id', $id)->update([
            'name' => $group['name'],
            'description' => $group['description'],
            'favourite' => $group['favourite'],
        ]);

       return response()->json(['data' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactGroup  $contactGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ContactGroup::destroy($id);

       return response()->json(['data' => 'success']);
    }
    
    /**
     * updateGroupStatus
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function updateGroupStatus(Request $request, $id)
    {
        $status = $request->status == 1 ? $status = 1 : $status = 0;
        ContactGroup::where('id', $id)->update([
            'status' => $status,
        ]);

        return response()->json(['data' => 'success']);
       
    }
    
    /**
     * addGroupMembers
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function addGroupMembers(Request $request, $id)
    {
        $contacts = $request->get('contacts');

        $contact_id = array_map(fn($contact) => $contact['id'], $contacts);
        
        // Check if user have added an existing contact
        $members = ContactGroup::find($id)->contacts->toArray();
        $member_id = array_map(fn($member) => $member['id'], $members);

        $new_contacts = array_diff($contact_id, $member_id);

        ContactGroup::find($id)->contacts()->attach($new_contacts);

        return response()->json(['data' => 'success']);
    }
    
    /**
     * detachContact
     *
     * @param  mixed $request
     * @return void
     */
    public function detachContact(Request $request)
    {
        $group_id = $request->get('group_id');
        $contact_id = $request->get('contact_id');

        ContactGroup::find($group_id)->contacts()->detach($contact_id);

        return response()->json(['data' => 'success']);
    }


    public function sendGroupMessage(Request $request)
    {
        // Get Group Contacts
        $message = $request->all();

        $group_id = $message['group_id'];
        $message = $message['message'];

        $contacts = ContactGroup::find($group_id)->contacts->toArray();

        $phone_numbers = array_map(fn($contact) => $contact['phone'], $contacts);

        $sendMessage = null;
        foreach ($phone_numbers as $phone_number) {            
            $sendMessage = MessageController::sendGroupMessage($group_id, $message, $phone_number);
        }

        if($sendMessage){
            return response()->json(['data' => 'success']);
        }else{
            return response()->json(['data' => 'error']);
        }

    }
}
