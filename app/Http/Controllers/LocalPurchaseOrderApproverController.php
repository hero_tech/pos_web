<?php

namespace App\Http\Controllers;

use Log;
use App\LocalPurchaseOrder;
use Illuminate\Http\Request;
use App\LocalPurchaseOrderStatus;
use App\LocalPurchaseOrderApprover;
use App\LocalPurchaseOrderApprovalStatus;

class LocalPurchaseOrderApproverController extends Controller
{

    /**
     * getLpoApprovers
     *
     * @return void
     */
    public function getLpoApprovers()
    {        
        $approvers = LocalPurchaseOrderApprover::with(['user'])->get();

        return response()->json([
            'data' => [
                'approvers' => $approvers
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $approver = $this->validate($request, [
            'user_id' => 'required|numeric|unique:local_purchase_order_approvers',
            'sequence' => 'required|numeric'
        ],
        [
            'user_id.unique' => 'Approver already exist, kindly select another user.'
        ]);

        LocalPurchaseOrderApprover::create($approver);

        return redirect()->back()->with('success', 'Local purchase order approver have been added successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocalPurchaseOrderApprover  $localPurchaseOrderApprover
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocalPurchaseOrderApprover $localPurchaseOrderApprover)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocalPurchaseOrderApprover  $localPurchaseOrderApprover
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        LocalPurchaseOrderApprover::find($id)->delete();

        return redirect()->back()->with('success', 'Approver have been deleted successfully.');
    }

    public function updateApproversSequence(Request $request)
    {
        LocalPurchaseOrderApprover::truncate();

        foreach ($request->approvers as $key => $approver) {
            LocalPurchaseOrderApprover::create([
                'user_id' => $approver['user_id'], 
                'sequence' => $approver['sequence'],
                'created_at' => $approver['created_at'],
                'updated_at' => $approver['updated_at'],
            ]);
        }

        return response()->json(['data' => "success"]);
    }

    public function approveLpo(Request $request)
    {   
        $lpo = $request->all();
        //? Check approval request type
        if($lpo['approval'] == LocalPurchaseOrderStatus::APPROVED){
            //* Todo: Approve request
            $finalApprover = self::checkLpoLastApprover($lpo['lpo']);

            if($finalApprover == 0){
                // Not the final approver
                // Approve
                $approve = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo['lpo'])
                    ->where('local_purchase_order_approver_id', auth()->user()->id)
                    ->update([
                        'local_purchase_order_status_id' => LocalPurchaseOrderStatus::APPROVED,
                        'description' => 'Approved',
                        'comment' => $lpo['comment'],
                    ]);
                
                $nextApprover = self::getLpoNextApprover($lpo['lpo']);

                // update next approver
                LocalPurchaseOrderApprovalStatus::where('id', $nextApprover)
                    ->update([
                        'description' => 'Pending Approval',
                    ]);

                return response()->json(['data' =>'success']);
            }else{
                // This is the final approver
                LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo['lpo'])
                    ->where('local_purchase_order_approver_id', auth()->user()->id)
                    ->update([
                        'local_purchase_order_status_id' => LocalPurchaseOrderStatus::APPROVED,
                        'description' => 'Approved',
                        'comment' => $lpo['comment'],
                    ]);

                LocalPurchaseOrder::where('id', $lpo['lpo'])->update([
                    'local_purchase_order_status_id' => LocalPurchaseOrderStatus::APPROVED
                ]);
                
                return response()->json(['data' =>'success']);
            }

        }elseif($lpo['approval'] == LocalPurchaseOrderStatus::CANCELED) {
            //* Todo: Cancel request
            self::cancelApprovalRequest($lpo['lpo']);

        }elseif($lpo['approval'] == LocalPurchaseOrderStatus::REJECTED) {
            //* Todo: Reject request
            // update reason for rejecting
            LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo['lpo'])
                ->where('local_purchase_order_approver_id', auth()->user()->id)
                ->update([
                    'comment' => 'Rejected',
                    'local_purchase_order_status_id' => LocalPurchaseOrderStatus::REJECTED,
                ]);
            //update lpo status
            LocalPurchaseOrder::where('id', $lpo['lpo'])->update([
                'local_purchase_order_status_id' => LocalPurchaseOrderStatus::REJECTED
            ]);
            
            return response()->json(['data' =>'success']);
        }

    }

    /**
     * checkLpoFirstApprover
     *
     * @param  mixed $lpo
     * @return void
     */
    public static function checkLpoFirstApprover($lpo)
    {
        $approver = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
                ->get();
        
        if(count($approver) > 0){
            if($approver->first()->local_purchase_order_approver_id == auth()->user()->id){
                return 1;
            }else{
                return 0;
            }
        } else { 
            return null;
        }
    }
    
    /**
     * checkLpoLastApprover
     *
     * @param  mixed $lpo
     * @return void
     */
    public static function checkLpoLastApprover($lpo)
    {
        $approver = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
            ->where('local_purchase_order_status_id', LocalPurchaseOrderStatus::PENDING)
            ->get();
        
        if(count($approver) > 0){
            if($approver->last()->local_purchase_order_approver_id == auth()->user()->id){
                return 1;
            }else{
                return 0;
            }  
        }else{
            return null;
        }     
    }
    
    /**
     * getLpoNextApprover
     *
     * @param  mixed $lpo
     * @return void
     */
    public static function getLpoNextApprover($lpo)
    {
        $approver = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
            ->where('local_purchase_order_status_id', LocalPurchaseOrderStatus::APPROVED)
            ->get();
        
        if(count($approver) > 0){
            $rowId = $approver->last()->id;

            $nextApprover = LocalPurchaseOrderApprovalStatus::where('id', '>', $rowId)->min('id');
            
            if($nextApprover == null){
                return null;
            }else{
                return $nextApprover;
            }
        }else{
            return null;
        }
    }


    /**
     * getLpoPreviousApprover
     *
     * @param  mixed $lpo
     * @return void
     */
    public static function getLpoPreviousApprover($lpo)
    {
        $approver = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
            ->where('local_purchase_order_status_id', LocalPurchaseOrderStatus::PENDING)
            ->get();
        
        if(count($approver) > 0){
            $rowId = $approver->last()->id;

            $previousApprover = LocalPurchaseOrderApprovalStatus::where('id', '<', $rowId)->max('id');
            
            if($previousApprover == null){
                return null;
            }else{
                return $previousApprover;
            }
        }else{
            return null;
        }
    }
    
    /**
     * checkIfPreviousApproverHasApproved
     *
     * @param  mixed $lpo
     * @return void
     */
    public static function checkIfPreviousApproverHasApproved($lpo)
    {
        $previous = self::getLpoPreviousApprover($lpo);
        
        if(self::checkLpoFirstApprover($lpo) == 0){

            $check = LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
                ->where('id', $previous)
                ->get();

          

            if(count($check) == 1){
                $hasApproved = $check->first()->local_purchase_order_status_id;
                
                if ($hasApproved == LocalPurchaseOrderStatus::APPROVED) {                   
                    return 0;
                }else{
                    return 1;
                }
            }else{
                return 0;
            }
        }else{
            return 0;
        }

    }
    
    /**
     * cancelApprovalRequest
     *
     * @param  mixed $lpo
     * @return void
     */
    public static function cancelApprovalRequest($lpo)
    {
        // update reason for canceling
        LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)
        ->where('local_purchase_order_approver_id', auth()->user()->id)
        ->update([
            'comment' => 'Canceled',
            'local_purchase_order_status_id' => LocalPurchaseOrderStatus::CANCELED,
        ]);
        // Remove approval entries
        LocalPurchaseOrderApprovalStatus::where('local_purchase_order_id', $lpo)->delete();
        //update lpo status
        LocalPurchaseOrder::where('id', $lpo)->update([
            'local_purchase_order_status_id' => LocalPurchaseOrderStatus::CANCELED
        ]);
        
        return response()->json(['data' => "success"]);
    }
    
    /**
     * sendApprovalRequest
     *
     * @param  mixed $lpo
     * @return void
     */
    public function sendApprovalRequest($lpo)
    {
        //update lpo status
        LocalPurchaseOrder::where('id', $lpo)->update([
            'local_purchase_order_status_id' => LocalPurchaseOrderStatus::PENDING
        ]);

        $approvers = LocalPurchaseOrderApprover::all();
        $description = null;
        foreach($approvers as $approver){
            if($approver->sequence == 1){
                $description = 'Pending Approval';
            }else{
                $description = 'Created';
            }

            LocalPurchaseOrderApprovalStatus::create([
                'local_purchase_order_id' => $lpo,
                'local_purchase_order_status_id' => LocalPurchaseOrderStatus::PENDING,
                'local_purchase_order_approver_id' => $approver->user_id,
                'description' => $description,
            ]);
            
        }

        return response()->json(['data' => "success"]);
    }
    
}
