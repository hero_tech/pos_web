<?php

namespace App\Http\Controllers;

use App\SupplierInvoiceItem;
use Illuminate\Http\Request;

class SupplierInvoiceItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupplierInvoiceItem  $supplierInvoiceItem
     * @return \Illuminate\Http\Response
     */
    public function show(SupplierInvoiceItem $supplierInvoiceItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupplierInvoiceItem  $supplierInvoiceItem
     * @return \Illuminate\Http\Response
     */
    public function edit(SupplierInvoiceItem $supplierInvoiceItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupplierInvoiceItem  $supplierInvoiceItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupplierInvoiceItem $supplierInvoiceItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupplierInvoiceItem  $supplierInvoiceItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupplierInvoiceItem $supplierInvoiceItem)
    {
        //
    }
}
