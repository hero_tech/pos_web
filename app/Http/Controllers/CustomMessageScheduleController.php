<?php

namespace App\Http\Controllers;

use DB;
use Log;
use App\Contact;
use Hashids\Hashids;
use App\ContactGroup;
use Illuminate\Http\Request;
use App\CustomMessageSchedule;
use App\CustomMessageSchedulePhoneNumber;

class CustomMessageScheduleController extends Controller
{
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('custom-message-schedules-salt', 15);;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('custom-message-schedules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messageSchedule = $request->all();

        $message_time = $messageSchedule['message_time'];
        $message_date = $messageSchedule['message_date'];
        $contacts = $messageSchedule['contacts'];
        $groups = $messageSchedule['groups'];
        $phone_numbers = $messageSchedule['phone_numbers'];
        $message = $messageSchedule['message'];
        $name = $messageSchedule['name'];


        $scheduleMessage = CustomMessageSchedule::create([
            'name' => $name,
            'message' => $message,
            'message_date' => $message_date,
            'message_time' => date('H:i', strtotime($message_time)),
        ]);

        //check if phone numbers array is present
        if($phone_numbers != null){
            $phone_numbers_array = explode(",", $phone_numbers);
            
            foreach ($phone_numbers_array as $value) {
                
                $phone = trim(preg_replace('/\s+/', ' ', $value));

                if(strlen($phone) != 10){
                    return response()->json(['data' => 'error']);
                }else{                
                    CustomMessageSchedulePhoneNumber::create([
                        'custom_message_schedule_id' => $scheduleMessage->id,
                        'phone' => $phone,
                    ]);
                }
            }
        }

        // check if contact are provided
        if($contacts != null) {
            $contact_id = array_map(fn($contact) => $contact['id'], $contacts);
            $scheduleMessage->contacts()->attach($contact_id);
        }


        //Check id groups are provided
        if($groups != null) {            
            $group_id = array_map(fn($group) => $group['id'], $groups);
            $scheduleMessage->contactGroups()->attach($group_id);
        }        
        
        return response()->json(['data' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomMessageSchedule  $customMessageSchedule
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $msg = $this->hashids->decode($id)[0];

        $customSchedule = CustomMessageSchedule::with([
            'phoneNumbers',
            'contacts',
            'contactGroups'])->find($msg);

        
        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))->latest()->get();
        $groups = ContactGroup::active()->has('contacts')->select('id', 'name')->latest()->get();

        return view('custom-message-schedules.show', [
            'customSchedule' => $customSchedule,
            'contacts' => $contacts,
            'groups' => $groups,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomMessageSchedule  $customMessageSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CustomMessageSchedule::where('id', $id)->update([
            'name' => $request->get('name'),
            'message' => $request->get('message'),
            'message_date' => date('Y-m-d', strtotime($request->get('message_date'))),
            'message_time' => date('H:i', strtotime($request->get('message_time'))),
        ]);
        
        return response()->json(['data' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomMessageSchedule  $customMessageSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
        
    /**
     * getMessageSchedulecontacts
     *
     * @param  mixed $id
     * @return void
     */
    public function getMessageSchedulecontacts($id)
    {
        $schedule_contacts = CustomMessageSchedule::find($id)->contacts->toArray();

        $contact_id = array_map(fn($contact) => $contact['id'], $schedule_contacts);

        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))
            ->whereNotIn('id', $contact_id)
            ->latest()
            ->get();

        return response()->json([
            'data' => [
                'contacts' => $contacts
            ]
        ]);
    }
    
    /**
     * addMessageSchedulecontacts
     *
     * @param  mixed $request
     * @return void
     */
    public function addMessageSchedulecontacts(Request $request)
    {
        $contacts = $request->get('contacts');
        $schedule_id = $request->get('schedule_id');

        $contact_id = array_map(fn($contact) => $contact['id'], $contacts);

        CustomMessageSchedule::find($schedule_id)->contacts()->attach($contact_id);
                
        return response()->json(['data' => 'success']);
    }

    /**
     * customMessageScheduleData
     *
     * @param  mixed $id
     * @return void
     */
    public function customMessageScheduleData($id)
    {
        $schedule = CustomMessageSchedule::where('id', $id)->first();
                
        return response()->json([
            'data' => [
                'schedule' => $schedule,
            ]
        ]);
    }

        
    /**
     * detachMessageScheduleContact
     *
     * @param  mixed $request
     * @return void
     */
    public function detachMessageScheduleContact(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $contact_id = $request->get('contact_id');
        
        CustomMessageSchedule::find($schedule_id)->contacts()->detach($contact_id);

        return redirect()->back()->with('success', 'Contact have been removed from message schedule successfully.');
    }
    
    /**
     * getMessageScheduleGroups
     *
     * @param  mixed $id
     * @return void
     */
    public function getMessageScheduleGroups($id)
    {
        $schedule_groups = CustomMessageSchedule::find($id)->contactGroups->toArray();

        $group_id = array_map(fn($group) => $group['id'], $schedule_groups);

        $groups = ContactGroup::active()->has('contacts')->select('id', 'name')
            ->whereNotIn('id', $group_id)
            ->latest()
            ->get();

        return response()->json([
            'data' => [
                'groups' => $groups
            ]
        ]);
    }

    
    /**
     * addMessageScheduleGroups
     *
     * @param  mixed $request
     * @return void
     */
    public function addMessageScheduleGroups(Request $request)
    {
        $groups = $request->get('groups');
        $schedule_id = $request->get('schedule_id');

        $group_id = array_map(fn($group) => $group['id'], $groups);

        CustomMessageSchedule::find($schedule_id)->contactGroups()->attach($group_id);
                
        return response()->json(['data' => 'success']);
    }
    
    /**
     * detachMessageScheduleGroup
     *
     * @param  mixed $request
     * @return void
     */
    public function detachMessageScheduleGroup(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $group_id = $request->get('group_id');
        
        CustomMessageSchedule::find($schedule_id)->contactGroups()->detach($group_id);

        return redirect()->back()->with('success', 'Group have been removed from message schedule successfully.');
    }

    /**
     * changeCustomScheduleStatus
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function changeCustomScheduleStatus(Request $request, $id)
    {
        $status = $request->get('status');

        $newStatus = collect();
        if($status == 1){
            $newStatus->put('status', 0);
        }else{
            $newStatus->put('status', 1);
        }

        CustomMessageSchedule::where('id', $id)->update($newStatus->toArray());

        return redirect()->back()->with('success', 'Message schedule status has been updated successfully.');

    }
}
