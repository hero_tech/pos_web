<?php

namespace App\Http\Controllers;

use App\Customer;
use Hashids\Hashids;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public $hashids;

    public function __construct() {
        $this->hashids =  new Hashids('customers', 15);;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy('id', 'desc')->get();

        return view('customers.index', [
            'customers' => $customers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = $this->validate($request, [
            'id_number' => 'required|numeric',
            'code' => 'required|numeric',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'nullable|email|unique:customers',
            'phone' => 'required|numeric|unique:customers',
        ],
        [
            'code.required' => 'You have to provide a valid customer loyalty card bar code.'
        ]);
        
        Customer::create($customer);

        //Todo Send a welcome message to the customer
        MessageController::sendWelcomeMessage($customer['first_name'], $customer['phone']);

        return redirect()->back()->with('success', 'Customer have been registered successfully');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }

    
    /**
     * updateProductCode
     *
     * @param  mixed $request
     * @param  mixed $product
     * @return void
     */
    public function updateCustomerCode(Request $request, $customer)
    {
        $id = $this->hashids->decode($customer)[0];

        $code = $this->validate($request, [
            'code' => 'required|numeric|unique:customers'
        ]);

        Customer::where('id', $id)->update($code);

        return redirect()->back()->with('success', 'Customer code have been set successfully');
    }

}
