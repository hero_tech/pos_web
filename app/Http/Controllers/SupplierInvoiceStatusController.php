<?php

namespace App\Http\Controllers;

use App\SupplierInvoiceStatus;
use Illuminate\Http\Request;

class SupplierInvoiceStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupplierInvoiceStatus  $supplierInvoiceStatus
     * @return \Illuminate\Http\Response
     */
    public function show(SupplierInvoiceStatus $supplierInvoiceStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupplierInvoiceStatus  $supplierInvoiceStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(SupplierInvoiceStatus $supplierInvoiceStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupplierInvoiceStatus  $supplierInvoiceStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupplierInvoiceStatus $supplierInvoiceStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupplierInvoiceStatus  $supplierInvoiceStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupplierInvoiceStatus $supplierInvoiceStatus)
    {
        //
    }
}
