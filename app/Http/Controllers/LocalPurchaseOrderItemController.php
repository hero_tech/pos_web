<?php

namespace App\Http\Controllers;

use Hashids\Hashids;
use Illuminate\Http\Request;
use App\LocalPurchaseOrderItem;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Log;

class LocalPurchaseOrderItemController extends Controller
{
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('local-purchase-order-items-salt', 15);;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocalPurchaseOrderItem  $localPurchaseOrderItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {     
        $item_id = $this->hashids->decode($id)[0];

        $validate = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required',
            'unit_price' => 'required',
        ]);

        if($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput();
        }

        LocalPurchaseOrderItem::where('id', $item_id)->update([
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'unit_price' => $request->unit_price
        ]);

        return redirect()->back()->with('success', 'LPO Item details have been updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocalPurchaseOrderItem  $localPurchaseOrderItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item_id = $this->hashids->decode($id)[0];

        LocalPurchaseOrderItem::destroy($item_id);

        return redirect()->back()->with('success', 'You have successfully removed an item from an LPO.');
    }

    public function addLpoItems(Request $request)
    {
        $items = $request->get('items');
        
        foreach($items as $item){
            LocalPurchaseOrderItem::create([
                'local_purchase_order_id' => $request->get('lpo'),
                'product_id' => $item['id'],
                'quantity' => $item['quantity'],
                'unit_price' => $item['price'],
            ]);
        }
        
        return response()->json(['data' => 'success']);
    }
}
