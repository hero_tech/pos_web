<?php

namespace App\Http\Controllers;

use App\LocalPurchaseOrderStatus;
use Illuminate\Http\Request;

class LocalPurchaseOrderStatusController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $status = $this->validate($request, [
            'name' => 'required|string',
        ]);

        LocalPurchaseOrderStatus::create($status);

        return redirect()->back()->with('success', 'New LPO status have been created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocalPurchaseOrderStatus  $localPurchaseOrderStatus
     * @return \Illuminate\Http\Response
     */
    public function show(LocalPurchaseOrderStatus $localPurchaseOrderStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocalPurchaseOrderStatus  $localPurchaseOrderStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(LocalPurchaseOrderStatus $localPurchaseOrderStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocalPurchaseOrderStatus  $localPurchaseOrderStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocalPurchaseOrderStatus $localPurchaseOrderStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocalPurchaseOrderStatus  $localPurchaseOrderStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocalPurchaseOrderStatus $localPurchaseOrderStatus)
    {
        //
    }
}
