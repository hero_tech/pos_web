<?php

namespace App\Http\Controllers;

use Log;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{    
    public static function getBalance()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, env('SMS_BAL'));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        $curl_post_data = array(
           'partnerID' => env('SMS_PARTNER_ID'),
           'apikey' => env("SMS_API_KEY"),
        );
    
        $data_string = json_encode($curl_post_data);
    
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        
        $curl_response = curl_exec($curl);

        if($curl_response != null){

            $balance = json_decode($curl_response, true);
            return $balance['credit'];

        }else{
            return "-";
        }
        
    }
    
    /**
     * formatPhoneNumber
     *
     * @param  mixed $phoneNumber
     * @return void
     */
    public static function formatPhoneNumber($phoneNumber)
    {
        if ($phoneNumber == null) return null;

        if (strlen($phoneNumber) == 10) {
            $phoneNumber = preg_replace('/\s+/', ' ', $phoneNumber);
            $phoneNumber = ltrim($phoneNumber, "0");
            $phoneNumber = '254' . $phoneNumber;
        } else if (strlen($phoneNumber) == 9) {
            $phoneNumber = '254' . $phoneNumber;
        }

        return ltrim($phoneNumber, "+");

    }
    
    /**
     * sendWelcomeMessage
     *
     * @param  mixed $customer
     * @param  mixed $phone
     * @return void
     */
    public static function sendWelcomeMessage($customer, $phone)
    {
        $mess = "Dear ".$customer.", Thank you for being our valued customer.\nYou will now be awarded loyalty points on every purchase you make. To welcome you, we have rewarded you with 10 points.\nCarrymore Suppermarket. Convenience at low price.";
        self::send( $id = null, $phone, $mess, true);

    }
    
    /**
     * sendSupplierWelcomeMessage
     *
     * @param  mixed $supplier
     * @param  mixed $phone
     * @return void
     */
    public static function sendSupplierWelcomeMessage($supplier, $phone)
    {
        $mess = "Dear ".$supplier.", Thank you for being our valued supplier.\nWe look forward walking with you through this journey and strengthening our business relationship.\nCarrymore Suppermarket. Convenience at low price.";
        self::send( $id = null, $phone, $mess, true);
    }
    
    /**
     * sendUserWelcomeMessage
     *
     * @param  mixed $user
     * @param  mixed $phone
     * @return void
     */
    public static function sendUserWelcomeMessage($user, $phone)
    {
        $mess = "Hello ".$user.", Welcome on board!.\nWe look forward walking with you through this journey and strengthening our work relationship.\nCarrymore Suppermarket. Convenience at low price.";
        self::send( $id = null, $phone, $mess, true);
    }
    
    /**
     * sendGroupMessage
     *
     * @param  mixed $message
     * @param  mixed $phoneNumbers
     * @param  mixed $exempt
     * @return void
     */
    public static function sendGroupMessage($group_id, $message, $phoneNumber, $exempt = false){
        if (!$exempt && strlen($message) > 300)
            return false;
            
        $mobile = self::formatPhoneNumber($phoneNumber);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, env('SMS_API'));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); //setting custom header

        $curl_post_data = array(
            'partnerID' => env('SMS_PARTNER_ID'),
            'apikey' => env("SMS_API_KEY"),
            'mobile' => $mobile,
            'message' => $message,
            'shortcode' => env('SMS_SHORT_CODE'),
            'pass_type' => 'plain', //bm5 {base64 encode} or plain
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

        if ($curl_response != null) {
            $response_data = json_decode($curl_response, true);

            foreach ($response_data as $responseItems) {
                foreach ($responseItems as $data) {
                    Message::create([
                        'contact_group_id' => $group_id,
                        'message_id' => $data['messageid'],
                        'mobile' => $data['mobile'],
                        'network_id' => $data['networkid'],
                        'message' => $message,
                        'response_code' => $data['response-code'],
                        'response_description' => $data['response-description'],
                    ]);                
                }
            }
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * send
     *
     * @param  mixed $phone
     * @param  mixed $message
     * @param  mixed $exempt
     * @return void
     */
    public static function send($contact_id, $phone, $message, $exempt = false)
    {

        if (!$exempt && strlen($message) > 300)
            return false;

        $mobile = self::formatPhoneNumber($phone);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, env('SMS_API'));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); //setting custom header

        $curl_post_data = array(
            'partnerID' => env('SMS_PARTNER_ID'),
            'apikey' => env("SMS_API_KEY"),
            'mobile' => $mobile,
            'message' => $message,
            'shortcode' => env('SMS_SHORT_CODE'),
            'pass_type' => 'plain', //bm5 {base64 encode} or plain
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);
       
        if ($curl_response != null) {
            $response_data = json_decode($curl_response, true);

            foreach ($response_data as $responseItems) {
                foreach ($responseItems as $data) {
                    Message::create([
                        'contact_id' => $contact_id,
                        'message_id' => $data['messageid'],
                        'mobile' => $data['mobile'],
                        'network_id' => $data['networkid'],
                        'message' => $message,
                        'response_code' => $data['response-code'],
                        'response_description' => $data['response-description'],
                    ]);                
                }
            }
            return true;
        }else{
            return false;
        }

    }
    
}
