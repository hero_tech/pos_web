<?php

namespace App\Http\Controllers;

use App\SystemConstant;
use Illuminate\Http\Request;

class SystemConstantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $constants = SystemConstant::get();

        return view('settings.system-constants.index', [
            'constants' => $constants
        ]);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SystemConstant  $systemConstant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $constant = $this->validate($request, [
            'name' => 'required',
            'value' => 'required|numeric',
        ]);

    SystemConstant::where('id', $id)
            ->update([
                'name' => $constant['name'],
                'value' => $constant['value'],
            ]);

        return redirect()->back()->with(
            'success', 
            'You have successfully updated system contant, this change will takes effect immediately.'
        );
    }

}
