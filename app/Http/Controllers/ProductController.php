<?php

namespace App\Http\Controllers;

use App\Product;
use Hashids\Hashids;
use App\UnitOfMeasure;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $hashids;

    public function __construct() {
        $this->hashids =  new Hashids('products', 15);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with(['category', 'unitOfMeasure'])->latest()->get();

        return view('products.index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ProductCategory::active()->get();
        $units = UnitOfMeasure::active()->get();

        return view('products.create', [
            'categories' => $categories,
            'units' => $units,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = $this->validate($request, [
            'product_category_id' => 'required|numeric',
            'unit_of_measure_id' => 'required|numeric',
            'name' => 'required|string',
            'code' => 'nullable|string',
            'cost' => 'required|numeric',
            'discount_allowed' => 'required|numeric',
            'quantity' => 'required|numeric',
            'notify' => 'required|numeric',
        ]);

        $product['vatable'] = $request->has('vatable') ? true : false;
        $product['exempted'] = $request->has('exempted') ? true : false;

        $product['user_id'] = auth()->user()->id;

        Product::create($product);

        return redirect()->back()->with('success', 'Product has been registered successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function updateProductCode(Request $request, $product)
    {
        $id = $this->hashids->decode($product)[0];

        $code = $this->validate($request, [
            'code' => 'required|numeric|unique:products'
        ]);

        Product::where('id', $id)->update($code);


        return redirect()->back()->with('success', 'Product code have been set successfully');
    }

}
