<?php

namespace App\Http\Controllers;

use Log;
use App\Role;
use App\User;
use App\Department;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['roles', 'department'])->get();
        $departments = Department::get();
        $roles = Role::active()->get();
        $activeUsers = User::active()->get();

        return view('users.index', [
            'users' => $users,
            'departments' => $departments,
            'roles' => $roles,
            'activeUsers' => $activeUsers
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data = $this->validate($request, [
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'email' => 'required|email|unique:users',
            'username' => 'required|max:10|unique:users',
            'mobile' => 'required|max:12|unique:users',
            'roles' => 'required',
            'department_id' => 'required',
            'supervisor_id' => 'nullable|numeric',
        ]);
        
        $password = self::randomString();

        $data['plain_password'] = $password;
        $data['password'] = bcrypt($password);        
        
        $user = User::create($data);

        $user->roles()->attach($data['roles']);

        //? send a welcome message to the user
        MessageController::sendUserWelcomeMessage($data['first_name'], $data['mobile']);

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send('emails.userwelcomeemail', ['data' => $data], function($message) use ($data)
        {
            $message
                ->from(env('MAIL_FROM_ADDRESS'), 'Carrymore Supermarket')
                ->to($data['email'])
                ->subject('Welcome To Carrymore Supuermarket');
        });

        return redirect()->back()->with('success', 'User has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userData = $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'username' => 'required|string',
            'mobile' => 'required',
            'department_id' => 'required|numeric',
            'supervisor_id' => 'required|numeric',
            'roles' => 'required',
        ]);

        $userInfo = $request->except('roles', '_token', '_method');

        User::where('id', $id)->update($userInfo);

        $user = User::find($id);
        // dump($id);
        // dd($userData['roles']);
        $user->roles()->sync($userData['roles']);

        return redirect()->back()->with('success', 'User details have been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        User::where('id', $id)->delete();
        return redirect()->back()->with('success', 'User have been moved to trash.');
    }

    public function myAccount()
    {
        return view('profile.index');
    }

    public function passwordReset()
    {
        return view('resetpassword');
    }

    public function resetPassword(Request $request)
    {
        $password = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:8|confirmed|different:old_password',
        ],[
            'password.different' => 'The new password must be different from the old password.'
        ]);
        // Check if old password matches
        $user = User::where('id', auth()->user()->id)->first();
        if(Hash::check($request->old_password, $user->password)){
            $new = bcrypt($request->password);

            User::where('id', auth()->user()->id)->update(array('password' => $new, 'reset_password' => false));
            Auth::logout();
            
            return redirect()->route('login');
        }else{
            $password->getMessageBag()->add('old_password', 'Old password does not match, try again.');
            return redirect()->back()->withErrors($password)->withInput();
        }
        
        
    }

    public function changeUserAccountStatus(Request $request, $id)
    {
        $status = $this->validate($request, [
            'status' => 'required|numeric'
        ]);
        $newStatus = collect();
        if($status['status'] == 1){
            $newStatus->put('status', 0);
        }else{
            $newStatus->put('status', 1);
        }

        User::where('id', $id)->update($newStatus->toArray());

        return redirect()->back()->with('success', 'User account status has been updated successfully.');

    }

    public static function randomString($length = 8)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function updateProfile(Request $request)
    {

        $user = $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'mobile' => 'required|numeric',
            'signature' => 'required|image|mimes:jpeg,png,jpg|max:1024',
        ]);
        $signatureName = $user['email'].time().'.'.request()->signature->getClientOriginalExtension();
        
        $userSignature = $user['signature']->move(public_path('/storage/signatures'), $signatureName);
        $user['signature'] = $signatureName;


        User::where('id', auth()->user()->id)->update($user);

        return redirect()->back()->with('success','You have successfully updated your profile.');

    }

}
