<?php

namespace App\Http\Controllers;

use DB;
use Log;
use App\Contact;
use Hashids\Hashids;
use App\ContactGroup;
use App\TaskFrequency;
use App\TaskFrequencyDay;
use App\MessageScheduling;
use Illuminate\Http\Request;
use App\CustomMessageSchedule;
use App\MessageSchedulePhoneNumber;

class MessageSchedulingController extends Controller
{
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('schedule-messages-salt', 15);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedules = MessageScheduling::with([
            'frequency',
            'frequencyDay',
            'phoneNumbers',
            'contacts',
            'contactGroups'])->get();

        $customSchedules = CustomMessageSchedule::with([
            'phoneNumbers',
            'contacts',
            'contactGroups'])->get();

        return view('message-schedules.index', [
            'schedules' => $schedules,
            'customSchedules' => $customSchedules
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('message-schedules.create');
    }
    
    /**
     * getMessageScheduleData
     *
     * @return void
     */
    public function getMessageScheduleData()
    {
        $frequencies = TaskFrequency::select('id', 'name')->get();
        $frequencyDays = TaskFrequencyDay::select('id', 'name')->get();

        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))->latest()->get();
        $groups = ContactGroup::active()->has('contacts')->select('id', 'name')->latest()->get();
                
        return response()->json([
            'data' => [
                'contacts' => $contacts,
                'groups' => $groups,
                'frequencies' => $frequencies,                                
                'frequencyDays' => $frequencyDays,                            
            ]
        ]);
    }
    
    /**
     * messageScheduleData
     *
     * @param  mixed $id
     * @return void
     */
    public function messageScheduleData($id)
    {
        $frequencies = TaskFrequency::select('id', 'name')->get();
        $frequencyDays = TaskFrequencyDay::select('id', 'name')->get();

        $schedule = MessageScheduling::where('id', $id)->first();
                
        return response()->json([
            'data' => [
                'frequencies' => $frequencies,                                
                'frequencyDays' => $frequencyDays,
                'schedule' => $schedule,
            ]
        ]);
    }

    
    /**
     * getMessageSchedulecontacts
     *
     * @param  mixed $id
     * @return void
     */
    public function getMessageSchedulecontacts($id)
    {
        $schedule_contacts = MessageScheduling::find($id)->contacts->toArray();

        $contact_id = array_map(fn($contact) => $contact['id'], $schedule_contacts);

        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))
            ->whereNotIn('id', $contact_id)
            ->latest()
            ->get();

        return response()->json([
            'data' => [
                'contacts' => $contacts
            ]
        ]);
    }

        
    /**
     * getMessageScheduleGroups
     *
     * @param  mixed $id
     * @return void
     */
    public function getMessageScheduleGroups($id)
    {
        $schedule_groups = MessageScheduling::find($id)->contactGroups->toArray();

        $group_id = array_map(fn($group) => $group['id'], $schedule_groups);

        $groups = ContactGroup::active()->has('contacts')->select('id', 'name')
            ->whereNotIn('id', $group_id)
            ->latest()
            ->get();

        return response()->json([
            'data' => [
                'groups' => $groups
            ]
        ]);
    }
    
    /**
     * addMessageSchedulecontacts
     *
     * @param  mixed $request
     * @return void
     */
    public function addMessageSchedulecontacts(Request $request)
    {
        $contacts = $request->get('contacts');
        $schedule_id = $request->get('schedule_id');

        $contact_id = array_map(fn($contact) => $contact['id'], $contacts);

        MessageScheduling::find($schedule_id)->contacts()->attach($contact_id);
                
        return response()->json(['data' => 'success']);
    }
    
    /**
     * addMessageScheduleGroups
     *
     * @param  mixed $request
     * @return void
     */
    public function addMessageScheduleGroups(Request $request)
    {
        $groups = $request->get('groups');
        $schedule_id = $request->get('schedule_id');

        $group_id = array_map(fn($group) => $group['id'], $groups);

        MessageScheduling::find($schedule_id)->contactGroups()->attach($group_id);
                
        return response()->json(['data' => 'success']);
    }
    
    /**
     * detachMessageScheduleContact
     *
     * @param  mixed $request
     * @return void
     */
    public function detachMessageScheduleContact(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $contact_id = $request->get('contact_id');
        
        MessageScheduling::find($schedule_id)->contacts()->detach($contact_id);

        return redirect()->back()->with('success', 'Contact have been removed from message schedule successfully.');
    }

    public function detachMessageScheduleGroup(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $group_id = $request->get('group_id');
        
        MessageScheduling::find($schedule_id)->contactGroups()->detach($group_id);

        return redirect()->back()->with('success', 'Group have been removed from message schedule successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messageSchedule = $request->all();

        $task_frequency_id = $messageSchedule['task_frequency_id'];
        $task_frequency_day_id = $messageSchedule['task_frequency_day_id'];
        $message_time = $messageSchedule['message_time'];
        $contacts = $messageSchedule['contacts'];
        $groups = $messageSchedule['groups'];
        $phone_numbers = $messageSchedule['phone_numbers'];
        $message = $messageSchedule['message'];
        $name = $messageSchedule['name'];


        $scheduleMessage = MessageScheduling::create([
            'task_frequency_id' => $task_frequency_id,
            'task_frequency_day_id' => $task_frequency_day_id,
            'name' => $name,
            'message' => $message,
            'message_time' => date('H:i', strtotime($message_time)),
        ]);

        //check if phone numbers array is present
        if($phone_numbers != null){
            $phone_numbers_array = explode(",", $phone_numbers);
            
            foreach ($phone_numbers_array as $value) {
                
                $phone = trim(preg_replace('/\s+/', ' ', $value));

                if(strlen($phone) != 10){
                    return response()->json(['data' => 'error']);
                }else{                
                    MessageSchedulePhoneNumber::create([
                        'message_scheduling_id' => $scheduleMessage->id,
                        'phone' => $phone,
                    ]);
                }
            }
        }

        // check if contact are provided
        if($contacts != null) {
            $contact_id = array_map(fn($contact) => $contact['id'], $contacts);
            $scheduleMessage->contacts()->attach($contact_id);
        }


        //Check id groups are provided
        if($groups != null) {            
            $group_id = array_map(fn($group) => $group['id'], $groups);
            $scheduleMessage->contactGroups()->attach($group_id);
        }        
        
        return response()->json(['data' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MessageScheduling  $messageScheduling
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $msg = $this->hashids->decode($id)[0];

        $schedule = MessageScheduling::with([
            'frequency',
            'frequencyDay',
            'phoneNumbers',
            'contacts',
            'contactGroups'])->find($msg);

        
        $contacts = Contact::active()->select('id', DB::raw('CONCAT(first_name, " ",last_name) AS name'))->latest()->get();
        $groups = ContactGroup::active()->has('contacts')->select('id', 'name')->latest()->get();

        return view('message-schedules.show', [
            'schedule' => $schedule,
            'contacts' => $contacts,
            'groups' => $groups,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MessageScheduling  $messageScheduling
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        MessageScheduling::where('id', $id)->update([
            'task_frequency_id' => $request->get('task_frequency_id'),
            'task_frequency_day_id' => $request->get('task_frequency_day_id'),
            'name' => $request->get('name'),
            'message' => $request->get('message'),
            'message_time' => date('H:i', strtotime($request->get('message_time'))),
        ]);
        
        return response()->json(['data' => 'success']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MessageScheduling  $messageScheduling
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule =  $this->hashids->decode($id)[0];

        MessageScheduling::destroy($schedule);

        return redirect()->back()->with('success', 'Message schedule have been deleted successfully.');
    }
    
    /**
     * changeScheduleStatus
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function changeScheduleStatus(Request $request, $id)
    {
        $status = $this->validate($request, [
            'status' => 'required|numeric'
        ]);

        $newStatus = collect();
        if($status['status'] == 1){
            $newStatus->put('status', 0);
        }else{
            $newStatus->put('status', 1);
        }


        MessageScheduling::where('id', $id)->update($newStatus->toArray());

        return redirect()->back()->with('success', 'Message schedule status has been updated successfully.');

    }

    
}
