<?php

namespace App\Http\Controllers;

use PDF;
use App\Product;
use Hashids\Hashids;
use App\GoodIssueNote;
use Illuminate\Http\Request;

class GoodIssueNoteController extends Controller
{
     /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('good-issue-notes-salt', 15);;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gins = GoodIssueNote::all();

        return view('gin.index', [
            'gins' => $gins
        ]);
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GoodIssueNote  $goodIssueNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gin = $this->validate($request, [
            'consent' => 'required',
            'comment' => 'required_if:consent,==,false',
        ]);
        
        $consent = $gin['consent'] == 'true' ? 1 : 0;

        GoodIssueNote::where('id', $id)->update([
            'consent' => $consent,
            'comment' => $gin['comment']
        ]);
        
        if ($consent == 1) {
            //Get items
            $items = GoodIssueNote::find($id)->srn->items->toArray();
            // Add to supermarket and remove from store
            foreach($items as $item){
                $product = Product::find($item['product_id']);
                $product->decrement('quantity', $item['quantity']);
                $product->increment('sales_quantity', $item['quantity']);
            }
        }
       

        return redirect()->back()->with('success', 'You have successfully acknowledged consent of GIN');
        
    }

    public function downloadGIN($gin)
    {        
        $id = $this->hashids->decode($gin)[0];

        $data = GoodIssueNote::find($id);
                
        $pdf = PDF::loadView('gin.ginpdf', compact(
            'data'
        ));

        return $pdf->stream();
    }
   
}
