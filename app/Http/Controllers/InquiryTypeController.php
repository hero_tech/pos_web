<?php

namespace App\Http\Controllers;

use App\InquiryType;
use Illuminate\Http\Request;

class InquiryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inquiryTypes = InquiryType::all();

        return view('inquiry-types.index', [
            'inquiryTypes' => $inquiryTypes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InquiryType  $inquiryType
     * @return \Illuminate\Http\Response
     */
    public function show(InquiryType $inquiryType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InquiryType  $inquiryType
     * @return \Illuminate\Http\Response
     */
    public function edit(InquiryType $inquiryType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InquiryType  $inquiryType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InquiryType $inquiryType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InquiryType  $inquiryType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InquiryType $inquiryType)
    {
        //
    }
}
