<?php

namespace App\Http\Controllers;

use Hashids\Hashids;
use Illuminate\Http\Request;
use App\MessageSchedulePhoneNumber;

class MessageSchedulePhoneNumberController extends Controller
{
      /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('message-scheduling-phone-numbers', 15);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $phone_numbers = $this->validate($request, [
            'phone_numbers' => 'required'
        ]);

        $phone_numbers_array = explode(",", $phone_numbers['phone_numbers']);
            
        foreach ($phone_numbers_array as $value) {
            
            $phone = trim(preg_replace('/\s+/', ' ', $value));

            if(strlen($phone) != 10){
                return redirect()->back()->with('error', 'Phone numbers are not valid, check again');
            }else{                
                MessageSchedulePhoneNumber::create([
                    'message_scheduling_id' => $request->get('schedule_id'),
                    'phone' => $phone,
                ]);
            }
        }

        return redirect()->back()->with('success', 'Phone numbers have been added successfully.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $phone_id =  $this->hashids->decode($id)[0];

        $phone = $this->validate($request, [
            'phone' => 'required|digits:10|numeric'
        ]);

        MessageSchedulePhoneNumber::where('id', $phone_id)->update(['phone' => $phone['phone']]);

        return redirect()->back()->with('success', 'Phone number have been updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone_id =  $this->hashids->decode($id)[0];

        MessageSchedulePhoneNumber::destroy($phone_id);

        return redirect()->back()->with('success', 'Phone number have been deleted successfully');
    }
}
