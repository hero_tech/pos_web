<?php

namespace App\Http\Controllers;

use Log;
use App\Product;
use App\Supplier;
use App\JournalEntry;
use App\InvoiceStatus;
use App\SystemConstant;
use App\SupplierInvoice;
use App\LocalPurchaseOrder;
use App\SupplierInvoiceItem;
use Illuminate\Http\Request;
use App\LocalPurchaseOrderItem;
use App\LocalPurchaseOrderStatus;

class ReceiveProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.receive');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::active()->whereHas('lpos', function($query) {
                        $query->where('local_purchase_order_status_id', LocalPurchaseOrderStatus::APPROVED);
                    })->select('id', 'company')->get();


        return response()->json([
            'data' => [
                'suppliers' => $suppliers
            ]
        ]);
    }
    
    /**
     * getSupplierLpos
     *
     * @param  mixed $supplier
     * @return void
     */
    public function getSupplierLpos($supplier)
    {
        // Get supplier lpos which total invoices amount is less than lpo amount
        $lpos = LocalPurchaseOrder::where('supplier_id', $supplier)
                ->where('local_purchase_order_status_id', LocalPurchaseOrderStatus::APPROVED)
                ->select('id', 'number')
                ->get();
       
        return response()->json([
            'data' => [
                'lpos' => $lpos,
            ]
        ]);
    }
    

    /**
     * getLpoItems
     *
     * @param  mixed $lpo
     * @return void
     */
    public function getLpoItems($lpo)
    {
        $lpo_items = LocalPurchaseOrder::find($lpo)->items->toArray();
        
        $lpo_product_ids = array_map(fn($item) => $item['product_id'], $lpo_items);

        $products = null;

        // to return only products whose invoice quantity is less that lpo quantity
        $invoice = SupplierInvoice::where('local_purchase_order_id', $lpo)->pluck('id')->first();

        if($invoice == null){

            $products = Product::whereIn('id', $lpo_product_ids)->select('id', 'name')->get();

        }else{

            $invoice_items = SupplierInvoice::find($invoice)->items->toArray();
            $inv_product_ids = array_map(fn($inv_item) => $inv_item['product_id'], $invoice_items);
            
            // $diff = array_diff($lpo_product_ids, $inv_product_ids);

            Log::info($invoice_items);
            // $product = array_map(fn($product) => $product['quantity'], $product_array);

            // $product_quantity = array_shift($product);
        }

        // dd();
        
        return response()->json([
            'data' => [
                'products' => $products,
            ]
        ]);

    }
    
    /**
     * getAvailableProducts
     *
     * @param  mixed $id
     * @return void
     */
    public function getLpoQuantityAndPrice(Request $request)
    {
        $product_quantity = 0;
        // Get invoice Id
        $invoice = SupplierInvoice::where('local_purchase_order_id', $request->get('lpo_id'))->pluck('id')->first();
        if($invoice != null){
            $invoice_items = SupplierInvoice::find($invoice)->items->toArray();

            $product_array = array_filter($invoice_items, function ($var) use ($request){
                return ($var['product_id'] == $request->get('product_id'));
            });

            $product = array_map(fn($product) => $product['quantity'], $product_array);

            $product_quantity = array_shift($product);
        }

        $item = LocalPurchaseOrderItem::where('product_id', $request->get('product_id'))
                ->where('local_purchase_order_id', $request->get('lpo_id'))
                ->get();

        $quantity = $item->first()->quantity;
        $price = $item->first()->unit_price;

        return response()->json([
            'data' => [
                'quantity' => $quantity,
                'quantity_received' => $product_quantity,
                'price' => $price
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $items = collect($data['items']);
        $invoice_number = self::generateInvoiceNumber();

        $amount_array = array_map(fn($item) => ($item['quantity'] * $item['price']), $items->toArray());

        $amount = array_sum($amount_array);
        
        $invoice  = SupplierInvoice::create([
            'local_purchase_order_id' => $data['lpo_id'],
            'invoice_status_id' => InvoiceStatus::PENDING,
            'supplier_id' => $data['supplier_id'],
            'supplier_invoice_no' => $data['invoice_no'],
            'invoice_no' => $invoice_number,
            'amount' => $amount,
            'balance' => $amount,
            'user_id' => auth()->id(),
        ]);

        foreach($items as $item){
            SupplierInvoiceItem::create([
                'supplier_invoice_id' => $invoice->id,
                'product_id' => $item['id'],
                'quantity' => $item['quantity'],
                'price' => $item['price'],
            ]);

            Product::find($item['id'])->increment('quantity', $item['quantity']);
        }

        // Create Journal Entries
        $account_payable_id = 15;
        $inventory_account_id = 4;

        // Credit (GIRLS)
        JournalEntry::create([
            'transaction_desc' => 'Received products in Invoice - '.$invoice_number,
            'folio' => $account_payable_id,
            'credit' => $amount,
        ]);

        // Debit (DEAL)
        JournalEntry::create([
            'transaction_desc' => 'Received products in Invoice - '.$invoice_number,
            'folio' => $inventory_account_id,
            'debit' => $amount,
        ]);

        return response()->json(['data' =>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //generate lpo number
    public function generateInvoiceNumber(){
        $invoice_number = '';
        $last_number = SupplierInvoice::pluck('invoice_no')->last();
        if ($last_number != null) {
            $invoice_number = substr($last_number, -5);
            $new_number = $invoice_number + 1;
            if (strlen($new_number) == 1) {
                $invoice_number = "S_INV0000".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 2) {
                $invoice_number = "S_INV000".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 3) {
                $invoice_number = "S_INV00".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 4) {
                $invoice_number = "S_INV0".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 5) {
                $invoice_number = "S_INV".$new_number;
                return $invoice_number;
            }
        }else{
            $invoice_number =  "S_INV00001";
            return $invoice_number;
        }
    }
}
