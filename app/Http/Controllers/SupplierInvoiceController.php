<?php

namespace App\Http\Controllers;

use PDF;
use Hashids\Hashids;
use App\SupplierInvoice;
use Illuminate\Http\Request;

class SupplierInvoiceController extends Controller
{   
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('supplier-invoices-salt', 15);;
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\SupplierInvoice  $supplierInvoice
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice_id = $this->hashids->decode($id)[0];

        $invoice = SupplierInvoice::with([
            'user',
            'lpo',
            'status',
            'supplier',
            'items'
        ])->find($invoice_id);

        return view('invoices.supplier.show', [
            'invoice' => $invoice
        ]);
        

    }
    
    /**
     * downloadInvoice
     *
     * @param  mixed $invoice
     * @return void
     */
    public function downloadInvoice($invoice)
    {
        $id = $this->hashids->decode($invoice)[0];
        
        $data = SupplierInvoice::find($id);                

        $pdf = PDF::loadView('invoices.supplier.invoicepdf', compact(
            'data',
        ));

        return $pdf->stream();
    }

}
