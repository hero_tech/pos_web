<?php

namespace App\Http\Controllers;

use App\SaleTemp;
use Illuminate\Http\Request;

class SaleTempController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SaleTemp  $saleTemp
     * @return \Illuminate\Http\Response
     */
    public function show(SaleTemp $saleTemp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SaleTemp  $saleTemp
     * @return \Illuminate\Http\Response
     */
    public function edit(SaleTemp $saleTemp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SaleTemp  $saleTemp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SaleTemp $saleTemp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SaleTemp  $saleTemp
     * @return \Illuminate\Http\Response
     */
    public function destroy(SaleTemp $saleTemp)
    {
        //
    }
}
