<?php

namespace App\Http\Controllers;

use Hashids\Hashids;
use App\CustomerInvoice;
use Illuminate\Http\Request;

class CustomerInvoiceController extends Controller
{
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('customer-invoices-salt', 15);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerInvoice  $customerInvoice
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerInvoice $customerInvoice)
    {
        //
    }

    
    /**
     * generateCustomerInvoiceNumber
     *
     * @return void
     */
    public static function generateCustomerInvoiceNumber(){
        $invoice_number = '';
        $last_number = CustomerInvoice::pluck('invoice_no')->last();
        if ($last_number != null) {
            $invoice_number = substr($last_number, -5);
            $new_number = $invoice_number + 1;
            if (strlen($new_number) == 1) {
                $invoice_number = "C_INV0000".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 2) {
                $invoice_number = "C_INV000".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 3) {
                $invoice_number = "C_INV00".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 4) {
                $invoice_number = "C_INV0".$new_number;
                return $invoice_number;
            }elseif (strlen($new_number) == 5) {
                $invoice_number = "C_INV".$new_number;
                return $invoice_number;
            }
        }else{
            $invoice_number =  "C_INV00001";
            return $invoice_number;
        }
    }



}
