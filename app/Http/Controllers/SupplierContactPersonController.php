<?php

namespace App\Http\Controllers;

use App\SupplierContactPerson;
use Illuminate\Http\Request;

class SupplierContactPersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupplierContactPerson  $supplierContactPerson
     * @return \Illuminate\Http\Response
     */
    public function show(SupplierContactPerson $supplierContactPerson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupplierContactPerson  $supplierContactPerson
     * @return \Illuminate\Http\Response
     */
    public function edit(SupplierContactPerson $supplierContactPerson)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupplierContactPerson  $supplierContactPerson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupplierContactPerson $supplierContactPerson)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupplierContactPerson  $supplierContactPerson
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupplierContactPerson $supplierContactPerson)
    {
        //
    }
}
