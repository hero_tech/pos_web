<?php

namespace App\Http\Controllers;

use Log;
use Excel;
use App\Contact;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Imports\ContactsImport;


class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contacts.index');
    }

    
        
    /**
     * getContacts
     *
     * @return void
     */
    public function getContacts()
    {
        $contacts = Contact::latest()->paginate(10);
        
        return response()->json($contacts);
    }
    
    /**
     * getFilteredContacts
     *
     * @param  mixed $search
     * @return void
     */
    public function getFilteredContacts($search)
    {
        $contacts = Contact::where(function($query) use ($search){
            $query->where('first_name','LIKE',"%$search%")
                ->orWhere('last_name','LIKE',"%$search%")
                ->orWhere('phone','LIKE',"%$search%");
        })->latest()->paginate(10);       

        return response()->json($contacts);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacts = $request->get('contacts');
        
        foreach($contacts as $contact){
            $createContact = Arr::except($contact, 'index');
            Contact::create($createContact);
        }

        return response()->json(['data' => 'success']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $contact = $request->all();

        $id = $contact['id'];

        Contact::where('id', $id)->update([
            'first_name' => $contact['first_name'],
            'last_name' => $contact['last_name'],
            'favourite' => $contact['favourite'],
            'phone' => $contact['phone']
        ]);

        return response()->json(['data' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Contact::destroy($id);

       return response()->json(['data' => 'success']);
    }
    
    /**
     * updateContactStatus
     *
     * @param  mixed $request
     * @param  mixed $id
     * @return void
     */
    public function updateContactStatus(Request $request, $id)
    {
        $status = $request->status == 1 ? $status = 1 : $status = 0;
        Contact::where('id', $id)->update([
            'status' => $status,
        ]);

        return response()->json(['data' => 'success']);       
    }
    
    /**
     * sendMessage
     *
     * @param  mixed $request
     * @return void
     */
    public function sendMessage(Request $request)
    {
        $sendMessage = MessageController::send($request->get('contact_id'), $request->get('phone'), $request->get('message'));

        if($sendMessage){
            return response()->json(['data' => 'success']);
        }else{
            return response()->json(['data' => 'error']);
        } 

    }

    public function importContacts(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);

        Excel::import(new ContactsImport, $request->file('import_file'));
        
        return redirect()->back()->with('success', 'Contacts have been imported successfully');
    }

}
