<?php

namespace App\Http\Controllers;

use Log;
use App\GoodIssueNote;
use Illuminate\Http\Request;
use App\StoreRequisitionNote;
use App\StoreRequisitionNoteStatus;
use App\StoreRequisitionNoteApproval;
use App\StoreRequisitionNoteApprover;

class StoreRequisitionNoteApproverController extends Controller
{
     /**
     * getLpoApprovers
     *
     * @return void
     */
    public function getSrnApprovers()
    {        
        $approvers = StoreRequisitionNoteApprover::with(['user'])->get();

        return response()->json([
            'data' => [
                'approvers' => $approvers
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $approver = $this->validate($request, [
            'user_id' => 'required|numeric|unique:store_requisition_note_approvers',
            'sequence' => 'required|numeric|unique:store_requisition_note_approvers'
        ],
        [
            'user_id.unique' => 'Approver already exist, kindly select another user.',
            'sequence.unique' => 'Approver in that position already exist, kindly provide another sequence.'
        ]);

        StoreRequisitionNoteApprover::create($approver);

        return redirect()->back()->with('success', 'Store requisition note approver have been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StoreRequisitionNoteApprover  $storeRequisitionNoteApprover
     * @return \Illuminate\Http\Response
     */
    public function show(StoreRequisitionNoteApprover $storeRequisitionNoteApprover)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StoreRequisitionNoteApprover  $storeRequisitionNoteApprover
     * @return \Illuminate\Http\Response
     */
    public function edit(StoreRequisitionNoteApprover $storeRequisitionNoteApprover)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StoreRequisitionNoteApprover  $storeRequisitionNoteApprover
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoreRequisitionNoteApprover $storeRequisitionNoteApprover)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoreRequisitionNoteApprover  $storeRequisitionNoteApprover
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        StoreRequisitionNoteApprover::find($id)->forceDelete();

        return redirect()->back()->with('success', 'Approver have been deleted successfully.');
    }
    
    /**
     * updateApproversSequence
     *
     * @param  mixed $request
     * @return void
     */
    public function updateApproversSequence(Request $request)
    {
        
        StoreRequisitionNoteApprover::truncate();

        foreach ($request->approvers as $key => $approver) {
            StoreRequisitionNoteApprover::create([
                'user_id' => $approver['user_id'], 
                'sequence' => $approver['sequence'],
                'created_at' => $approver['created_at'],
                'updated_at' => $approver['updated_at'],
            ]);
        }

        return response()->json(['data' => "success"]);
    }
    
    /**
     * approveSrn
     *
     * @param  mixed $request
     * @return void
     */
    public function approveSrn(Request $request)
    {   
        $srn = $request->all();
        //? Check approval request type
        if($srn['approval'] == StoreRequisitionNoteStatus::APPROVED){
            //* Todo: Approve request
            $finalApprover = self::checkSrnLastApprover($srn['srn']);

            if($finalApprover == 0){
                // Not the final approver
                // Approve
                $approve = StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn['srn'])
                    ->where('store_requisition_note_approver_id', auth()->user()->id)
                    ->update([
                        'store_requisition_note_status_id' => StoreRequisitionNoteStatus::APPROVED,
                        'description' => 'Approved',
                        'comment' => $srn['comment'],
                    ]);
                
                $nextApprover = self::getSrnNextApprover($srn['srn']);

                // update next approver
                StoreRequisitionNoteApproval::where('id', $nextApprover)
                    ->update([
                        'description' => 'Pending Approval',
                    ]);

                return response()->json(['data' =>'success']);
            }else{
                // This is the final approver
                StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn['srn'])
                    ->where('store_requisition_note_approver_id', auth()->user()->id)
                    ->update([
                        'store_requisition_note_status_id' => StoreRequisitionNoteStatus::APPROVED,
                        'description' => 'Approved',
                        'comment' => $srn['comment'],
                    ]);

                StoreRequisitionNote::where('id', $srn['srn'])->update([
                    'store_requisition_note_status_id' => StoreRequisitionNoteStatus::APPROVED
                ]);
                //Todo: Create a GRN to be approved by the srn requestor
                GoodIssueNote::create([
                    'store_requisition_note_id' => $srn['srn'],
                    'user_id' => self::getSrnOwner($srn['srn']),
                ]);
                
                return response()->json(['data' =>'success']);
            }

        }elseif($srn['approval'] == StoreRequisitionNoteStatus::CANCELED) {
            //* Todo: Cancel request
            self::cancelApprovalRequest($srn['srn']);

        }elseif($srn['approval'] == StoreRequisitionNoteStatus::REJECTED) {
            //* Todo: Reject request
            // update reason for rejecting
            StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn['srn'])
                ->where('store_requisition_note_approver_id', auth()->user()->id)
                ->update([
                    'comment' => 'Rejected',
                    'store_requisition_note_status_id' => StoreRequisitionNoteStatus::REJECTED,
                ]);
            //update lpo status
            StoreRequisitionNote::where('id', $srn['srn'])->update([
                'store_requisition_note_status_id' => StoreRequisitionNoteStatus::REJECTED
            ]);
            
            return response()->json(['data' =>'success']);
        }

    }

    public function getSrnOwner($id)
    {
        $getUser = StoreRequisitionNote::where('id', $id)->get()->first();

        return $getUser->user_id;
    }

    /**
     * checkLpoFirstApprover
     *
     * @param  mixed $srn
     * @return void
     */
    public static function checkSrnFirstApprover($srn)
    {
        $approver = StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)->get();
        
        if(count($approver) > 0){
            if($approver->first()->store_requisition_note_approver_id == auth()->user()->id){
                return 1;
            }else{
                return 0;
            }
        } else { 
            return null;
        }
    }
    
        
    /**
     * checkSrnLastApprover
     *
     * @param  mixed $srn
     * @return void
     */
    public static function checkSrnLastApprover($srn)
    {
        $approver = StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)
            ->where('store_requisition_note_status_id', StoreRequisitionNoteStatus::PENDING)
            ->get();
        
        if(count($approver) > 0){
            if($approver->last()->store_requisition_note_approver_id == auth()->user()->id){
                return 1;
            }else{
                return 0;
            }  
        }else{
            return null;
        }     
    }
            
    /**
     * getSrnNextApprover
     *
     * @param  mixed $srn
     * @return void
     */
    public static function getSrnNextApprover($srn)
    {
        $approver = StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)
            ->where('store_requisition_note_status_id', StoreRequisitionNoteStatus::APPROVED)
            ->get();
        
        if(count($approver) > 0){
            $rowId = $approver->last()->id;

            $nextApprover = StoreRequisitionNoteApproval::where('id', '>', $rowId)->min('id');
            
            if($nextApprover == null){
                return null;
            }else{
                return $nextApprover;
            }
        }else{
            return null;
        }
    }

    /**
     * getSrnPreviousApprover
     *
     * @param  mixed $srn
     * @return void
     */
    public static function getSrnPreviousApprover($srn)
    {
        $approver = StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)
            ->where('store_requisition_note_status_id', StoreRequisitionNoteStatus::PENDING)
            ->get();
        
        if(count($approver) > 0){
            $rowId = $approver->last()->id;

            $previousApprover = StoreRequisitionNoteApproval::where('id', '<', $rowId)->max('id');
            
            if($previousApprover == null){
                return null;
            }else{
                return $previousApprover;
            }
        }else{
            return null;
        }
    }
        
    /**
     * checkIfPreviousApproverHasApproved
     *
     * @param  mixed $srn
     * @return void
     */
    public static function checkIfPreviousApproverHasApproved($srn)
    {
        $previous = self::getSrnPreviousApprover($srn);
        
        if(self::checkSrnFirstApprover($srn) == 0){

            $check = StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)
                ->where('id', $previous)
                ->get();

            if(count($check) == 1){
                $hasApproved = $check->first()->store_requisition_note_status_id;
                
                if ($hasApproved == StoreRequisitionNoteStatus::APPROVED) {                   
                    return 0;
                }else{
                    return 1;
                }
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    
    /**
     * cancelApprovalRequest
     *
     * @param  mixed $srn
     * @return void
     */
    public static function cancelApprovalRequest($srn)
    {
        // update reason for canceling
        StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)
        ->where('store_requisition_note_approver_id', auth()->user()->id)
        ->update([
            'comment' => 'Canceled',
            'store_requisition_note_status_id' => StoreRequisitionNoteStatus::CANCELED,
        ]);
        // Remove approval entries
        StoreRequisitionNoteApproval::where('store_requisition_note_id', $srn)->delete();
        //update srn status
        StoreRequisitionNote::where('id', $srn)->update([
            'store_requisition_note_status_id' => StoreRequisitionNoteStatus::CANCELED
        ]);
        
        return response()->json(['data' => "success"]);
    }
    
    /**
     * sendApprovalRequest
     *
     * @param  mixed $srn
     * @return void
     */
    public function sendApprovalRequest($srn)
    {
        //update srn status
        StoreRequisitionNote::where('id', $srn)->update([
            'store_requisition_note_status_id' => StoreRequisitionNoteStatus::PENDING
        ]);

        $approvers = StoreRequisitionNoteApprover::all();
        $description = null;

        foreach($approvers as $approver){
            if($approver->sequence == 1){
                $description = 'Pending Approval';
            }else{
                $description = 'Created';
            }

            StoreRequisitionNoteApproval::create([
                'store_requisition_note_id' => $srn,
                'store_requisition_note_status_id' => StoreRequisitionNoteStatus::PENDING,
                'store_requisition_note_approver_id' => $approver->user_id,
                'description' => $description,
            ]);
            
        }

        return response()->json(['data' => "success"]);
    }
    

}
