<?php

namespace App\Http\Controllers;

use App\CustomMessageSchedulePhoneNumber;
use Illuminate\Http\Request;

class CustomMessageSchedulePhoneNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $phone_numbers = $this->validate($request, [
            'phone_numbers' => 'required'
        ]);

        $phone_numbers_array = explode(",", $phone_numbers['phone_numbers']);
            
        foreach ($phone_numbers_array as $value) {
            
            $phone = trim(preg_replace('/\s+/', ' ', $value));

            if(strlen($phone) != 10){
                return redirect()->back()->with('error', 'Phone numbers are not valid, check again');
            }else{                
                CustomMessageSchedulePhoneNumber::create([
                    'custom_message_schedule_id' => $request->get('schedule_id'),
                    'phone' => $phone,
                ]);
            }
        }

        return redirect()->back()->with('success', 'Phone numbers have been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomMessageSchedulePhoneNumbers  $customMessageSchedulePhoneNumbers
     * @return \Illuminate\Http\Response
     */
    public function show(CustomMessageSchedulePhoneNumbers $customMessageSchedulePhoneNumbers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomMessageSchedulePhoneNumbers  $customMessageSchedulePhoneNumbers
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomMessageSchedulePhoneNumbers $customMessageSchedulePhoneNumbers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomMessageSchedulePhoneNumbers  $customMessageSchedulePhoneNumbers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $phone_id =  $this->hashids->decode($id)[0];

        $phone = $this->validate($request, [
            'phone' => 'required|digits:10|numeric'
        ]);

        CustomMessageSchedulePhoneNumber::where('id', $phone_id)->update(['phone' => $phone['phone']]);

        return redirect()->back()->with('success', 'Phone number have been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomMessageSchedulePhoneNumbers  $customMessageSchedulePhoneNumbers
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $phone_id =  $this->hashids->decode($id)[0];

        CustomMessageSchedulePhoneNumber::destroy($phone_id);

        return redirect()->back()->with('success', 'Phone number have been deleted successfully');
    }
}
