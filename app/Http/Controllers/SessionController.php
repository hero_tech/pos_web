<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class SessionController extends Controller
{
    public function ajaxCheck()
    {
        /*
         * TODO abstract this logic away to the domain
         */

        // Configuration
        $minutes = config('session.lifetime');
        
        // dd($minutes);

        $maxIdleBeforeLogout = $minutes * 60;
        $maxIdleBeforeWarning = (($minutes * 60) - 30);
        $warningTime = $maxIdleBeforeLogout - $maxIdleBeforeWarning;

        // Calculate the number of seconds since the use's last activity
        $idleTime = date('U') - Session::get('lastActive');

        // Warn user they will be logged out if idle for too long
        if ($idleTime > $maxIdleBeforeWarning && empty(Session::get('idleWarningDisplayed'))) {

            Session::put('idleWarningDisplayed', true);

            return 'You have ' . $warningTime . ' seconds left before you are logged out';
        }

        // Log out user if idle for too long
        if ($idleTime > $maxIdleBeforeLogout && empty(Session::get('logoutWarningDisplayed'))) {

            // *** Do stuff to log out user here

            Session::put('logoutWarningDisplayed', true);

            return 'loggedOut';
        }

        return '';
    }
}
