<?php

namespace App\Http\Controllers;

use Log;
use App\User;
use App\Product;
use App\Customer;
use App\SaleTemp;
use App\JournalEntry;
use App\InvoiceStatus;
use App\SystemConstant;
use App\CustomerInvoice;
use App\CustomerInvoiceItem;
use Illuminate\Http\Request;
use App\Http\Requests\PointOfSalePost;

class PointOfSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("pos.index");
    }

    /**
     * getProduct
     *
     * @param  mixed $request
     * @return void
     */
    public function getProduct(Request $request)
    {
        $product = Product::with(['unitOfMeasure'])
            ->where('code', $request->get('product_code'))
            ->where('cost', '>', 0)
            ->get()->first();

        if($product != null){
            return response()->json([
                'data' => [
                    'product' => $product
                ]
            ]);
        }else{
            return response()->json(['data' => 'not_found']);
        }
    }

    public function getVat()
    {
        $tax = SystemConstant::vat();

        return response()->json([
            'data' => [
                'tax' => $tax / 100
            ]
        ]);
    }

    /**
     * createCustomer
     *
     * @param  mixed $request
     * @return void
     */
    public function createCustomer(Request $request)
    {
        $check = Customer::where('phone', $request->get('mobile'))
            ->orWhere('id_number', $request->get('id_number'))
            ->get();
        if(count($check) > 0){
            return response()->json(['data' => 'error']);
        }else{

            Customer::create([
                'id_number' => $request->get('id_number'),
                'code' => rand(1_000_000, 9_999_999),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'phone' => $request->get('mobile'),
            ]);

            //Todo Send a welcome message to the customer
            MessageController::sendWelcomeMessage($request->get('first_name'), $request->get('mobile'));

            return response()->json(['data' =>'success']);

        }
    }


    /**
     * authorizeAction
     *
     * @param  mixed $request
     * @return void
     */
    public static function authorizeAction(Request $request)
    {
        $user = User::where('key_code', $request->get('key_code'))->get()->first();

        if($user != null){
            if($user->can('delete-items')){
                return response()->json(['data' => 'authorized']);
            }else{
                return response()->json(['data' => 'not_authorized']);
            }
        }else{
            return response()->json(['data' => 'user_not_found']);
        }
    }

    /**
     * holdTransaction
     *
     * @param  mixed $request
     * @return void
     */
    public function holdTransaction(Request $request)
    {

        $user = User::where('key_code', $request->get('key_code'))->get()->first();

        if($user != null){
            if($user->can('hold-sales-transaction')){
                $tmp_no = self::generateTempSalesNumber();
                foreach($request->get('products') as $product){
                    SaleTemp::create([
                        'temp_sales_number' => $tmp_no,
                        'product_id' => $product['id'],
                        'product_quantity' => $product['quantity'],
                        'user_id' => auth()->user()->id,
                    ]);
                }

                return response()->json(['data' => 'authorized']);
            }else{
                return response()->json(['data' => 'not_authorized']);
            }
        }else{
            return response()->json(['data' => 'user_not_found']);
        }
    }


    public function getHeldSales()
    {
        $getSales = SaleTemp::with(['product', 'product.unitOfMeasure'])->where('user_id', auth()->user()->id)
            ->latest()
            ->get()
            ->toArray();

        return response()->json([
            'data' => [
                'sales' => $getSales
            ]
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function createCustomerInvoice(Request $request)
    {
        $tax = SystemConstant::vat();

        $phone_id = $request->get('customer');
        $items = $request->get('items');

        //Get customer
        $customer = Customer::where('phone', 'like', '%'.$phone_id.'%')
            ->orWhere('id_number', 'like', '%'.$phone_id.'%')
            ->get()
            ->first()
            ->toArray();

        // Generate invoice number
        $invoice_number = CustomerInvoiceController::generateCustomerInvoiceNumber();

        $invoice_amount = array_sum(array_map(fn($item) => ($item['quantity'] * $item['unit_price']), $items));

        // Sum invoice discount amount
        $invoice_discount_amount = array_sum(array_map(fn($item) => ($item['cost_before_discount'] * $item['discount'] / 100) , $items));
        // Sum invoice tax amount
        $invoice_tax_amount = array_sum(array_map(fn($item) => $item['vatable'] == true ? ($item['cost_before_discount'] * $tax / 100) : 0 , $items));
        //Sales amount (totalInvoiceAmount - (totalDiscount + totalTax))
        $invoice_sales_amount = $invoice_amount - ($invoice_discount_amount + $invoice_tax_amount);

        // Create Invoice
        $customerInvoice = CustomerInvoice::create([
            'customer_id' => $customer['id'],
            'invoice_no' => $invoice_number,
            'amount' => $invoice_amount,
            'balance' => $invoice_amount,
            'invoice_status_id' => InvoiceStatus::PENDING,
            'tax' => $tax,
            'user_id' => auth()->user()->id,
        ]);

        // Invoice items
        foreach ($items as $item) {
            $customerInvoice->items()->saveMany([
                new CustomerInvoiceItem([
                    'product_id' => $item['id'],
                    'quantity' => $item['quantity'],
                    'discount' => $item['discount'],
                    'price' => $item['cost_before_discount'],
                ])
            ]);
        }
        // Create Journal Entries
        $account_receivable_id = 3;
        $sales_revenue_id = 4;
        $sales_tax_payable = 50;
        $sales_discount_id = 28;

        // Credit (GIRLS)
        // Debit (DEAL)
        // Debit: account Receivable
        JournalEntry::create([
            'transaction_desc' => 'Invoiced customer on sales - '.$invoice_number,
            'folio' => $account_receivable_id,
            'debit' => $invoice_amount,
        ]);
        // Credit: sales revenue
        JournalEntry::create([
            'transaction_desc' => 'Invoiced customer on sales (amount after tax and discount)- '.$invoice_number,
            'folio' => $sales_revenue_id,
            'credit' => $invoice_sales_amount,
        ]);
        // Credit: Tax payable
        JournalEntry::create([
            'transaction_desc' => 'Invoiced customer on sales (tax amount)- '.$invoice_number,
            'folio' => $sales_tax_payable,
            'credit' => $invoice_tax_amount,
        ]);
        // Credit: Sales discount
        JournalEntry::create([
            'transaction_desc' => 'Invoiced customer on sales (discount amount)- '.$invoice_number,
            'folio' => $sales_discount_id,
            'credit' => $invoice_discount_amount,
        ]);

        return response()->json(['data' => 'success']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        SaleTemp::where('temp_sales_number', $request->get('temp_number'))->delete();
    }

    /**
     * generateTempSalesNumber
     *
     * @return void
     */
    public function generateTempSalesNumber(){
        $temp_number = '';
        $last_number = SaleTemp::pluck('temp_sales_number')->last();
        if ($last_number != null) {
            $temp_number = substr($last_number, -5);
            $new_number = $temp_number + 1;
            if (strlen($new_number) == 1) {
                $temp_number = "S_TMP0000".$new_number;
                return $temp_number;
            }elseif (strlen($new_number) == 2) {
                $temp_number = "S_TMP000".$new_number;
                return $temp_number;
            }elseif (strlen($new_number) == 3) {
                $temp_number = "S_TMP00".$new_number;
                return $temp_number;
            }elseif (strlen($new_number) == 4) {
                $temp_number = "S_TMP0".$new_number;
                return $temp_number;
            }elseif (strlen($new_number) == 5) {
                $temp_number = "S_TMP".$new_number;
                return $temp_number;
            }
        }else{
            $temp_number =  "S_TMP00001";
            return $temp_number;
        }
    }

}
