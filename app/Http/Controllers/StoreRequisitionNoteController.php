<?php

namespace App\Http\Controllers;

use Log;
use PDF;
use App\User;
use App\Product;
use Hashids\Hashids;
use Illuminate\Http\Request;
use App\StoreRequisitionNote;
use App\StoreRequisitionNoteItem;
use App\StoreRequisitionNoteStatus;
use App\StoreRequisitionNoteApproval;
use App\StoreRequisitionNoteApprover;

class StoreRequisitionNoteController extends Controller
{
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('store-requisition-notes-salt', 15);;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $srns = StoreRequisitionNote::with(['user', 'srnStatus', 'items', 'statusLogs'])->get();
        $statuses = StoreRequisitionNoteStatus::get();
        $users = User::active()->get();

        return view('srn.index', [
            'srns' => $srns,
            'statuses' => $statuses,
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('srn.create');
    }

    /**
     * getData
     *
     * @return void
     */
    public function getData()
    {
        
        $products = Product::active()->select('id', 'name')->latest()->get();

        return response()->json([
            'data' => [
                'products' =>$products,
            ]
        ]);
    }
    
    /**
     * getSrnData
     *
     * @param  mixed $id
     * @return void
     */
    public function getSrnData($id)
    {     
        // Return products not in srn items
        $srn = StoreRequisitionNote::find($id);
        $items = collect($srn->items)->toArray();

        $ids = array_map(fn($item) => $item['product_id'], $items);

        $products = Product::active()->select('id', 'name')
            ->whereNotIn('id', $ids)
            ->latest()
            ->get();

        return response()->json([
            'data' => [
                'products' => $products
            ]
        ]);

    }
    
    /**
     * getAvailableProducts
     *
     * @param  mixed $id
     * @return void
     */
    public function getAvailableProducts($id)
    {        
        $product = Product::find($id);

        $available = $product->quantity;
        $available_super = $product->sales_quantity;

        return response()->json([
            'data' => [
                'available' => $available,
                'available_super' => $available_super
            ]
        ]);
    }
    
    /**
     * statusLogs
     *
     * @param  mixed $id
     * @return void
     */
    public function statusLogs($id)
    {
        $srnStatus = StoreRequisitionNote::where('id', $id)->first();
        $status = $srnStatus->store_requisition_note_status_id;

        $statusLogs = StoreRequisitionNoteApproval::with([
                'status', 
                'approver', 
                'approver.user'
            ])->where('store_requisition_note_id', $id)->get();
        
        $firstApprover = StoreRequisitionNoteApproverController::checkSrnFirstApprover($id);
        // check if the previous approver has approve the LPO
        $isApproved = StoreRequisitionNoteApproverController::checkIfPreviousApproverHasApproved($id);
        
        $currentUserData = StoreRequisitionNoteApproval::where('store_requisition_note_id', $id)
            ->where('store_requisition_note_approver_id', auth()->user()->id)
            ->where('store_requisition_note_status_id', StoreRequisitionNoteStatus::PENDING)
            ->get();
        
        $currentApprover = null;
        
        if($firstApprover == 0 && count($currentUserData) > 0){
            $currentApprover = 1;
        }
        
        if( $isApproved == 0 && count($currentUserData) == 1){
            $currentApprover = 1;
        }else{
            $currentApprover = 0;
        }          

        $user = StoreRequisitionNote::find($id)->first();
        $originator = null;
        if($user->user_id == auth()->user()->id){
            $originator = 1;
        }else{
            $originator = 0;
        }
        
        $hasSignature = User::hasSignature();
        $nextApprover = StoreRequisitionNoteApproverController::checkSrnLastApprover($id);

        return response()->json([
            'data' => [
                'statusLogs' => $statusLogs,
                'approver' => $currentApprover,
                'status' => $status,
                'next' => $nextApprover,
                'originator' => $originator,
                'hasSignature' => $hasSignature
            ]
        ]);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $srn_number = self::generateSRNNumber();
        $srn_products = $data['srn_products'];

        $products = collect();
        $allProducts = array();
        
        foreach ($srn_products as $product) {
            $products->put('product_id', $product['id']);
            $products->put('quantity', $product['quantity']);

            array_push($allProducts, $products->toArray());
        }

        $srn = StoreRequisitionNote::create([
            'store_requisition_note_status_id' => StoreRequisitionNoteStatus::PENDING,
            'number' => $srn_number,
            'user_id' => auth()->user()->id
        ]);

        $approvers = StoreRequisitionNoteApprover::all();
        
        $description = null;
        foreach($approvers as $approver){
            if($approver->sequence == 1){
                $description = 'Pending Approval';
            }else{
                $description = 'Created';
            }

            StoreRequisitionNoteApproval::create([
                'store_requisition_note_id' => $srn->id,
                'store_requisition_note_status_id' => StoreRequisitionNoteStatus::PENDING,
                'store_requisition_note_approver_id' => $approver->user_id,
                'description' => $description,
            ]);
            
        }
        
        foreach($allProducts as $product){
            StoreRequisitionNoteItem::create([
                'store_requisition_note_id' => $srn->id,
                'product_id' => $product['product_id'],
                'quantity' => $product['quantity'],
            ]);
        }

        return response()->json(['data' => 'success']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StoreRequisitionNote  $storeRequisitionNote
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $srn_id = $this->hashids->decode($id)[0];

        $products = Product::active()->get();
        $srn = StoreRequisitionNote::with([
                'srnStatus',
                'user',
                'items',
                'statusLogs'
            ])->find($srn_id);

        return view('srn.show', [
            'srn' => $srn,
            'products' => $products
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StoreRequisitionNote  $storeRequisitionNote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoreRequisitionNote $storeRequisitionNote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoreRequisitionNote  $storeRequisitionNote
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoreRequisitionNote $storeRequisitionNote)
    {
        //
    }

        
    /**
     * generateSRNNumber
     *
     * @return void
     */
    public function generateSRNNumber(){
        $srn_number = '';
        $last_number = StoreRequisitionNote::pluck('number')->last();
        if ($last_number != null) {
            $srn_number = substr($last_number, -5);
            $new_number = $srn_number + 1;
            if (strlen($new_number) == 1) {
                $srn_number = "SRN0000".$new_number;
                return $srn_number;
            }elseif (strlen($new_number) == 2) {
                $srn_number = "SRN000".$new_number;
                return $srn_number;
            }elseif (strlen($new_number) == 3) {
                $srn_number = "SRN00".$new_number;
                return $srn_number;
            }elseif (strlen($new_number) == 4) {
                $srn_number = "SRN0".$new_number;
                return $srn_number;
            }elseif (strlen($new_number) == 5) {
                $srn_number = "SRN".$new_number;
                return $srn_number;
            }
        }else{
            $srn_number =  "SRN00001";
            return $srn_number;
        }
    }

    public function downloadSRN($srn)
    {
        $id = $this->hashids->decode($srn)[0];
        
        $data = StoreRequisitionNote::find($id);
                
        $prepared_by = User::find(auth()->user()->id)->first();

        $approvers = $data->statusLogs;

        $pdf = PDF::loadView('srn.srnpdf', compact(
            'data',
            'prepared_by',
            'approvers'
        ));

        return $pdf->stream();
    }
}
