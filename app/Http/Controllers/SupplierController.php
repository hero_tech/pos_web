<?php

namespace App\Http\Controllers;

use Log;
use App\Supplier;
use Illuminate\Http\Request;
use App\SupplierContactPerson;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::get();

        return view('suppliers.index', [
            'suppliers' => $suppliers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $people = collect($data['people']);

        $supplier = array();
        $supplier['company'] = $data['company'];
        $supplier['phone'] = $data['phone'];
        $supplier['email'] = $data['email'];
        $supplier['location'] = $data['location'];
        $supplier['address'] = $data['address'];
        $supplier['description'] = $data['description'];
        $supplier['user_id'] = auth()->user()->id;

        $createSupplier = Supplier::create($supplier);

        foreach ($people as $person) {
            $createPerson = SupplierContactPerson::create([
                'first_name' => $person['first_name'],
                'last_name' => $person['last_name'],
                'phone' => $person['phone'],
                'email' => $person['email'],
                'job_title' => $person['job_title']
            ]);

            $createSupplier->people()->attach($createPerson);
        }

        MessageController::sendSupplierWelcomeMessage($supplier['company'], $supplier['phone']);
        
        return response()->json(['data' =>'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }
}
