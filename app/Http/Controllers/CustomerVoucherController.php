<?php

namespace App\Http\Controllers;

use App\SystemConstant;
use App\CustomerVoucher;
use Illuminate\Http\Request;

class CustomerVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vouchers = CustomerVoucher::get();

        return view('customer-vouchers.index', [
            'vouchers' => $vouchers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer-vouchers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $voucher = $this->validate($request, [
            'customer_name' => 'required|string',
            'customer_phone' => 'required|numeric',
            'purchased_by' => 'required|string',
            'voucher_amount' => 'required|numeric',
        ]);

        $voucher['expires_at'] = date('Y-m-d H:i:s', strtotime("+ ".SystemConstant::CGVEXP()."months"));
        $voucher['code'] = self::generateVoucherCode();
        $voucher['user_id'] = auth()->user()->id;
        $voucher['voucher_balance'] = $voucher['voucher_amount'];
        
        CustomerVoucher::create($voucher);

        return redirect()->back()->with('success', 'Customer voucher gift have been generated successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerVoucher  $customerVoucher
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerVoucher $customerVoucher)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerVoucher  $customerVoucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerVoucher $customerVoucher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerVoucher  $customerVoucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerVoucher $customerVoucher)
    {
        //
    }

    public function generateVoucherCode()
    {
        $last_code = CustomerVoucher::pluck('code')->last();

        return $last_code != null ? $last_code + 1: 1000000;        
    }

}
