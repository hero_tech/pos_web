<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::latest()->get();
        $users = User::get();

        return view('roles.index', [
            'roles' => $roles,
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = $this ->validate($request,[
            'name' => 'required|unique:roles',
            'permissions' => 'required',
        ]);


        $array = collect();

        foreach ($role['permissions'] as $key => $value) {
            if ($value == 1) {
                $value = true;
            }
            $array->put($key, $value);
        }

        $role['permissions'] = $array->toArray();

        Role::create($role);

        return redirect()->back()->with('success', 'Role has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $users = User::get();
        return view('roles.show', [
            'role' => $role,
            'users' => $users
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $perm = $this->validate($request,[
            'name' => 'required',
            'permissions' => 'required',
        ]);

        $array = collect();

        foreach ($perm['permissions'] as $key => $value) {
            if ($value == 1) {
                $value = true;
            }
            $array->put($key, $value);
        }

        $perm['permissions'] = $array->toArray();

        Role::find($id)->update($perm);

        return redirect()->back()->with('success', 'Role permissions has been update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
    
    public function attachUser(Request $request)
    {
        $newRole = $this->validate($request, [
            'role_id' => 'required',
            'user_id' =>'required',
        ]);

        $checkifExists = DB::select('select * from role_user where user_id = ? and role_id =? ', [$request->user_id, $request->role_id]);

        if ($checkifExists != null) {
            return redirect()->back()->with('error', 'The user is already assigned to this role, try assigning another user...');
        }


        $user = User::find($newRole['user_id']);
        // dd($user);
        $user->roles()->attach($newRole['role_id']);


        return redirect()->back()->with('success', 'User has been assigned to role successfully');
    }

    /**
     * [detachUser description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function detachUser(Request $request)
    {
        $user = User::find($request->user_id);

        $user->roles()->detach([$request->role_id]);

        return redirect()->back()->with('success', 'User has been removed from role successfully');
    }

    /**
     * [copyRole description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function copyRole(Request $request)
    {
        $role = Role::find($request->role_id);
        $newRole = $role->replicate();
        $newRole->save();

        $newRole->name = $newRole->name.' (copy)';
        $newRole->save();

        return redirect()->back()->with('success', 'Role have been copied successfully.');
    }


}
