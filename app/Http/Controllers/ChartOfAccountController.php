<?php

namespace App\Http\Controllers;

use App\AccountType;
use App\AccountSubType;
use Illuminate\Http\Request;
use App\AccountSubTypeCategory;

class ChartOfAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_types = AccountType::orderBy('code', 'asc')->get();
        $categories = AccountSubTypeCategory::with(['account'])->orderBy('account_type_id', 'asc')->get();
        $accounts = AccountSubType::with(['account'])->orderBy('account_type_id', 'asc')->get();
        
        return view('coa.index', [
            'accounts' => $accounts,
            'account_types' => $account_types,
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $account_types = AccountType::select('id', 'name')->get();

        return response()->json([
            'data' => [
                'account_types' => $account_types
            ]
        ]);
        
    }
    
    /**
     * getAccountCategories
     *
     * @param  mixed $id
     * @return void
     */
    public function getAccountCategories($id)
    {        
        $account_categories = AccountSubTypeCategory::where('account_type_id', $id)->select('id', 'name')->get();

        return response()->json([
            'data' => [
                'account_categories' => $account_categories
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        AccountSubType::create([
            'account_type_id' => $request->get('account_type_id'),
            'account_sub_type_category_id' => $request->get('account_category_id'),
            'code' => $request->get('code'),
            'name' => $request->get('name'),
        ]);

        return response()->json(['data' => 'success']);
    }
    
    /**
     * addAccountCategory
     *
     * @param  mixed $request
     * @return void
     */
    public function addAccountCategory(Request $request)
    {
        $category = $this->validate($request, [
            'account_type_id' => 'required',
            'name' => 'required',
            'code' => 'required',
        ]);
        
        AccountSubTypeCategory::create($category);

        return redirect()->back()->with('success', 'Account category has been added successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
