<?php

namespace App\Http\Controllers;

use Hashids\Hashids;
use Illuminate\Http\Request;
use App\StoreRequisitionNoteItem;
use Illuminate\Support\Facades\Validator;

class StoreRequisitionNoteItemController extends Controller
{
    
    /**
     * hashids
     *
     * @var mixed
     */
    public $hashids;
    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->hashids =  new Hashids('store-requisition-note-items-salt', 15);;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StoreRequisitionNoteItem  $storeRequisitionNoteItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item_id = $this->hashids->decode($id)[0];

        $validate = Validator::make($request->all(), [
            'product_id' => 'required',
            'quantity' => 'required',
        ]);

        if($validate->fails()){
            return redirect()->back()->withErrors($validate)->withInput();
        }

        StoreRequisitionNoteItem::where('id', $item_id)->update([
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
        ]);

        return redirect()->back()->with('success', 'SRN Item details have been updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoreRequisitionNoteItem  $storeRequisitionNoteItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item_id = $this->hashids->decode($id)[0];

        StoreRequisitionNoteItem::destroy($item_id);

        return redirect()->back()->with('success', 'You have successfully removed an item from SRN.');
    }

    public function addSrnItems(Request $request)
    {
        $items = $request->get('items');
        
        foreach($items as $item){
            StoreRequisitionNoteItem::create([
                'store_requisition_note_id' => $request->get('srn'),
                'product_id' => $item['id'],
                'quantity' => $item['quantity'],
            ]);
        }
        
        return response()->json(['data' => 'success']);
    }
}
