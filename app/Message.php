<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'contact_id',
        'contact_group_id',
        'message_id',
        'network_id',
        'mobile',
        'message',
        'response_code',
        'response_description'
    ];
}
