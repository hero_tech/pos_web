<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountSubType extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'account_type_id',
        'account_sub_type_category_id',
        'code',
        'name',
        'balance',
        'status'
    ];

    public function account()
    {
        return $this->belongsTo(AccountType::class, 'account_type_id', 'id');
    }

    public function category()
    {
        if ($this->account_sub_type_category_id != null) {
            return $this->belongsTo(AccountSubTypeCategory::class, 'account_sub_type_category_id', 'id');
        }else{
            return $this->belongsTo(AccountType::class, 'account_type_id', 'id');
        }
    }
}
