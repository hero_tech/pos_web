<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountSubTypeCategory extends Model
{
    protected $fillable = [
        'account_type_id',
        'code',
        'name'
    ];

    public function account()
    {
        return $this->belongsTo(AccountType::class, 'account_type_id', 'id');
    }

    public function subType()
    {
        return $this->hasMany(AccountSubType::class);
    }
}
