<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierContactPerson extends Model
{
    use SoftDeletes;

    protected $dates = [
		'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'job_title',
        'status',
    ];

    /**
     * [getFullNameAttribute description]
     * @return [type] [string]
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
    
    public function scopeActive($builder)
	{
		return $builder->where('status', true);
    }
    
    public function people()
    {
        return $this->belongsToMany(
            Supplier::class, 
            'supplier_suppliercontactperson', 
            'supplier_contact_person_id',
            'supplier_id'
        );
    }
}
