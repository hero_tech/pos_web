<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageScheduling extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected $fillable = [
        'task_frequency_id', 
        'task_frequency_day_id', 
        'name', 
        'message',
        'message_time', 
        'status'
    ];

    public function frequency()
    {
    	return $this->belongsTo(TaskFrequency::class, 'task_frequency_id', 'id');
    }

    public function frequencyDay()
    {
    	return $this->belongsTo(TaskFrequencyDay::class, 'task_frequency_day_id', 'id');
    }

    public function phoneNumbers()
    {
        return $this->hasMany(MessageSchedulePhoneNumber::class);
    }

    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class, 
            'contact_message_scheduling', 
            'message_scheduling_id', 
            'contact_id'
        )->withTimestamps();
    }

    public function contactGroups()
    {
        return $this->belongsToMany(
            ContactGroup::class, 
            'contact_group_message_scheduling', 
            'message_scheduling_id', 
            'contact_group_id'
        )->withTimestamps();
    }

    public function scopeActive($builder)
    {
        return $builder->where('status', true);
    }

    public function getRouteKey()
    {
        $hashids = new Hashids('schedule-messages-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
