<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupplierInvoice extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'local_purchase_order_id',
        'invoice_status_id',
        'supplier_id',
        'supplier_invoice_no',
        'invoice_no',
        'amount',
        'balance',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function lpo()
    {
        return $this->belongsTo(LocalPurchaseOrder::class, 'local_purchase_order_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(InvoiceStatus::class, 'invoice_status_id', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function items()
    {
        return $this->hasMany(SupplierInvoiceItem::class);
    }

    public function getRouteKey()
    {
        $hashids = new Hashids('supplier-invoices-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
