<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreRequisitionNoteApproval extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];
    
    protected $fillable = [
        'store_requisition_note_id',
        'store_requisition_note_status_id',
        'store_requisition_note_approver_id',
        'description',
        'comment'
    ];

    public function srn()
    {
        return $this->belongsTo(StoreRequisitionNote::class, 'store_requisition_note_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(StoreRequisitionNoteStatus::class, 'store_requisition_note_status_id', 'id');
    }

    public function approver()
    {
        return $this->belongsTo(StoreRequisitionNoteApprover::class, 'store_requisition_note_approver_id', 'user_id');
    }
}
