<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskFrequency extends Model
{
    protected $fillable = ['name'];
}
