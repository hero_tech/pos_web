<?php

namespace App;

use Hashids\Hashids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocalPurchaseOrderItem extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'local_purchase_order_id',
        'product_id',
        'quantity',
        'unit_price',
    ];

    public function lpo()
    {
        return $this->belongsTo(LocalPurchaseOrder::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

     /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        $hashids = new Hashids('local-purchase-order-items-salt', 15);

        return $hashids->encode($this->getKey());
    }

}
