<?php

use App\Product;
use App\Customer;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/search',function(Request $request){
    $query = $request->get('query');
    
    $products = Product::with(['unitOfMeasure'])
            ->where('cost', '>', 0)
            ->where('name','like','%'.$query.'%')
            ->orWhere('code', 'like', '%'.$query.'%')
            ->get()->take(5);

    return response()->json($products);
});


Route::get('search-customer', function(Request $request){
    $phone_id = $request->get('phone_id');

    $customer = Customer::where('phone', 'like', '%'.$phone_id.'%')
            ->orWhere('id_number', 'like', '%'.$phone_id.'%')
            ->get()
            ->first();

    if($customer != null){
        return response()->json([
            'data' => [
                'customer' => $customer
            ]
        ]);
    }else{
        return response()->json(['data' => 'not_found']);
    }
});
