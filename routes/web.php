<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logout', function () {
    Session::flush();

    Auth::logout();

    return redirect()->route('login');
});

Route::get('/', function () {
    return view('auth.login');
})->name('login');

Auth::routes(['verify' => true]);

Route::get('password-reset', 'UserController@passwordReset')->name('reset-account-password');
Route::post('reset-password', 'UserController@resetPassword')->name('reset-password');
Route::get('welcome', function () {
    return view('welcome');
})->name('user-without-role');
Route::post('session/ajaxCheck','SessionController@ajaxCheck')->name('session.ajax.check');

Route::group(['middleware' => ['auth', 'verified', 'password', 'auth:role']], function () {
    
    Route::get('get-vat', 'PointOfSaleController@getVat');

    Route::get('/home', 'HomeController@index')->name('home');

    // POS
    Route::group(['middleware' => ['can:view-pos']], function () {
        Route::resource('point-of-sales', 'PointOfSaleController');
        Route::post('get-product', 'PointOfSaleController@getProduct');
        Route::post('create-customer', 'PointOfSaleController@createCustomer');

        Route::post('authorize-action', 'PointOfSaleController@authorizeAction');
        Route::post('hold-transaction', 'PointOfSaleController@holdTransaction');
        Route::get('get-held-sales', 'PointOfSaleController@getHeldSales');
        Route::post('delete-temp-sale', 'PointOfSaleController@destroy');
    });

    // Supplier
    Route::resource('suppliers', 'SupplierController');


    // Inventory Management
    Route::group(['middleware' => ['can:view-store']], function () {

        // inventory
        Route::resource('products', 'ProductController');

        Route::post('update-product-code/{product}', 'ProductController@updateProductCode')->name('update-product-code');
        
        Route::get("receive-products", "ReceiveProductController@index");
        Route::get("receive-products-data", "ReceiveProductController@create");
        Route::post("receive-products", "ReceiveProductController@store");
        
        Route::get('get-supplier-lpos/{supplier}', 'ReceiveProductController@getSupplierLpos');
        Route::get('get-lpo-items/{lpo}', 'ReceiveProductController@getLpoItems');
        Route::post('get-lpo-quantity-price', 'ReceiveProductController@getLpoQuantityAndPrice');
        
        // Product Categories
        Route::resource('product-categories', 'ProductCategoryController');
        // Unit of Measure
        Route::resource('unit-of-measures', 'UnitOfMeasureController');

    });
    // Customers
    Route::group(['middleware' => ['can:view-customers-management']], function () {
        Route::resource('customers', 'CustomerController');
        Route::post('update-customer-code/{customer}', 'CustomerController@updateCustomerCode')->name('update-customer-code');

        Route::resource('customer-vouchers', 'CustomerVoucherController');
        Route::post('create-customer-invoice', 'PointOfSaleController@createCustomerInvoice');

    });
    // CRM
    Route::group(['middleware' => ['can:view-crm']], function () {
        Route::resource('inquiry-types', 'InquiryTypeController');
        Route::resource('inquiry-statuses', 'InquiryStatusController');
    });

    // Bulk sms
    Route::group(['middleware' => ['can:view-bulk-sms']], function () {
        // Send Messages
        Route::resource('send-messages', 'SendMessageController');
        Route::get('get-contacts-groups', 'SendMessageController@getContactsGroups');

        //Fixed Schedule messages
        Route::resource('schedule-messages', 'MessageSchedulingController');
        Route::post('change-message-schedule-status/{id}', 'MessageSchedulingController@changeScheduleStatus');
        Route::get('get-message-schedule-data', 'MessageSchedulingController@getMessageScheduleData');
        Route::get('message-schedule-data/{id}', 'MessageSchedulingController@messageScheduleData');
        Route::resource('message-scheduling-phone-numbers', 'MessageSchedulePhoneNumberController');
        Route::get('get-message-schedule-contacts/{id}', 'MessageSchedulingController@getMessageSchedulecontacts');
        Route::post('add-contacts-to-schedule', 'MessageSchedulingController@addMessageSchedulecontacts');
        Route::post('detach-contacts-from-schedule', 'MessageSchedulingController@detachMessageScheduleContact');
        Route::get('get-message-schedule-groups/{id}', 'MessageSchedulingController@getMessageScheduleGroups');
        Route::post('add-groups-to-schedule', 'MessageSchedulingController@addMessageScheduleGroups');        
        Route::post('detach-group-from-schedule', 'MessageSchedulingController@detachMessageScheduleGroup');

        // Custom Schedule messages
        Route::resource('custom-message-schedules', 'CustomMessageScheduleController');
        Route::post('change-custom-message-schedule-status/{id}', 'CustomMessageScheduleController@changeCustomScheduleStatus');
        Route::resource('cms-phone-numbers', 'CustomMessageSchedulePhoneNumberController');
        Route::post('detach-custom-contacts-from-schedule', 'CustomMessageScheduleController@detachMessageScheduleContact');
        Route::get('custom-message-schedule-data/{id}', 'CustomMessageScheduleController@customMessageScheduleData');
        Route::post('detach-contacts-custom-schedule', 'CustomMessageScheduleController@detachMessageScheduleContact');
        Route::get('get-custom-schedule-contacts/{id}', 'CustomMessageScheduleController@getMessageSchedulecontacts');
        Route::post('add-custom-contacts-schedule', 'CustomMessageScheduleController@addMessageSchedulecontacts');
        Route::get('get-custom-schedule-groups/{id}', 'CustomMessageScheduleController@getMessageScheduleGroups');
        Route::post('detach-group-from-cms', 'CustomMessageScheduleController@detachMessageScheduleGroup');
        Route::post('add-groups-to-cms', 'CustomMessageScheduleController@addMessageScheduleGroups');    
                        

        // Contacts
        Route::resource('bulk-sms-contacts', 'ContactController');
        Route::post('update-contact', 'ContactController@update');        
        Route::post('update-contact-status/{id}', 'ContactController@updateContactStatus');        
        Route::post('import-contacts', 'ContactController@importContacts');
        //Get contacts (Vue)
        Route::get('get-contacts', 'ContactController@getContacts');
        Route::get('get-contacts/{id}', 'ContactController@getFilteredContacts');
        Route::post('send-message-contact', 'ContactController@sendMessage');        

        // groups
        Route::resource('groups', 'ContactGroupController');
        Route::get('get-group-contacts', 'ContactGroupController@getContacts');
        Route::get('groups/{id}', 'ContactGroupController@getFilteredGroups');
        Route::post('update-group-status/{id}', 'ContactGroupController@updateGroupStatus');
        Route::get('group-members/{id}', 'ContactGroupController@groupMembers');
        Route::post('add-group-members/{id}', 'ContactGroupController@addGroupMembers');
        Route::post('detach-contact', 'ContactGroupController@detachContact');
        Route::put('groups/{id}', 'ContactGroupController@update');
        Route::post('send-group-message', 'ContactGroupController@sendGroupMessage');  
        
        Route::get('get-group-members/{id}', 'ContactGroupController@getGroupMembers');      
        
    });

    // Procurement
    Route::group(['middleware' => ['can:view-procurement']], function () {
        Route::resource('local-purchase-orders', 'LocalPurchaseOrderController');
        Route::get('create-lpo-data', 'LocalPurchaseOrderController@getData');
        Route::resource('lpo-approvers', 'LocalPurchaseOrderApproverController');

        Route::get('get-lpo-data/{id}', 'LocalPurchaseOrderController@getLpoData');
        // LPO Items
        Route::resource('local-purchase-order-items', 'LocalPurchaseOrderItemController');
        Route::post('add-lpo-items', 'LocalPurchaseOrderItemController@addLpoItems');

        Route::get('get-lpo-approvers', 'LocalPurchaseOrderApproverController@getLpoApprovers');
        Route::put('update-lpo-approvers-sequence', 'LocalPurchaseOrderApproverController@updateApproversSequence');

        Route::get('get-lpo-status-logs/{id}', 'LocalPurchaseOrderController@statusLogs');
        Route::post('approve-lpo', 'LocalPurchaseOrderApproverController@approveLpo');
        Route::post('cancel-lpo-approval-request/{id}', 'LocalPurchaseOrderApproverController@cancelApprovalRequest');
        Route::post('send-lpo-approval-request/{id}', 'LocalPurchaseOrderApproverController@sendApprovalRequest');
        
        Route::resource('lpo-statuses', 'LocalPurchaseOrderStatusController');

        Route::resource('set-selling-price', 'SellingPriceController');

        //Download LPO
        Route::get('download-local-purchase-order/{lpo}', 'LocalPurchaseOrderController@downloadLPO')->name('download-local-purchase-order');


        // SRN
        Route::resource('store-requisition-notes', 'StoreRequisitionNoteController');
        Route::get('create-srn-data', 'StoreRequisitionNoteController@getData');
        Route::get('get-available-products/{id}', 'StoreRequisitionNoteController@getAvailableProducts');

        Route::get('get-srn-data/{id}', 'StoreRequisitionNoteController@getSrnData');
        Route::get('get-srn-status-logs/{id}', 'StoreRequisitionNoteController@statusLogs');
        Route::post('approve-srn', 'StoreRequisitionNoteApproverController@approveSrn');
        Route::post('cancel-srn-approval-request/{id}', 'StoreRequisitionNoteApproverController@cancelApprovalRequest');
        Route::post('send-srn-approval-request/{id}', 'StoreRequisitionNoteApproverController@sendApprovalRequest');

        Route::resource('srn-approvers', 'StoreRequisitionNoteApproverController');

        Route::get('get-srn-approvers', 'StoreRequisitionNoteApproverController@getSrnApprovers');
        Route::put('update-srn-approvers-sequence', 'StoreRequisitionNoteApproverController@updateApproversSequence');

        Route::resource('store-requisition-note-items', 'StoreRequisitionNoteItemController');
        Route::post('add-srn-items', 'StoreRequisitionNoteItemController@addSrnItems');
        //Download SRN
        Route::get('download-store-requisition-note/{srn}', 'StoreRequisitionNoteController@downloadSRN')->name('download-store-requisition-note');

        // Good Issue Note
        Route::resource('good-issue-notes', 'GoodIssueNoteController');
        Route::get('download-good-issue-note/{gin}', 'GoodIssueNoteController@downloadGIN')->name('download-good-issue-note');


    });
    
    // Accounts    
    Route::group(['middleware' => ['can:view-accounts']], function () {
        Route::resource('chart-of-accounts', 'ChartOfAccountController')->middleware('can:view-charts-of-account');

        Route::get('get-accounts-data', 'ChartOfAccountController@create');
        Route::get('get-account-categories/{id}', 'ChartOfAccountController@getAccountCategories');
        
        Route::post('add-account-category', 'ChartOfAccountController@addAccountCategory');

        Route::resource('invoices', 'InvoiceController');

        Route::resource('supplier-invoice', 'SupplierInvoiceController');
        Route::get('download-invoice/{invoice}', 'SupplierInvoiceController@downloadInvoice')->name('download-invoice');
        Route::resource('customer-invoice', 'CustomerInvoiceController');

    });

    // users
    Route::group(['middleware' => ['can:view-users']], function () {
        Route::resource('users', 'UserController');
        Route::post('change-user-status/{id}', 'UserController@changeUserAccountStatus')->middleware('can:update-users');
        
    });

    // Roles
    Route::group(['middleware' => ['can:view-roles']], function () {
		Route::resource('roles','RoleController');
        Route::post('copy-role', 'RoleController@copyRole');
        Route::get('update-role/{id}', 'RoleController@update');
	    Route::post('user-roles/attach', 'RoleController@attachUser');
	    Route::post('user-roles/detach', 'RoleController@detachUser');
    });

    // Departments
    Route::group(['middleware' => ['can:view-departments']], function () {
		Route::resource('departments', 'DepartmentController');
		Route::post('change-department-status/{id}', 'DepartmentController@changeDepartmentStatus')->middleware('can:update-department');
	});

    // System settings
    Route::group(['middleware' => ['can:view-settings']], function () {
        Route::resource('system-settings', 'SystemConstantController');

        Route::get('my-account', 'UserController@myAccount')->middleware('password.confirm');
        Route::post('my-account', 'UserController@updateProfile');
    });
});
