<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreRequisitionNoteApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_requisition_note_approvals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('store_requisition_note_id');
            $table->unsignedInteger('store_requisition_note_status_id');
            $table->unsignedInteger('store_requisition_note_approver_id');
            $table->longText('description');
            $table->longText('comment')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_requisition_note_approvals');
    }
}
