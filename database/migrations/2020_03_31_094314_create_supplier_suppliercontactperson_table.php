<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierSuppliercontactpersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_suppliercontactperson', function (Blueprint $table) {
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('supplier_contact_person_id');
            $table->timestamps();  
            
            $table->foreign('supplier_id', 's_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->foreign('supplier_contact_person_id', 'scp_id')->references('id')->on('supplier_contact_people')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_suppliercontactperson');
    }
}
