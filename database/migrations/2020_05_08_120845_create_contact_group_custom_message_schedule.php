<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactGroupCustomMessageSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_group_custom_message_schedule', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_group_id');
            $table->unsignedBigInteger('custom_message_schedule_id');
            $table->timestamps();

            $table->foreign('contact_group_id', 'c_grp_id')->references('id')->on('contact_groups')->onDelete('cascade');
            $table->foreign('custom_message_schedule_id', 'cm_grp_id')->references('id')->on('custom_message_schedules')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_group_custom_message_schedule');
    }
}
