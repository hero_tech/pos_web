<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('contact_id')->nullable();
            $table->unsignedInteger('contact_group_id')->nullable();
            $table->bigInteger('message_id')->nullable();
            $table->integer('network_id')->nullable();
            $table->string('mobile')->nullable();
            $table->longText('message')->nullable();
            $table->integer('response_code')->nullable();
            $table->longText('response_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
