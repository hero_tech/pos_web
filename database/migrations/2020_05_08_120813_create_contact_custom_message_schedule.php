<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactCustomMessageSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_custom_message_schedule', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_id');
            $table->unsignedBigInteger('custom_message_schedule_id');
            $table->timestamps();

            $table->foreign('contact_id', 'cnt_id')->references('id')->on('contacts')->onDelete('cascade');
            $table->foreign('custom_message_schedule_id', 'cms_id')->references('id')->on('custom_message_schedules')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_custom_message_schedule');
    }
}
