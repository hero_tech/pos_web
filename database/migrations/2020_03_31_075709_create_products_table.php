<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('product_category_id');
            $table->unsignedInteger('unit_of_measure_id');
            $table->unsignedInteger('user_id');
            $table->bigInteger("code")->nullable();
            $table->string("name")->unique();
            $table->string('picture_url')->default('https://carrymore.app/storage/products/');
            $table->string('picture')->default('product.png')->nullable();
            $table->double('cost')->default(0);
            $table->integer('discount_allowed');
            $table->double('quantity')->default(0);
            $table->bigInteger('sales_quantity')->default(0);
            $table->integer('notify');
            $table->boolean('vatable');
            $table->boolean('exempted');
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
