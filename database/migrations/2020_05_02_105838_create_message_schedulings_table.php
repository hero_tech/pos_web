<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageSchedulingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_schedulings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('task_frequency_id');
            $table->unsignedInteger('task_frequency_day_id');
            $table->string('name');
            $table->longText('message');
            $table->time('message_time');
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_schedulings');
    }
}
