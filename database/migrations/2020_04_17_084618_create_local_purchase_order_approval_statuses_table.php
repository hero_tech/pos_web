<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalPurchaseOrderApprovalStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_purchase_order_approval_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('local_purchase_order_id');
            $table->unsignedInteger('local_purchase_order_status_id');
            $table->unsignedInteger('local_purchase_order_approver_id');
            $table->longText('description')->nullable();
            $table->longText('comment')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_purchase_order_approval_statuses');
    }
}
