<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactMessageSchedulingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_message_scheduling', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_id');
            $table->unsignedBigInteger('message_scheduling_id');
            $table->timestamps();

            $table->foreign('contact_id', 'cs_id')->references('id')->on('contacts')->onDelete('cascade');
            $table->foreign('message_scheduling_id', 'ms_id')->references('id')->on('message_schedulings')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_message_scheduling');
    }
}
