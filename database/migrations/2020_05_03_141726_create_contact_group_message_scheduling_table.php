<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactGroupMessageSchedulingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_group_message_scheduling', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_group_id');
            $table->unsignedBigInteger('message_scheduling_id');
            $table->timestamps();

            $table->foreign('contact_group_id', 'cgs_id')->references('id')->on('contact_groups')->onDelete('cascade');
            $table->foreign('message_scheduling_id', 'mss_id')->references('id')->on('message_schedulings')->onDelete('cascade');
                   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_group_message_scheduling');
    }
}
