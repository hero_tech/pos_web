<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalpurchaseorderSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('localpurchaseorder_supplier', function (Blueprint $table) {
        //     $table->unsignedBigInteger('local_purchase_order_id');
        //     $table->unsignedBigInteger('supplier_id');
        //     $table->timestamps();

        //     $table->foreign('local_purchase_order_id', 'lpo_id')->references('id')->on('local_purchase_orders')->onDelete('cascade');
        //     $table->foreign('supplier_id', 'sup_id')->references('id')->on('suppliers')->onDelete('cascade');

        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('localpurchaseorder_supplier');
    }
}
