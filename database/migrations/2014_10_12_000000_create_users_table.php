<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('mobile')->unique();
            $table->unsignedInteger('department_id');
            $table->unsignedInteger('supervisor_id')->nullable();            
            $table->string('job_title')->nullable();            
            $table->boolean('status')->default(true);
            $table->string('avatar_url')->default("https://carrymore.app/storage/avatars/");
            $table->string('avatar_img')->default("user.png");
            $table->string('signature_url')->default("https://carrymore.app/storage/signatures/");
            $table->string('signature')->nullable();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('reset_password')->default(true);
            $table->integer('key_code')->nullable();
            $table->SoftDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
