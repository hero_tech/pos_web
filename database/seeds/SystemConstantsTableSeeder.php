<?php

use Illuminate\Database\Seeder;

class SystemConstantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $constants = [
            'code' => 'VAT', 'name' => 'VAT', 'value' => 14
        ];

        App\SystemConstant::create($constants);
    }
}
