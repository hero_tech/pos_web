<?php

use Illuminate\Database\Seeder;

class TaskFrequencyDaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $days = [        	
        	['name' => 'weekdays'],
        	['name' => 'weekends'],
        	['name' => 'sundays'],
        	['name' => 'mondays'],
        	['name' => 'tuesdays'],
        	['name' => 'wednesdays'],
        	['name' => 'thursdays'],
        	['name' => 'fridays'],
        	['name' => 'saturdays'],
        ];

        foreach ($days as $value) {
        	App\TaskFrequencyDay::create($value);
        }
    }
}
