<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));

        foreach (range(1, 250) as $index) {
	        DB::table('products')->insert([
                'product_category_id' => rand(1, 8),
                'unit_of_measure_id' => rand(1, 10),
	            'name' => $faker->productName,
	            'code' => $faker->ean8,
                'cost' => rand(1, 70000),
	            'discount_allowed' => rand(0, 25),
                'quantity' => rand(50, 450),
                'notify' => rand(10, 100), 
                'vatable' => rand(0, 1), 
                'user_id' => 1,
	            'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'Africa/Nairobi')
	        ]);
		}
    }
}
