<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        foreach (range(1, 100) as $index) {
	        DB::table('contacts')->insert([
	            'first_name' => $faker->firstName,
	            'last_name' => $faker->lastName,
                'phone' => '0'.rand(700000000, 799999999),
                'favourite' => rand(0, 1),
	            'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'Africa/Nairobi')
	        ]);
		}
    }
}
