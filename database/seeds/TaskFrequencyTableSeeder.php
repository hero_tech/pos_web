<?php

use Illuminate\Database\Seeder;

class TaskFrequencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $days = [
        	['name' => 'hourly'],
        	['name' => 'daily'],
        	['name' => 'weekly'],
        	['name' => 'monthly'],
        	['name' => 'quarterly'],
        	['name' => 'yearly'],
        ];

        foreach ($days as $value) {
        	App\TaskFrequency::create($value);
        }
    }
}
