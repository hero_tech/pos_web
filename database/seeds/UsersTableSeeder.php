<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Carrymore',
            'last_name' => 'Supermarket',
            'email'=> 'admin@carrymore.co.ke',
            'username' => 'admin',
            'mobile' => '0708390695',
            'department_id' => rand(1, 4),
            'reset_password' => 0,
            'password' => Hash::make('password')
        ]);

        DB::insert('insert into role_user (user_id, role_id) values (?, ?)', [1, 1]);

        $faker = Faker::create();
        
        foreach (range(1, 15) as $index) {
	        DB::table('users')->insert([
	            'first_name' => $faker->firstName,
	            'last_name' => $faker->lastName,
	            'email' => $faker->email,
	            'username' => $faker->userName,
	            'mobile' => '0'.rand(700000000, 799999999),
	            'department_id' => rand(1, 4),
                'reset_password' => rand(0, 1),
                'job_title' => $faker->jobTitle,
                'password' => Hash::make('password'),
	            'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now')
	        ]);
		}
    }
}
