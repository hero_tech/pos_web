<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
        	['name' => 'Finance & Administration'],
        	['name' => 'Sales & Marketing'],
        	['name' => 'ICT'],
        	['name' => 'Procurement'],
        ];

        foreach ($departments as $department) {
        	App\Department::create($department);
        }
    }
}
