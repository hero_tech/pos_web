<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $count = 1;
        $count2 = 1;

        foreach (range(1, 150) as $index) {
	        DB::table('suppliers')->insert([
	            'company' => $faker->company,
                'description' => $faker->realText($maxNbChars = 150, $indexSize = 2),
	            'phone' => '254'.rand(700000000, 799999999),
                'email' => $faker->email,
                'location' => $faker->city,
                'address' => $faker->city,
                'user_id' => rand(1, 50),
	            'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'Africa/Nairobi')
	        ]);
        }
        
        foreach (range(1, 150) as $index) {
	        DB::table('supplier_contact_people')->insert([
	            'first_name' => $faker->firstName,
	            'last_name' => $faker->lastName,
                'phone' => '254'.rand(700000000, 799999999),
	            'email' => $faker->email,
                'job_title' => $faker->jobTitle,
	            'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'Africa/Nairobi')
	        ]);
        }

        foreach (range(1, 150) as $index) {
	        DB::table('supplier_suppliercontactperson')->insert([
	            'supplier_id' => $count++,
	            'supplier_contact_person_id' => $count2++,
	        ]);
        }
    }
}
