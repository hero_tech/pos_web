<?php

use App\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cats = [
        	['name' => 'Electronics', 'description'=>'Electronics', 'user_id'=>1],
        	['name' => 'Stationeries', 'description'=>'Stationeries', 'user_id'=>1],
        	['name' => 'Beddings', 'description'=>'Beddings', 'user_id'=>1],
        	['name' => 'Utensils', 'description'=>'Utensils', 'user_id'=>1],
        	['name' => 'Consumables', 'description'=>'Consumables', 'user_id'=>1],
        	['name' => 'Beverages', 'description'=>'Beverages', 'user_id'=>1],
        	['name' => 'Toiletries', 'description'=>'Toiletries', 'user_id'=>1],
        	['name' => 'Clothes', 'description'=>'Clothes', 'user_id'=>1],
        ];

        foreach ($cats as $cat) {
        	ProductCategory::create($cat);
        }
    }
}
