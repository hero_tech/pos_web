<?php

use Illuminate\Database\Seeder;

class InvoiceStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ['name' => 'Pending'],
            ['name' => 'Canceled'],
            ['name' => 'Partially Paid'],
            ['name' => 'Paid']
        ];

        foreach ($statuses as $status) {
            App\InvoiceStatus::create($status);
        }
    }
}
