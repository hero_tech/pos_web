<?php

use Illuminate\Database\Seeder;

class StoreRequisitionNoteStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ['name' => 'Pending'],
            ['name' => 'Canceled'],
            ['name' => 'Rejected'],
            ['name' => 'Approved']
        ];

        foreach ($statuses as $status) {
            App\StoreRequisitionNoteStatus::create($status);
        }
    }
}
