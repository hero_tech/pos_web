<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class GroupContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        foreach (range(1, 10) as $index) {
	        DB::table('contact_groups')->insert([
	            'name' => $faker->company,
	            'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'favourite' => rand(0, 1),
	            'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'Africa/Nairobi')
	        ]);
		}
    }
}
