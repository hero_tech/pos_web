<?php

use App\UnitOfMeasure;
use Illuminate\Database\Seeder;

class UnitOfMeasuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
        	['name' => 'Unit', 'user_id' => 1],
        	['name' => 'Pcs', 'user_id' => 1],
        	['name' => 'ml', 'user_id' => 1],
        	['name' => 'ltr', 'user_id' => 1],
        	['name' => 'Ounce', 'user_id' => 1],
        	['name' => 'Kg', 'user_id' => 1],
            ['name' => 'KJ', 'user_id' => 1],
            ['name' => 'mg', 'user_id' => 1],
            ['name' => 'g', 'user_id' => 1],
            ['name' => 'kcal', 'user_id' => 1],
        ];
        
        foreach ($units as $unit) {
        	UnitOfMeasure::create($unit);
        }
    }
}
