<?php

use Illuminate\Database\Seeder;

class LPOStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            ['name' => 'New'],
            ['name' => 'Approved'],
            ['name' => 'Pending'],
            ['name' => 'Canceled'],
        ];

        foreach ($statuses as $status) {
            # code...
            App\LocalPurchaseOrderStatus::create($status);
        }

    }
}
