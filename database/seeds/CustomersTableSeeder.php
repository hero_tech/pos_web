<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker::create();
        
        foreach (range(1, 250) as $index) {
	        DB::table('customers')->insert([
	            'first_name' => $faker->firstName,
	            'last_name' => $faker->lastName,
	            'phone' => '254'.rand(700000000, 799999999),
	            'id_number' => rand(10000000, 40000000),
	            'code' => rand(100000000, 900000000),
	            'email' => $faker->email,
	            'status' => 1,
	            'created_at' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = 'Africa/Nairobi')
	        ]);
		}
    }
}
