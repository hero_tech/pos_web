<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(ProductCategoriesTableSeeder::class);  
        $this->call(SuppliersTableSeeder::class);  
        $this->call(UnitOfMeasuresTableSeeder::class);  
        $this->call(LPOStatusesTableSeeder::class);
        $this->call(SystemConstantsTableSeeder::class);
        $this->call(ChartOfAccountsSeeder::class);
        $this->call(StoreRequisitionNoteStatus::class);
        $this->call(ContactTableSeeder::class);
        $this->call(GroupContactTableSeeder::class);
        $this->call(TaskFrequencyTableSeeder::class);
        $this->call(TaskFrequencyDaysTableSeeder::class);
        $this->call(InvoiceStatusTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        
    }
}
