<?php

use App\AccountType;
use App\AccountSubType;
use App\FinancialStatement;
use App\AccountSubTypeCategory;
use Illuminate\Database\Seeder;

class ChartOfAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //? Assets = Liabilities + Shareholders Equity
        //? Shareholders Equity = Assets - Liabilities
        //* Create Financial Statement
        $statements = [
            ['name' => 'Balance Sheet'],
            ['name' => 'Income Statement']
        ];
        foreach ($statements as $statement) {
            FinancialStatement::create($statement);
        }

        //* Create Account Types
        $accounts = [
            ['financial_statement_id' => 1, 'code' => 10000, 'name' => 'Assets'],
            ['financial_statement_id' => 1, 'code' => 20000, 'name' => 'Liabilities'],
            ['financial_statement_id' => 1, 'code' => 30000, 'name' => 'Shareholder\'s Equity'],
            ['financial_statement_id' => 2, 'code' => 40000, 'name' => 'Revenue'],
            ['financial_statement_id' => 2, 'code' => 50000, 'name' => 'Expenses']
        ];
        
        foreach($accounts as $account){
            AccountType::create($account);
        }

        //* Account Sub types categories
        $categories = [
            ['name' => 'Current Assets'],
            ['name' => 'Non-Current Assets'],
            ['name' => 'Current Liabilities'],
            ['name' => 'Long-term Liabilities']
        ] ;
        
        foreach($categories as $category){
            AccountSubTypeCategory::create($category);
        }

        //* Create Account Sub Types
        $types = [
            // assets
            ['account_type_id' => 1, 'account_sub_type_category_id' => 1, 'code' => 10100, 'name' => 'Cash'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 1, 'code' => 10600, 'name' => 'Petty Cash'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 1, 'code' => 12100, 'name' => 'Accounts Receivable'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 1, 'code' => 13100, 'name' => 'Inventory'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 1, 'code' => 14100, 'name' => 'Supplies'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 1, 'code' => 15300, 'name' => 'Prepaid Insurance'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 2, 'code' => 17000, 'name' => 'Land'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 2, 'code' => 17100, 'name' => 'Buildings'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 2, 'code' => 17300, 'name' => 'Equipments'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 2, 'code' => 17800, 'name' => 'Vehicles'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 2, 'code' => 18100, 'name' => 'Accumulated Depreciation - Buildings'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 2, 'code' => 18300, 'name' => 'Accumulated Depreciation - Equipments'],
            ['account_type_id' => 1, 'account_sub_type_category_id' => 2, 'code' => 18800, 'name' => 'Accumulated Depreciation - Equipments'],
            // liabilities
            ['account_type_id' => 2, 'account_sub_type_category_id' => 3, 'code' => 20140, 'name' => 'Notes Payable'],
            ['account_type_id' => 2, 'account_sub_type_category_id' => 3, 'code' => 21000, 'name' => 'Accounts Payable'],
            ['account_type_id' => 2, 'account_sub_type_category_id' => 3, 'code' => 22100, 'name' => 'Wages Payable'],
            ['account_type_id' => 2, 'account_sub_type_category_id' => 3, 'code' => 23100, 'name' => 'Interest Payable'],
            ['account_type_id' => 2, 'account_sub_type_category_id' => 3, 'code' => 24500, 'name' => 'Unearned Revenues'],
            ['account_type_id' => 2, 'account_sub_type_category_id' => 4, 'code' => 25100, 'name' => 'Mortgage Loan Payable'],
            // Equity
            ['account_type_id' => 3, 'code' => 27100, 'name' => 'Common Stock'],
            ['account_type_id' => 3, 'code' => 27500, 'name' => 'Retained Earnings'],
            ['account_type_id' => 3, 'code' => 29500, 'name' => 'Treasury Stock'],
            ['account_type_id' => 3, 'code' => 29000, 'name' => 'Capital'],
            ['account_type_id' => 3, 'code' => 29100, 'name' => 'Drawings'],
            //Revenue
            ['account_type_id' => 4, 'code' => 31000, 'name' => 'Service Revenues'],
            ['account_type_id' => 4, 'code' => 81000, 'name' => 'Interest Revenues'],
            ['account_type_id' => 4, 'code' => 91000, 'name' => 'Gain on Sales of Assets'],
            ['account_type_id' => 4, 'code' => 31100, 'name' => 'Sales Returns and discounts'],
            // Expenses
            ['account_type_id' => 5, 'code' => 50000, 'name' => 'Salaries Expense'],
            ['account_type_id' => 5, 'code' => 51000, 'name' => 'Wages Expense'],
            ['account_type_id' => 5, 'code' => 54000, 'name' => 'Supplies Expense'],
            ['account_type_id' => 5, 'code' => 56000, 'name' => 'Rent Expense'],
            ['account_type_id' => 5, 'code' => 57000, 'name' => 'Utilities Expense'],
            ['account_type_id' => 5, 'code' => 57600, 'name' => 'Telephone Expense'],
            ['account_type_id' => 5, 'code' => 61000, 'name' => 'Advertising Expense'],
            ['account_type_id' => 5, 'code' => 75000, 'name' => 'Depreciation Expense']            
        ];

        foreach($types as $type){
            AccountSubType::create($type);
        }

    }
}
