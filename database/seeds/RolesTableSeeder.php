<?php

use Illuminate\Database\Seeder;
use App\Role;
use Illuminate\Support\Collection;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Role::PERMISSIONS;

        $all = collect();
        foreach ($permissions as $permission) {
            foreach ($permission as $key => $value) {
                $all->put($key, $value);
            }
        }

        Role::create([
            'name' => 'System Administrator',
            'permissions' => $all
        ]);
    }
}
