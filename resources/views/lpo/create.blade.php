@extends('layouts.app', ['title' => 'Create LPO'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">LPO</li>
                    </ol>
                </div>
                <h4 class="page-title">Create Local purchase order</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('local-purchase-orders.index') }}" class="btn btn-info btn-sm"><i class="mdi mdi-truck-fast"></i> L.P.O (s)</a>
                </div>
                <div class="card-body">
                    <create-local-purchase-order></create-local-purchase-order>
                </div>
            </div>
        </div>
    </div>

@endsection