<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ public_path('assets/css/bootstrap.min.css') }}">
    <title>Local Purchase Order - {{ $data->number }}</title>

    <style>
        @page{
            margin-top: 70px;
        }
        body {
            font-family: "Cambria";
            font-size: 14px;
            background-color: white;
            color: black
        }
        .logo {
            width: 250px;
            max-width: 50%;
            height: 70px;
            float: left;
        }

        .signature {
            width: 100px;
            height: 50px;
        }

        table, tr, td {
            padding: 0px;
            color: black
        }
        .header{
            height: 80px;
        }
        .lpo_number{
            float: right;
        }
        .supplier {
            margin-bottom: 30px;
        }
        .supplier .vendor{
            margin-bottom: 10px;
        }
        .supplier .details {
            width: 50%;
            float:left;
        }
        .supplier .shipto {
            width: 50%;
            float:right;
        }
        .info{
            margin-top: 1,0px;
        }
        
        .page_break { page-break-before: always; }
        .no_page_break { page-break-before: avoid; }
        hr{
            color: grey;
            border-bottom: 1px solid grey;
        }
    </style>
</head>
        
    <div class="header">
        <div class="logo">
            <img src="{{ public_path('storage/logo/carrymore_logo.png') }}" height="100%" width="100%">
        </div>
        <div class="lpo_number">
            <h6>LPO Number: {{ $data->number }} <br>
                <strong>LPO Date: {{ date('d-m-Y H:i A', strtotime($data->created_at)) }}</strong>
            </h6>
        </div>
    </div>
    <hr>
    <div class="row supplier">
        <div class="details">
            <div class="vendor">Vendor information</div>
            <div class="address">
                <address>
                    <strong>{{ $data->supplier->company }}</strong><br>
                    {{ $data->supplier->location }}<br>
                    {{ $data->supplier->address }}<br>
                    <abbr title="Phone">Phone:</abbr> {{ $data->supplier->phone }}<br>
                    <abbr title="Phone">Email:</abbr> {{ $data->supplier->email }}
                </address>
            </div>
        </div>
        <div class="shipto">
            <div class="vendor">Shipment information</div>
            <address>
                <strong>Carrymore Supermarket.</strong><br>
                P.O. Box 176-10100<br>Kamakwa shopping center, Nyeri<br>
                <abbr title="Phone">Phone:</abbr> 0741 187 222<br>
                <abbr title="Phone">Email:</abbr> carrymoresupermarket@gmail.com
                </address>
        </div>
    </div>
    <!-- end row -->

    <div style="margin-top: 2px">&nbsp;</div>

    <div class="info">
        <p>We are pleased to confirm our purchase of the following items:</p>
    </div>

    <div>
        <table class="table table-bordered table-condensed table-striped" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Unit Cost</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data->items as $key => $item)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                        <td>{{ number_format($item->unit_price, 2) }}</td>
                        <td>{{ number_format(($item->unit_price * $item->quantity), 2) }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3"></td>
                    <td>Sub-total</td>
                    <td> {{ number_format($subtotal,2) }} </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td>VAT</td>
                    <td>{{ number_format($vat, 2) }}</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td>Total</td>
                    <td>{{ number_format($subtotal + $vat , 2) }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-12" style="page-break-inside: avoid">
        <h5 class="small text-dark">TERMS AND CONDITIONS</h5>

        <small class="text-muted">
            <ol type="1">
                <li>Delivery: All goods to be delivered within 14 days from date of LPO</li>
                <li>Condition: We reserve the right to reject goods that are not in good order or condition as determined by our quality control</li>
            </ol>
        </small>

        <div class="col-12" style="margin-top: 15px;">
            <table width="100%">
                <tr>
                    <td width="15%"><b>Prepared By:</b></td>
                    <td style="border-bottom: 0.5px solid #000">{{ $prepared_by->full_name }}</td>
                    <td width="8%"><b>Sign:</b></td>
                    <td width="20%" style="border-bottom: 0.5px solid #000" width="20%"><img class="signature" src="{{ public_path('storage/signatures/'.$prepared_by->signature) }}" height="100%" width="100%"></td>
                    <td width="8%"><b>Date:</b></td>
                    <td style="border-bottom: 0.5px solid #000" width="20%">{{ date('d/m/Y', strtotime($data->created_at)) }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                @foreach ($approvers as $approver)
                    <tr>
                        <td>
                            <b>
                                @if(lastApprover($approver->approver->user->id, $data->id) == 0)
                                    Approved By:
                                @else
                                    Authorised By:
                                @endif
                            </b>
                        </td>
                        <td style="border-bottom: 0.5px solid #000">{{ $approver->approver->user->full_name }}</td>
                        <td><b>Sign:</b></td>
                        <td style="border-bottom: 0.5px solid #000">
                            @if($approver->local_purchase_order_status_id == App\LocalPurchaseOrderStatus::APPROVED)
                                <img class="signature" src="{{ public_path('storage/signatures/'.$approver->approver->user->signature) }}" height="100%" width="100%">
                            @endif
                        </td>
                        <td><b>Date:</b></td>
                        <td style="border-bottom: 0.5px solid #000">
                            @if($approver->local_purchase_order_status_id == App\LocalPurchaseOrderStatus::APPROVED)
                                {{ date('d/m/Y', strtotime($data->updated_at)) }}
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endforeach
                
                
            </table>
        </div>
        
    </div>     
