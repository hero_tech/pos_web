@extends('layouts.app', ['title' => 'Local Purchase Orders'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">LPO</li>
                    </ol>
                </div>
                <h4 class="page-title">Local purchase orders</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-b1-tab" data-toggle="tab" href="#home-b1" role="tab" aria-controls="home-b1" aria-selected="true">
                            <span class="d-block d-sm-none"><i class="mdi mdi-truck-delivery"></i></span>
                            <span class="d-none d-sm-block">Local Purchase Orders</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-b1-tab" data-toggle="tab" href="#lpo-approvers" role="tab" aria-controls="lpo-approvers" aria-selected="false">
                            <span class="d-block d-sm-none"><i class="fa fa-check"></i></span>
                            <span class="d-none d-sm-block">LPO Approvers</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-b1-tab" data-toggle="tab" href="#profile-b1" role="tab" aria-controls="profile-b1" aria-selected="false">
                            <span class="d-block d-sm-none"><i class="fa fa-info-circle"></i></span>
                            <span class="d-none d-sm-block">LPO Statuses</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="home-b1" role="tabpanel" aria-labelledby="home-b1-tab">
                        <div class="form-group">
                            <a href="{{ route('local-purchase-orders.create') }}" class="btn btn-success btn-sm" style="border-radius:0px;"><i class="fa fa-plus-circle"></i> Create New LPO</a>
                        </div>
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>LPO No</th>
                                    <th>Supplier</th>
                                    <th>Items</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Date Created</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($lpos as $key => $lpo)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $lpo->number }}</td>
                                        <td>{{ $lpo->supplier->company }}</td>
                                        <td>
                                            <code style="color: blue;cursor: pointer;" data-toggle="modal" data-target="#view-lpo-items{{ $lpo->id }}">{{ count($lpo->items) }} (click to view)</code>
        
                                            <div class="modal fade" id="view-lpo-items{{ $lpo->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                                <div class="modal-dialog modal-full" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">{{ $lpo->number }} requested items</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="dt-responsive table-responsive">
                                                                <table class="table table-striped table-condensed table-bordered">
                                                                    <thead>
                                                                        <th>#</th>
                                                                        <th>Name</th>
                                                                        <th>Quantity</th>
                                                                        <th>Unit Price</th>
                                                                        <th>Line Total</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($lpo->items as $count => $item)
                                                                            <tr>
                                                                                <td>{{ $count+1 }}</td>
                                                                                <td>{{ $item->product->name }}</td>
                                                                                <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                                                                                <td>{{ number_format($item->unit_price, 2) }}</td>
                                                                                <td>{{ number_format($item->quantity * $item->unit_price) }}</td>
                                                                            </tr>
                                                                        @endforeach
        
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>Sub-total</td>
                                                                            <td><b>{{ number_format(getLpoSubTotal($lpo->id), 2) }}</b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>VAT</td>
                                                                            <td><b>{{ number_format(getLpoVat($lpo->id), 2) }}</b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>Total</td>
                                                                            <td><b>{{ number_format(getLpoVat($lpo->id) + getLpoSubTotal($lpo->id), 2) }}</b> </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            @php
                                                $amount = 0;
                                                foreach ($lpo->items as $value) {
                                                    $amount += ($value->unit_price * $value->quantity);
                                                }    
                                            @endphp
                                            {{ number_format($amount, 2) }}
                                        </td>
                                        <td>
                                            @if($lpo->local_purchase_order_status_id == 1)
                                                <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-info btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Pending</span>
                                            @elseif($lpo->local_purchase_order_status_id == 2)
                                                <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-orange btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Canceled</span>
                                            @elseif($lpo->local_purchase_order_status_id == 3)
                                                <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-danger btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Rejected</span>
                                            @else
                                                <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-success btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Approved</span>
                                            @endif
                                            <div class="modal fade" id="view-lpo-status-{{ $lpo->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                                <div class="modal-dialog modal-full" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">{{ $lpo->number }} Approval Status</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            {{-- {{ dd(checkUserIfCurrentLpoApprover($lpo->id, auth()->user()->id)) }} --}}                                                                
                                                            <approve-lpo 
                                                                :lpo="{{ $lpo->id }}">
                                                            </approve-lpo>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ date('d-m-Y H:i A', strtotime($lpo->created_at)) }}</td>
                                        <td>
                                            <a href="{{ route('local-purchase-orders.show', [$lpo]) }}" class="btn btn-xs btn-info"><i class=" fa fa-edit"></i> View More</a>
    
                                            @if($lpo->local_purchase_order_status_id == App\LocalPurchaseOrderStatus::APPROVED)
                                                <a href="{{ route('download-local-purchase-order', [$lpo]) }}" class="btn btn-xs btn-primary"><i class="mdi mdi-download"></i> Download</a>            
                                            @else
                                                <button type="button" class="btn btn-xs btn-warning" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="" data-content="You are not permitted to downlaod an LPO if it has not been fully approved" data-original-title="LPO Not Approved"><i class="mdi mdi-download-off"></i> Download</button>                                            
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                    <div class="tab-pane" id="lpo-approvers" role="tabpanel" aria-labelledby="lpo-approvers">
                        <div class="form-group">
                            <button class="btn btn-success width-xs" style="border-radius:0px;"  data-toggle="modal" data-target="#lpo-approver"><i class="fa fa-plus-circle"></i> LPO Approver</a>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                        <local-purchase-order-approvers></local-purchase-order-approvers>

                    </div>

                    <div class="tab-pane" id="profile-b1" role="tabpanel" aria-labelledby="profile-b1-tab">
                        <div class="form-group">
                            <button class="btn btn-success width-xs" style="border-radius:0px;"  data-toggle="modal" data-target="#lpo-statuses"><i class="fa fa-plus-circle"></i> LPO Status</a>
                        </div>
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($statuses as $key => $status)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $status->name }}</td>
                                        <td>{{ $status->status }}</td>
                                        <td>{{ $status->created_at }}</td>
                                        <td>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>


    <div class="modal fade" id="lpo-approver" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">New LPO Approver</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('lpo-approvers') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="col-12 form-group">
                            <select name="user_id" class="form-control" required>
                                <option value="">-- Select Approver --</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <input type="number" name="sequence" class="form-control" placeholder="Sequence" required>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Create new LPO Approver</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="lpo-statuses" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">New LPO Status</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('lpo-statuses') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="col-12">
                            <label>Status name</label>
                        </div>
                        <div class="col-12">
                            <input type="text" name="name" class="form-control" required>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Create new LPO status</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection