@extends('layouts.app', ['title' => 'LPO Details'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">LPO</li>
                    </ol>
                </div>
                <h4 class="page-title">Local purchase order details - {{ $lpo->number }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('local-purchase-orders.index') }}" class="btn btn-info btn-sm"><i class="mdi mdi-view-list"></i> View All L.P.O (s)</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="card-box shadow-none border border-aqua">
                                <div class="member-card">
                                    <div class="avatar-xl member-thumb mb-3 mx-auto d-block">
                                        <img src="{{ asset('assets/images/purchase-order.png') }}" class="rounded-circle img-thumbnail" alt="LPO">
                                        <i class="mdi mdi-star-circle member-star text-danger"></i>
                                    </div>

                                    <div class="text-center">
                                        <h5 class="font-18 mb-1">{{ $lpo->number }}</h5>
                                        <p class="text-muted mb-2">Supplier: <b>{{ $lpo->supplier->company }}</b></p>
                                    </div>

                                    <hr/>

                                    <div class="">
                                        <div class="row form-group text-muted font-14 ">
                                            <div class="col-7">
                                                <strong>LPO Items</strong> 
                                            </div>
                                            <div class="col-5">
                                                <span>{{ count($lpo->items) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>LPO Sub Total:</strong>
                                            </div>
                                            <div class="col-5">
                                                <span>{{ number_format(getLpoSubTotal($lpo->id), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>LPO VAT :</strong> 
                                            </div>
                                            <div class="col-5">
                                                <span>{{ number_format(getLpoVat($lpo->id), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>LPO Total :</strong> 
                                            </div>
                                            <div class="col-5">
                                                <span>{{ number_format(getLpoVat($lpo->id) + getLpoSubTotal($lpo->id), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>LPO Status :</strong> 
                                            </div>

                                            <div class="col-5">
                                                <span>
                                                    @if($lpo->local_purchase_order_status_id == App\LocalPurchaseOrderStatus::PENDING)
                                                    <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-info btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Pending</span>
                                                    @elseif($lpo->local_purchase_order_status_id == App\LocalPurchaseOrderStatus::CANCELED)
                                                        <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-orange btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Canceled</span>
                                                    @elseif($lpo->local_purchase_order_status_id == App\LocalPurchaseOrderStatus::REJECTED)
                                                        <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-danger btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Rejected</span>
                                                    @else
                                                        <span data-toggle="modal" data-target="#view-lpo-status-{{ $lpo->id }}" class="btn btn-success btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Approved</span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row form-group text-muted font-14">
                                            @if ($lpo->local_purchase_order_status_id == App\LocalPurchaseOrderStatus::APPROVED)
                                                <a href="{{ route('download-local-purchase-order', [$lpo]) }}" class="btn btn-xs btn-block btn-primary"><i class="mdi mdi-download"></i> Download LPO Document</a>
                                            @else
                                                <a href="javascript::void(0)" class="btn btn-xs btn-block btn-primary" disabled data-toggle="popover" data-placement="bottom" data-trigger="focus" title="" data-content="You are not permitted to downlaod an LPO if it has not been fully approved" data-original-title="LPO Not Approved"><i class="mdi mdi-download-off"></i> Download LPO Document</a>
                                                
                                            @endif
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                        <div class="col-9">
                            <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#items" role="tab">
                                        <span><i class="mdi mdi-format-list-bulleted text-danger"></i> LPO Items</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#approvals" role="tab" >
                                        <span><i class="mdi mdi-check-circle text-success"></i> LPO Approvals</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane show active" id="items" role="tabpanel">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    
                                    @if (canEditLPO($lpo->id) && $lpo->user_id == auth()->user()->id) 
                                        <button type="button" data-toggle="modal" data-target="#add-lpo-items" class="btn btn-sm btn-success" style="border-radius:0px"><i class=" fa fa-plus-circle"></i> Add more items</button>
                                        <hr>
                                    @endif
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-striped table-condensed table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Quantity</th>
                                                <th>Unit Price</th>
                                                <th>Line Total</th>
                                                @if (canEditLPO($lpo->id) && $lpo->user_id == auth()->user()->id)                                                    
                                                    <th>Actions</th>
                                                @endif
                                            </thead>
                                            <tbody>
                                                @foreach ($lpo->items as $count => $item)
                                                    <tr>
                                                        <td>{{ $count+1 }}</td>
                                                        <td>{{ $item->product->name }}</td>
                                                        <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                                                        <td>{{ number_format($item->unit_price, 2) }}</td>
                                                        <td>{{ number_format($item->quantity * $item->unit_price) }}</td>
                                                        
                                                        @if (canEditLPO($lpo->id) && $lpo->user_id == auth()->user()->id) 
                                                            <td>
                                                                <form action="{{ route('local-purchase-order-items.destroy', [$item]) }}" method="post">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button type="button" data-toggle="modal" data-target="#edit-lpo-item-{{ $item->id }}" class="btn btn-xs btn-info" style="border-radius:0px"><i class=" fa fa-edit"></i> Edit</button>
                                                                    <button type="submit" class="btn btn-xs btn-danger delete" style="border-radius:0px"><i class="fa fa-trash-alt"></i> Delete</button>
                                                                </form>

                                                                <div class="modal fade" id="edit-lpo-item-{{ $item->id }}" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px;">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title" align="center">Edit {{ $item->product->name }}</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form action="{{ route('local-purchase-order-items.update', [$item]) }}" method="POST">
                                                                                @csrf
                                                                                @method('PUT')
                                                                                <input type="hidden" name="lpo" value="{{ $lpo->id }}">
                                                                                <div class="modal-body">
                                                                                    <div class="row form-group">
                                                                                        <div class="col-2">
                                                                                            <label>Product</label>
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                            <select name="product_id" class="form-control">
                                                                                                @foreach ($products as $product)
                                                                                                    <option value="{{ $product->id}}" @if($product->id == $item->product_id) selected @endif>{{ $product->name }}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row form-group">
                                                                                        <div class="col-2">
                                                                                            <label>Quantity</label>
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                            <input type="number" name="quantity" class="form-control" value="{{ $item->quantity }}" required>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row form-group">
                                                                                        <div class="col-2">
                                                                                            <label>Price Per Unit</label>
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                            <input type="number" name="unit_price" class="form-control" value="{{ $item->unit_price }}" required>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-success waves-effect" >Edit Item</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="approvals" role="tabpanel">
                                    <approve-lpo :lpo="{{ $lpo->id }}"></approve-lpo>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-lpo-items" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px"  >
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add More Items - {{ $lpo->number }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">                                                              
                    <add-lpo-items :lpo={{ $lpo->id }}></add-lpo-items>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection