@extends('beautymail::templates.sunny')

@section('content')
    @include ('beautymail::templates.sunny.heading' , [
        'heading' => 'Its time to get started',
        'level' => 'h1',
    ])

    @include('beautymail::templates.sunny.contentStart')

        <p>Welcome to Carrymore Supermarket {{ $data['first_name']}}! You can now login to our POS System. </p>
        <p>To get started, use the following credentials</p>
        <p>
            Username: <b>{{ $data['email'] }}</b> or {{ $data['username'] }} <br>
            Password: {{ $data['plain_password'] }}
        </p>
        <p>Once you login, you will be required to change your password.</p>

    @include('beautymail::templates.sunny.contentEnd')

    @include('beautymail::templates.sunny.button', [
        'title' => 'Get Started',
        'link' => 'http://carrymore.app'
    ])

@stop