@extends('layouts.app', ['title' => 'Set Products Selling Price'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">Selling Price</li>
                    </ol>
                </div>
                <h4 class="page-title">Set products selling price</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Unit</th>
                            <th>Cost</th>
                            <th>Discount</th>
                            <th>Quantity</th>
                            <th>Notify</th>
                            <th>Vatable</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>{{ $product->unitOfMeasure->name }}</td>
                                <td>{{ number_format($product->cost, 2) }}</td>
                                <td>{{ $product->discount_allowed }}%</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->notify }}</td>
                                <td>
                                    @if ($product->vatable)
                                        <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px">Yes</span>
                                    @else
                                        <span class="btn btn-warning btn-rounded btn-xs" style="border-radius:15px">No</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <span class="btn btn-info btn-rounded btn-xs" style="border-radius:15px"  data-toggle="modal" data-target="#set-price-{{ $product->id }}"><i class="mdi mdi-coin"></i> Set Selling Price</span>

                                    <div class="modal fade" id="set-price-{{ $product->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <form method="POST" action="{{ url('set-selling-price') }}">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" align="center">Edit </h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">

                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection