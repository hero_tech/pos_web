@extends('layouts.app', ['title' => 'View Users'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div>
                <h4 class="page-title">Users</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
					<button type="button" class="btn btn-info width-xs btn-rounded" data-toggle="modal" data-target="#create-new-user-modal"><i class="mdi mdi-account-plus"></i> Create New User</button>
                </div>
                
                <div class="card-body">
                    <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Job Title</th>
                                <th>Supervisor</th>
                                <th>Role(s)</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($count = 1)
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $count++ }}</td>
                                    <td>{{ $user->full_name }}</td>
                                    <td>{{ $user->job_title }}</td>
                                    <td>
                                        @if($user->supervisor_id)
                                            {{ $user->supervisor->full_name }}
                                        @else
                                            <code>Not Assigned</code>
                                        @endcan
                                    </td>
                                    <td>
                                        @php( $userroles = $user->roles )
                                        @if(count($userroles) <= 0)
                                            <code>Not assigned	</code>
                                        @else
                                        <ol type="i">
                                            @foreach($userroles as $role)
                                                <li>{{ $role->name }}</li>
                                            @endforeach
                                        </ol>
                                        @endif
                                    </td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->mobile }}</td>
                                    <td>
                                        <form method="POST" action="{{ url('change-user-status/'.$user->id) }}">
                                            @csrf
                                            <input type="hidden" name="status" value="{{ $user->status }}">
                                            @if($user->status)
                                                <button type="submit" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Active</button>
                                            @else
                                                <button type="submit" class="btn btn-xs btn-warning"><i class="fa fa-ban"></i>Inactive</button>
                                            @endif
                                        </form>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ url('users', ['id' => $user->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            @can('update-users')
                                                <button type="button" data-toggle="modal" data-target="#edit-user{{ $user->id }}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i> Edit</button>
                                            @endcan
                                            @can('delete-users')
                                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                            @endcan
                                        </form>

                                        <div class="modal fade" id="edit-user{{ $user->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form method="POST" action="{{ url('users', ['id' => $user->id]) }}">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">Edit {{ $user->full_name }} info</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>First Name</label></div>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="first_name" class="form-control" placeholder="User first name" value="{{ $user->first_name }}" required>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>Last Name</label></div>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="last_name" class="form-control" placeholder="User last name" value="{{ $user->last_name }}" required>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>Email Adrress</label></div>
                                                                <div class="col-md-8">
                                                                    <input type="email" name="email" class="form-control" placeholder="User email address" value="{{ $user->email }}" required>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>User username</label></div>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="username" class="form-control" placeholder="User username" value="{{ $user->username }}" required>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>Mobile Number</label></div>
                                                                <div class="col-md-8">
                                                                    <input type="number" name="mobile" class="form-control" placeholder="User phone number" value="{{ $user->mobile }}" required>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>Department</label></div>
                                                                <div class="col-md-8">
                                                                    <select name="department_id" class="form-control" required="">
                                                                        @foreach($departments as $department)
                                                                            <option value="{{ $department->id }}" @if($user->department_id == $department->id) selected  @endif>{{ $department->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>Supervisor</label></div>
                                                                <div class="col-md-8">
                                                                    <select name="supervisor_id" class="form-control" required="">
                                                                        @foreach($activeUsers as $activeUser)
                                                                            <option value="{{ $activeUser->id }}" @if($user->supervisor_id == $activeUser->id) selected  @endif>{{ $activeUser->full_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-4"><label>Role(s)</label></div>
                                                                <div class="row col-md-8">
                                                                    @foreach($roles as $key => $role)
                                                                        <div class="col-md-6">
                                                                            @if(in_array($role->id, Arr::pluck($user->roles->toArray(), ['id'])))
                                                                                <div class="custom-control custom-checkbox checkbox-success">
                                                                                    <input type="checkbox" id="role{{$role->id}}" class="custom-control-input" name="roles[{{$role->id}}]" value="{{ $role->id }}" checked>
                                                                                    <label class="custom-control-label" for="role{{$role->id}}">{{ ucwords($role->name) }}</label>
                                                                                </div>
                                                                            @else
                                                                                <div class="custom-control custom-checkbox checkbox-success">
                                                                                    <input type="checkbox" id="role{{$role->id}}" class="custom-control-input" name="roles[{{$role->id}}]" value="{{ $role->id }}">
                                                                                    <label class="custom-control-label" for="role{{$role->id}}">{{ ucwords($role->name) }}</label>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary waves-effect waves-light ">Update user info</button>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="create-new-user-modal" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form method="POST" action="{{ url('users') }}">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" align="center">Create a new system user</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <input type="text" name="first_name" class="form-control" placeholder="User first name" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="last_name" class="form-control" placeholder="User last name" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <input type="email" name="email" class="form-control" placeholder="User email address" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="username" class="form-control" placeholder="User username" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <input type="number" name="mobile" class="form-control" placeholder="User phone number" required>
                            </div>
                            <div class="col-md-6">
                                <select name="department_id" class="form-control select2" required="">
                                    <option>-- Select Department --</option>
                                    @foreach($departments as $department)
                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <input type="text" name="job_title" class="form-control" placeholder="Job title" required>
                            </div>
                            <div class="col-md-6">
                                <select name="supervisor_id" class="form-control">
                                    <option value="">-- Select Supervisor --</option>
                                    @foreach ($activeUsers as $activeUser)
                                        <option value="{{ $activeUser->id }}">{{ $activeUser->full_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <span class="col-md-12 text-muted">Tick user role(s)</span>
                        </div>
                        <div class="row form-group">
                            @foreach($roles as $role)
                                <div class="col-md-4">
                                    <div class="custom-control custom-checkbox checkbox-success">
                                        <input type="checkbox" id="{{$role->name}}" class="custom-control-input" name="roles[{{$role->id}}]" value="{{ $role->id }}">
                                        <label class="custom-control-label" for="{{$role->name}}">{{ ucwords($role->name) }}</label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Create new user</button>
                    </div>
                </form>
    
            </div>
        </div>
    </div>

@endsection