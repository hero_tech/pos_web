@extends('layouts.app', ['title' => 'Custom Message Schedule'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bulk SMS</a></li>
                        <li class="breadcrumb-item active">Schedule Message</li>
                    </ol>
                </div>
                <h4 class="page-title">Create Custom Message Schedule</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('schedule-messages.index') }}" class="btn btn-success btn-sm" style="border-radius:0px;"><i class="mdi mdi-send-clock"></i> Custom Message Schedules</a>
                </div>

                <div class="card-body">                    
                    <create-custom-message-schedule></create-custom-message-schedule>
                </div>
            </div>
        </div>
    </div>

@endsection