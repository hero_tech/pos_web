@extends('layouts.app', ['title' => 'Chart of Accounts'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                        <li class="breadcrumb-item active">COA</li>
                    </ol>
                </div>
                <h4 class="page-title">Chart of Accounts</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#chart" role="tab">
                            <span><i class="mdi mdi-format-list-bulleted text-danger"></i> Chart of Accounts</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#acc-types" role="tab" >
                            <span><i class="mdi mdi-source-branch text-success"></i> Account Types</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#acc-sub-types" role="tab" >
                            <span><i class="mdi mdi-sitemap text-pink"></i> Account Types Categories</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="chart" role="tabpanel">
                        <button type="button" data-toggle="modal" data-target="#add-account" class="btn btn-sm btn-success" style="border-radius:0px"><i class=" fa fa-plus-circle"></i> Add Account</button>
                        <hr>
                        <table class="table table-striped table-condensed table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>GL Code</th>
                                <th>Account Name</th>
                                <th>Account Type</th>
                                <th>Account Category</th>
                                <th>Date Created</th>
                            </thead>
                            <tbody>
                                @foreach ($accounts as $account)
                                    <tr>
                                        <td>{{ $account->code }}</td>
                                        <td>{{ $account->name }}</td>
                                        <td>{{ $account->account->name }}</td>
                                        <td>
                                            @if ($account->account_sub_type_category_id == null)
                                                {{ $account->account->name }}
                                            @else
                                                {{ $account->category->name }}
                                            @endif
                                        </td>
                                        <td>{{ date('d-m-Y', strtotime($account->created_at)) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="acc-types" role="tabpanel">
                        
                            <table class="table table-striped table-condensed table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <th>#</th>
                                    <th>GL Code</th>
                                    <th>Name</th>
                                </thead>
                                <tbody>
                                    @foreach ($account_types as $key => $account)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $account->code }}</td>
                                            <td>{{ $account->name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>

                    <div class="tab-pane" id="acc-sub-types" role="tabpanel">
                        <button type="button" data-toggle="modal" data-target="#add-account-category" class="btn btn-sm btn-success" style="border-radius:0px"><i class=" fa fa-plus-circle"></i> Add Account Category</button>
                        <hr>
                        <table class="table table-striped table-condensed table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>#</th>
                                <th>Code</th>
                                <th>Account</th>
                                <th>Name</th>
                            </thead>
                            <tbody>
                                @foreach ($categories as $key => $category)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $category->code }}</td>
                                        <td>{{ $category->account->name }}</td>
                                        <td>{{ $category->name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-account" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px"  >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add Account</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <add-account></add-account>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-account-category" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px"  >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add Account</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <form action="{{ url('add-account-category') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="">Account Type</label>
                            </div>
                            <div class="col-9">
                                <select name="account_type_id" class="form-control" required>
                                    <option value="">-- Select Account Type --</option>
                                    @foreach ($account_types as $account_type)
                                        <option value="{{ $account_type->id }}">{{ $account_type->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="">Category Name</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="name" class="form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="">Category Code</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-info waves-effect">Add Account Category</button>
                    </div>
                </form>
                
            </div>
        </div>
    </div>

@endsection