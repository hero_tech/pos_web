@extends('layouts.app', ['title' => 'Departments'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                        <li class="breadcrumb-item active">Departments</li>
                    </ol>
                </div>
                <h4 class="page-title">Departments</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card table-responsive">
                <div class="card-header">
					<a href="#" data-toggle="modal" data-target="#create-new-department" class="btn btn-info width-xs btn-rounded"><i class="fa fa-plus-circle"></i> Create New Department</a>
                </div>

                <div class="card-body">
                    @if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
                    @endif
                    <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Date Created </th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($departments as $key => $department)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $department->name }}</td>
                                    <td>{{ date('d.M.Y', strtotime($department->created_at)) }}</td>
                                    <td>
                                        <form method="POST" action="{{ url('change-department-status/'.$department->id) }}">
                                            @csrf
                                            <input type="hidden" name="status" value="{{ $department->status }}">
                                            @if($department->status)
                                                <button type="submit" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Active</button>
                                            @else
                                                <button type="submit" class="btn btn-xs btn-warning"><i class="fa fa-ban"></i>Inactive</button>
                                            @endif
                                        </form>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ url('departments', ['id' => $department->id]) }}">
                                            @csrf
                                            @method('DELETE')
                                            @can('update-department')
                                                <button type="button" data-toggle="modal" data-target="#edit-department{{ $department->id }}" class="btn btn-xs btn-info"><i class="fa fa-edit"></i> Edit</button>
                                            @endcan
                                            @can('delete-department')
                                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                            @endcan
                                        </form>

                                        <div class="modal fade" id="edit-department{{ $department->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <form method="POST" action="{{ url('departments', ['id' => $department->id]) }}">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">Edit department ({{ $department->name }})</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row form-group">
                                                                <div class="col-md-4">
                                                                    <label>Department Name</label>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <input type="text" name="name" class="form-control" value="{{ $department->name }}" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary waves-effect waves-light ">Update department</button>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="create-new-department" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <form method="POST" action="{{ url('departments') }}">
                    @csrf
                    <div class="modal-header">
                        <h4 class="modal-title" align="center">Create a new department</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-md-4">
                                <label>Department Name</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="name" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Create new department</button>
                    </div>
                </form>
    
            </div>
        </div>
    </div>

@endsection