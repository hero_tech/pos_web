@extends('layouts.app', ['title' => 'View Unit Of Measures'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Inventory management</a></li>
                        <li class="breadcrumb-item active">Products</li>
                    </ol>
                </div>
                <h4 class="page-title">Unit of Measures</h4>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Created By</th>
                            <th>Date Created</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($unitOfMeasures as $k => $unitOfMeasure)
                            <tr>
                                <td>{{ $k+1 }}</td>
                                <td>{{ $unitOfMeasure->name }}</td>
                                <td>
                                    @if ($unitOfMeasure->status)
                                        <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px">Active</span>
                                    @else
                                        <span class="btn btn-warning btn-rounded btn-xs" style="border-radius:15px">Disabled</span>
                                    @endif
                                </td>
                                <td>{{ $unitOfMeasure->user->full_name }}</td>
                                <td>{{ date('d-m-Y', strtotime($unitOfMeasure->created_at)) }}</td>
                                <td class="text-center">
                                    <span class="btn btn-info btn-rounded btn-xs" style="border-radius:15px"  data-toggle="modal" data-target="#edit-supplier"><i class="fa fa-edit"></i> Edit</span>
                                    <span class="btn btn-danger btn-rounded btn-xs"  style="border-radius:15px" data-toggle="modal" data-target="#del-supplier"><i class="fa fa-trash"></i> Delete</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection