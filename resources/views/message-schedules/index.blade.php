@extends('layouts.app', ['title' => 'Schedule Message'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bulk SMS</a></li>
                        <li class="breadcrumb-item active">Schedule Message</li>
                    </ol>
                </div>
                <h4 class="page-title">Schedule Messages</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered " role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#fixed" role="tab">
                            <span><i class="mdi mdi-calendar-check text-info"></i> Fixed Schedules</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#custom" role="tab" >
                            <span><i class="mdi mdi-calendar-clock text-success"></i> Custom Schedules</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="fixed" role="tabpanel">
                        <div class="form-group">
                            <a href="{{ route('schedule-messages.create') }}" class="btn btn-success btn-sm" style="border-radius:0px;"><i class="mdi mdi-send-clock"></i> Create New Message Schedule</a>
                        </div>
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>#</th>
                                <th>Schedule Name</th>
                                <th>Frequency</th>
                                <th>Day</th>
                                <th>Time</th>
                                <th>Recipients & Msg</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($schedules as $key => $schedule)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $schedule->name }}</td>
                                        <td>{{ $schedule->frequency->name }}</td>
                                        <td>{{ $schedule->frequencyDay->name }}</td>
                                        <td>{{ date('H:i A', strtotime($schedule->message_time)) }}</td>
                                        <td>
                                            <code style="color:blue;cursor: pointer; " data-toggle="modal" data-target="#schedule-recipients-{{ $schedule->id }}">Show Recipients</code>

                                            <div class="modal fade" id="schedule-recipients-{{ $schedule->id }}" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top: 50px">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">{{ $schedule->name }} Recipients & Message</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row form-group">
                                                                <p class="col-12">
                                                                    <strong>Message:</strong> <br>
                                                                    {!! $schedule->message !!}
                                                                </p> 
                                                            </div>
                                                            @if(count($schedule->phoneNumbers) > 0)
                                                                <div class="row form-group">
                                                                    <p class="col-12">
                                                                        <strong>Phone Numbers: </strong><small>(Not in contacts)</small>
                                                                    </p>                                                                    
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-12">
                                                                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                                                            <thead>
                                                                                <th>#</th>
                                                                                <th>Phone Number</th>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach ($schedule->phoneNumbers as $countNos => $phoneNumber)
                                                                                    <tr>
                                                                                        <td>{{ $countNos + 1 }}</td>
                                                                                        <td>{{ $phoneNumber->phone }}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            @if(count($schedule->contacts) > 0)
                                                                <hr>
                                                                <div class="row form-group">
                                                                    <p class="col-12">
                                                                        <strong>Contacts: </strong>
                                                                    </p>                                                                    
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-12">
                                                                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                                                            <thead>
                                                                                <th>#</th>
                                                                                <th>Name</th>
                                                                                <th>Phone Number</th>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach ($schedule->contacts as $countCncts => $contact)
                                                                                    <tr>
                                                                                        <td>{{ $countCncts + 1 }}</td>
                                                                                        <td>{{ $contact->full_name }}</td>
                                                                                        <td>{{ $contact->phone }}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                            @if(count($schedule->contactGroups) > 0)
                                                                <hr>
                                                                <div class="row form-group">
                                                                    <p class="col-12">
                                                                        <strong>Contacts Groups: </strong>
                                                                    </p>                                                                    
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-12">
                                                                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                                                            <thead>
                                                                                <th>#</th>
                                                                                <th>Group Name</th>
                                                                                <th>Contacts in Group</th>
                                                                            </thead>
                                                                            <tbody>
                                                                                @foreach ($schedule->contactGroups as $countGrps => $group)
                                                                                    <tr>
                                                                                        <td>{{ $countGrps + 1 }}</td>
                                                                                        <td>{{ $group->name }}</td>
                                                                                        <td>{{ count($group->contacts) }} contacts</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ url('change-message-schedule-status/'.$schedule->id) }}">
                                                @csrf
                                                <input type="hidden" name="status" value="{{ $schedule->status ? 1 : 0  }}">
                                                @if($schedule->status)
                                                    <button type="submit" class="btn btn-xs btn-rounded btn-success"><i class="fa fa-check"></i> Active</button>
                                                @else
                                                    <button type="submit" class="btn btn-xs btn-rounded btn-orange"><i class="fa fa-ban"></i> Inactive</button>
                                                @endif
                                            </form>  
                                        </td>
                                        <td>
                                            <form action="{{ route('schedule-messages.destroy', [$schedule]) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <a href="{{ route('schedule-messages.show', [$schedule]) }}" class="btn btn-xs btn-info" style="border-radius:0px"><i class=" fa fa-edit"></i> Show More</a>
                                                <button type="submit" class="btn btn-xs btn-danger delete" style="border-radius:0px"><i class="fa fa-trash-alt"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="custom" role="tabpanel">
                        <div class="form-group">
                            <a href="{{ route('custom-message-schedules.create') }}" class="btn btn-success btn-sm" style="border-radius:0px;"><i class="mdi mdi-send-clock"></i> Create Custom Message Schedule</a>
                        </div>
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>#</th>
                                <th>Schedule Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Recipients & Msg</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($customSchedules as $key => $customSchedule)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $customSchedule->name }}</td>
                                    <td>{{ date('d-M-Y', strtotime($customSchedule->message_date)) }}</td>
                                    <td>{{ date('H:i A', strtotime($customSchedule->message_time)) }}</td>
                                    <td>
                                        <code style="color:blue;cursor: pointer; " data-toggle="modal" data-target="#schedule-recipients-{{ $customSchedule->id }}">Show Recipients</code>

                                        <div class="modal fade" id="schedule-recipients-{{ $customSchedule->id }}" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top: 50px">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" align="center">{{ $customSchedule->name }} Recipients & Message</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row form-group">
                                                            <p class="col-12">
                                                                <strong>Message:</strong> <br>
                                                                {!! $customSchedule->message !!}
                                                            </p> 
                                                        </div>
                                                        @if(count($customSchedule->phoneNumbers) > 0)
                                                            <div class="row form-group">
                                                                <p class="col-12">
                                                                    <strong>Phone Numbers: </strong><small>(Not in contacts)</small>
                                                                </p>                                                                    
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-12">
                                                                    <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                                                        <thead>
                                                                            <th>#</th>
                                                                            <th>Phone Number</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($customSchedule->phoneNumbers as $countNos => $phoneNumber)
                                                                                <tr>
                                                                                    <td>{{ $countNos + 1 }}</td>
                                                                                    <td>{{ $phoneNumber->phone }}</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        @endif

                                                        @if(count($customSchedule->contacts) > 0)
                                                            <hr>
                                                            <div class="row form-group">
                                                                <p class="col-12">
                                                                    <strong>Contacts: </strong>
                                                                </p>                                                                    
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-12">
                                                                    <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                                                        <thead>
                                                                            <th>#</th>
                                                                            <th>Name</th>
                                                                            <th>Phone Number</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($customSchedule->contacts as $countCncts => $contact)
                                                                                <tr>
                                                                                    <td>{{ $countCncts + 1 }}</td>
                                                                                    <td>{{ $contact->full_name }}</td>
                                                                                    <td>{{ $contact->phone }}</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        @endif

                                                        @if(count($customSchedule->contactGroups) > 0)
                                                            <hr>
                                                            <div class="row form-group">
                                                                <p class="col-12">
                                                                    <strong>Contacts Groups: </strong>
                                                                </p>                                                                    
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-12">
                                                                    <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                                                        <thead>
                                                                            <th>#</th>
                                                                            <th>Group Name</th>
                                                                            <th>Contacts in Group</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($customSchedule->contactGroups as $countGrps => $group)
                                                                                <tr>
                                                                                    <td>{{ $countGrps + 1 }}</td>
                                                                                    <td>{{ $group->name }}</td>
                                                                                    <td>{{ count($group->contacts) }} contacts</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <form method="POST" action="{{ url('change-custom-message-schedule-status/'.$customSchedule->id) }}">
                                            @csrf
                                            <input type="hidden" name="status" value="{{ $customSchedule->status ? 1 : 0  }}">
                                            @if($customSchedule->status)
                                                <button type="submit" class="btn btn-xs btn-rounded btn-success"><i class="fa fa-check"></i> Active</button>
                                            @else
                                                <button type="submit" class="btn btn-xs btn-rounded btn-orange"><i class="fa fa-ban"></i> Inactive</button>
                                            @endif
                                        </form>  
                                    </td>
                                    <td>
                                        <form action="{{ route('custom-message-schedules.destroy', [$customSchedule]) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <a href="{{ route('custom-message-schedules.show', [$customSchedule]) }}" class="btn btn-xs btn-info" style="border-radius:0px"><i class=" fa fa-edit"></i> Show More</a>
                                            <button type="submit" class="btn btn-xs btn-danger delete" style="border-radius:0px"><i class="fa fa-trash-alt"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>                                    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection