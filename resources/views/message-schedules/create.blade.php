@extends('layouts.app', ['title' => 'Schedule Message'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bulk SMS</a></li>
                        <li class="breadcrumb-item active">Schedule Message</li>
                    </ol>
                </div>
                <h4 class="page-title">Create Message Schedule</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <create-message-schedule></create-message-schedule>
            </div>
        </div>
    </div>

@endsection