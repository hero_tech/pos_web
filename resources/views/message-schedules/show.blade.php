@extends('layouts.app', ['title' => 'Scheduled Message'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bulk SMS</a></li>
                        <li class="breadcrumb-item active">Schedule Message</li>
                    </ol>
                </div>
                <h4 class="page-title">Schedule Messages - {{ $schedule->name }}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="card-header">
                    <a href="{{ route('schedule-messages.index') }}" class="btn btn-info btn-sm"><i class="mdi mdi-view-list"></i> View All Message Schedule(s)</a>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="card-box shadow-none border border-aqua">
                            <div class="member-card">
                                <div class="avatar-xl member-thumb mb-3 mx-auto d-block">
                                    <img src="{{ asset('assets/images/schedule_msg.png') }}" class="rounded-circle img-thumbnail" alt="Schedule">
                                </div>

                                <div class="text-center">
                                    <h5 class="font-18 mb-1">{{ $schedule->name }}</h5>
                                </div>

                                <hr/>
                                <div class="">
                                    <div class="row form-group text-muted font-14 ">
                                        <div class="col-7">
                                            <strong>Frequency</strong> 
                                        </div>
                                        <div class="col-5">
                                            <span>{{ $schedule->frequency->name }}</span>
                                        </div>
                                    </div>
                                    <div class="row form-group text-muted font-14 ">
                                        <div class="col-7">
                                            <strong>Frequency Day</strong> 
                                        </div>
                                        <div class="col-5">
                                            <span>{{ $schedule->frequencyDay->name }}</span>
                                        </div>
                                    </div>
                                    <div class="row form-group text-muted font-14 ">
                                        <div class="col-7">
                                            <strong>Message Time</strong> 
                                        </div>
                                        <div class="col-5">
                                            <span>{{ date('H:i A', strtotime($schedule->message_time)) }}</span>
                                        </div>
                                    </div>
                                    <div class="row form-group text-muted font-14 ">
                                        <div class="col-7">
                                            <strong>Schedule Status</strong> 
                                        </div>
                                        <div class="col-5">
                                            <span>
                                                <form method="POST" action="{{ url('change-message-schedule-status/'.$schedule->id) }}">
                                                    @csrf
                                                    <input type="hidden" name="status" value="{{ $schedule->status ? 1 : 0  }}">
                                                    @if($schedule->status)
                                                        <button type="submit" class="btn btn-xs btn-rounded btn-success"><i class="fa fa-check"></i> Active</button>
                                                    @else
                                                        <button type="submit" class="btn btn-xs btn-rounded btn-orange"><i class="fa fa-ban"></i> Inactive</button>
                                                    @endif
                                                </form>                                                
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-9">
                        <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#message" role="tab">
                                    <span><i class="mdi mdi-message text-danger"></i> Details</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#phones" role="tab" >
                                    <span><i class="mdi mdi-phone-alert text-purple"></i> Phone Numbers</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#contacts" role="tab" >
                                    <span><i class="mdi mdi-contact-phone text-success"></i> Contatcs</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#groups" role="tab" >
                                    <span><i class="mdi mdi-account-group text-pink"></i> Groups</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane show active" id="message" role="tabpanel">
                                <button class="btn btn-primary width-xs" style="border-radius: 0px" data-toggle="modal" data-target="#edit-schedule"><i class="mdi mdi-calendar-edit"></i> Edit Schedule</button>
                                <hr>
                                <div class="row form-group">
                                    <div class="col-2">
                                        <label>Message:</label>
                                    </div>
                                    <div class="col-10">
                                        {!! $schedule->message !!}
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="phones" role="tabpanel">
                                <button class="btn btn-success width-xs" style="border-radius: 0px"  data-toggle="modal" data-target="#add-phone-numbers"><i class="mdi mdi-phone-plus"></i> Add Phone Numbers</button>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (count($schedule->phoneNumbers) > 0)
                                    <hr>
                                    <div class="col-12">                                        
                                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                            <thead>
                                                <th>#</th>
                                                <th>Phone Number</th>
                                                <th>Actions</th>
                                            </thead>
                                            <tbody>
                                                @foreach ($schedule->phoneNumbers as $countNos => $phoneNumber)
                                                    <tr>
                                                        <td>{{ $countNos + 1 }}</td>
                                                        <td>{{ $phoneNumber->phone }}</td>
                                                        <td>
                                                            <form action="{{ route('message-scheduling-phone-numbers.destroy', [$phoneNumber]) }}" method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button type="button" data-toggle="modal" data-target="#edit-phone-number-{{ $phoneNumber->id }}" class="btn btn-xs btn-info" style="border-radius:0px"><i class=" fa fa-edit"></i> Edit</button>
                                                                <button type="submit" class="btn btn-xs btn-danger delete" style="border-radius:0px"><i class="fa fa-trash-alt"></i> Delete</button>
                                                            </form>

                                                            <div class="modal fade" id="edit-phone-number-{{ $phoneNumber->id }}" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px;">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" align="center">Edit Phone Number</h4>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{ route('message-scheduling-phone-numbers.update', [$phoneNumber]) }}" method="POST">
                                                                            @csrf
                                                                            @method('PUT')
                                                                            <div class="modal-body">
                                                                                <div class="row form-group">
                                                                                    <div class="col-2">
                                                                                        <label>Phone No.</label>
                                                                                    </div>
                                                                                    <div class="col-10">
                                                                                        <input type="text" name="phone" value="{{ $phoneNumber->phone }}" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn btn-success waves-effect" >Update</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @else
                                    <div class="row-form-group">
                                        <div class="col-12">
                                            <p>No phone numbers found</p>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="tab-pane" id="contacts" role="tabpanel">
                                <button class="btn btn-success width-xs" style="border-radius: 0px" data-toggle="modal" data-target="#add-contacts"><i class="mdi mdi-phone-plus"></i> Add Contacts</button>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @if (count($schedule->contacts) > 0)
                                    <hr>
                                    <div class="col-12">                                        
                                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                            <thead>
                                                <th>#</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Phone Number</th>
                                                <th>Actions</th>
                                            </thead>
                                            <tbody>
                                                @foreach ($schedule->contacts as $countCnts => $contact)
                                                    <tr>
                                                        <td>{{ $countCnts + 1 }}</td>
                                                        <td>{{ $contact->first_name }}</td>
                                                        <td>{{ $contact->last_name }}</td>
                                                        <td>{{ $contact->phone }}</td>
                                                        <td>
                                                            <form action="{{ url('detach-contacts-from-schedule') }}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="schedule_id" value="{{ $schedule->id }}">
                                                                <input type="hidden" name="contact_id" value="{{ $contact->id }}">
                                                                <button type="submit" class="btn btn-xs btn-danger delete" style="border-radius:0px"><i class="fa fa-trash-alt"></i> Delete</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                @else
                                <div class="row-form-group">
                                    <div class="col-12">
                                        <p>No contacts found</p>
                                    </div>
                                </div>
                                @endif
                            </div>

                            <div class="tab-pane" id="groups" role="tabpanel">
                                <button class="btn btn-success width-xs" style="border-radius: 0px" data-toggle="modal" data-target="#add-groups"><i class="mdi mdi-account-group"></i> Add Groups</button>
                               
                                @if (count($schedule->contactGroups) > 0)
                                    <hr>
                                    <div class="col-12">                                        
                                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0;" width="100%">
                                            <thead>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Contacts</th>
                                                <th>Actions</th>
                                            </thead>
                                            <tbody>
                                                @foreach ($schedule->contactGroups as $countGrps => $group)
                                                    <tr>
                                                        <td>{{ $countGrps + 1 }}</td>
                                                        <td>{{ $group->name }}</td>
                                                        <td>{{ count($group->contacts) }}</td>
                                                        <td>
                                                            <form action="{{ url('detach-group-from-schedule') }}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="schedule_id" value="{{ $schedule->id }}">
                                                                <input type="hidden" name="group_id" value="{{ $group->id }}">
                                                                <button type="submit" class="btn btn-xs btn-danger delete" style="border-radius:0px"><i class="fa fa-trash-alt"></i> Delete</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                @else
                                <div class="row-form-group">
                                    <div class="col-12">
                                        <p>No groups found</p>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-schedule" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Edit Message Schedule </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <edit-message-schedule :schedule_id="{{ $schedule->id }}"></edit-message-schedule>

            </div>
        </div>
    </div>

    <div class="modal fade" id="add-phone-numbers" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add phone numbers to message schedule </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('message-scheduling-phone-numbers') }}" method="POST">
                    @csrf
                    <input type="hidden" name="schedule_id" value="{{ $schedule->id }}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <p>Comma separated phone numbers</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <textarea name="phone_numbers" id="" class="form-control" rows="5" placeholder="0712345678, 0700000000" value="{{ old('phone_numbers') }}" required></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect"> Add Phone Numbers</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-contacts" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add contacts to message schedule </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <add-message-schedule-contacts :schedule_id="{{ $schedule->id }}"></add-message-schedule-contacts>

            </div>
        </div>
    </div>

    <div class="modal fade" id="add-groups" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add groups to message schedule </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <add-message-schedule-groups :schedule_id="{{ $schedule->id }}"></add-message-schedule-groups>

            </div>
        </div>
    </div>

@endsection