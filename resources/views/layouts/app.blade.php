<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CarryMore Suppermarket - {{ $title }} </title>
    <link rel="shortcut icon" href="{{ asset('assets/images/carrymore_logo.png') }}">
    
    @include('layouts.common.styles')
    
</head>
<body data-layout="horizontal">
    <div id="wrapper">
        <header id="topnav">
            @include('layouts.common.topbar')
            @include('layouts.common.navbar')
        </header>
        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                <div id="app" class="container-fluid">
                    @yield('content')
                </div>
            </div>
            <!-- Footer Start -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <script>document.write(new Date().getFullYear())</script> &copy; System developed by <a href="https://afrinettelecom.com" target="_blank">Afrinet Telecom Ltd</a>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->
        </div>
    </div>
    @include('layouts.common.scripts')
    @yield('scripts')
</body>
</html>
