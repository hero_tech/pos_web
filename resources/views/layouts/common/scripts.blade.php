<script src="{{ asset('js/app.js') }}"></script>
<!-- Vendor js -->
<script src="{{ asset('assets/js/vendor.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.min.js') }}"></script>
<!-- Datatable plugin js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('assets/libs/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/libs/pdfmake/vfs_fonts.js') }}"></script>
<!-- Toastr js -->
<script src="{{ asset('assets/libs/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('assets/libs/dropify/dropify.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/libs/tooltipster/tooltipster.bundle.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>

<script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
{{-- Am Charts --}}
<script src="{{ asset('assets/libs/amcharts4/core.js') }}"></script>
<script src="{{ asset('assets/libs/amcharts4/charts.js') }}"></script>
<script src="{{ asset('assets/libs/amcharts4/themes/material.js') }}"></script>
<script src="{{ asset('assets/libs/amcharts4/themes/animated.js') }}"></script>
<script src="{{ asset('assets/libs/amcharts4/themes/dataviz.js') }}"></script>


<script src="{{  asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

@if(session('success'))
<script type="text/javascript">
    $(document).ready(function(){
        var message = {!! json_encode(session('success')) !!};
        toastr.success(message, "Success!", {   
            "closeButton": true,        
            "progressBar": true,
            "onclick": null,
            "positionClass": "toast-top-right",
            "showDuration": "500",
            "hideDuration": "2000",
            "timeOut": "8000",
            "extendedTimeOut": "2000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    });             
</script>
@elseif(session('error'))
<script type="text/javascript">
    $(document).ready(function(){
        var message = {!! json_encode(session('error')) !!};
        toastr.warning(message, "Warning!", {
            "closeButton": true,
            "progressBar": true,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "2000",
            "timeOut": "8000",
            "extendedTimeOut": "2000",
            "positionClass": "toast-top-right",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    });             
</script>
@elseif(session('danger'))
<script type="text/javascript">
    $(document).ready(function(){
        var message = {!! json_encode(session('danger')) !!};
        toastr.error(message, "Error.", {
            "closeButton": true,
            "progressBar": true,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "2000",
            "positionClass": "toast-top-right",
            "timeOut": "8000",
            "extendedTimeOut": "2000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    });             
</script>
@endif

<script>

    // (".tooltip-hover").tooltipster();

	$(document).ready(function() {
        $(".datatables_responsive").DataTable({
            "pagingType": "full_numbers"
        });


        $("input[name='demo3_21']").TouchSpin({initval:40})

        $(".select2").select2({
            width: '100%',
            theme: 'classic'
        });

        $("textarea#textarea").maxlength({
            alwaysShow:!0,
            warningClass:"badge badge-success",
            limitReachedClass:"badge badge-danger"
        });

    });

    $('#myTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    // store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });

    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#myTab a[href="' + hash + '"]').tab('show');

    $(".dropify").dropify({
        messages:{
            default:"Drag and drop a file here or click",
            replace:"Drag and drop or click to replace",
            remove:"Remove",
            error:"Ooops, something wrong appended."
        },
        error:{
            fileSize:"The file size is too big (1M max)."
        }
    });

    $('.delete').on('click', function(e) {
        e.preventDefault();
        var form = $(this).parents('form');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this action!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                form.submit()   
            }
        })
    });
</script>