<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="javascript::void(0)"> <i class="mdi mdi-view-dashboard"></i>Dashboard</a>
                    <ul class="submenu">
                        <li><a href="{{ route('home') }}">Sales Dashboard</a></li>
                        <li><a href="javascript::void(0)">Analytics Dashboard</a></li>
                        <li><a href="javascript::void(0)">CRM Dashboard</a></li>
                    </ul>
                </li>


                @can('view-pos')
                    <li class="has-submenu">
                        <a href="{{ url('point-of-sales') }}">
                            <i class="mdi mdi-basket"></i>POS
                        </a>
                    </li>
                @endcan
                
                @can('view-store')
                    <li class="has-submenu">
                        <a href="javascript::void(0)">
                            <i class="mdi mdi-store"></i>Inventory Mngmt
                        </a>
                        <ul class="submenu">
                            @can('register-products')
                                <li><a href="{{ route('products.create') }}">Register Products</a></li>
                            @endcan

                            @can('view-products')
                                <li><a href="{{ url('products') }}">View Products</a></li>
                            @endcan

                            @can('view-products-categories')
                                <li><a href="{{ url('product-categories') }}">Product Categories</a></li>
                            @endcan

                            @can('view-unit-of-measure')
                                <li><a href="{{ url('unit-of-measures') }}">Unit of Measures</a></li>
                            @endcan
                            
                            @can('receive-products')
                                <li><a href="{{ url('receive-products') }}">Receive Products</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan

                @can('view-bulk-sms')
                    <li class="has-submenu">
                        <a href="javascript::void(0)">
                            <i class="mdi mdi-message-text"></i>Bulk SMS
                        </a>
                        <ul class="submenu">
                            @can('send-messages')<li><a href="{{ url('send-messages') }}">Send Message</a></li>@endcan
                            @can('schedule-messages')<li><a href="{{ url('schedule-messages') }}">Schedule Messages</a></li>@endcan
                            <li><a href="{{ url('bulk-sms-contacts') }}">Contacts</a></li>
                        </ul>
                    </li>
                @endcan

                @can('view-suppliers-management')
                    <li class="has-submenu">
                        <a href="javascript::void(0);">
                            <i class="mdi mdi-truck-delivery"></i>Supplier Mngmt
                        </a>
                        <ul class="submenu">
                            @can('create-suppliers')
                                <li><a href="{{ route('suppliers.create') }}">Create Suppliers</a></li>                                
                            @endcan

                            @can('view-suppliers')
                                <li><a href="{{ url('suppliers') }}">View Suppliers</a></li>                                
                            @endcan
                            <li><a href="javascript::void(0)">Supplier Statememts</a></li>
                        </ul>
                    </li>
                @endcan

                @can('view-customers-management')
                    <li class="has-submenu">
                        <a href="javascript::void(0)"> <i class="mdi mdi-account-card-details"></i>Customer Mngmt</a>
                        <ul class="submenu">
                            @can('create-customers')
                                <li><a href="{{ route('customers.create') }}">Create Customer</a></li>
                            @endcan

                            @can('view-customers')
                                <li><a href="{{ url('customers') }}">View Customers</a></li>
                            @endcan
                            <li><a href="javascript::void(0)">Customers Loyalty</a></li>
                            <li><a href="{{ url('customer-vouchers') }}">Customers Vouchers</a></li>
                        </ul>
                    </li>
                @endcan
                
                @can('view-accounts')
                    <li class="has-submenu">
                        <a href="javascript::void(0)"> <i class="mdi mdi-bank"></i>Accounts </a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li><a href="{{ url('invoices') }}">Invoices</a></li>
                                    <li><a href="javascript::void(0)">Petty Cash</a></li>
                                    <li><a href="javascript::void(0)">Cash Flow Statememt</a></li>
                                    <li><a href="javascript::void(0)">Income Statement</a></li>
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><a href="javascript::void(0)">Bank Statement</a></li>
                                    <li><a href="javascript::void(0)">Bank Reconcilation</a></li>
                                    <li><a href="javascript::void(0)">Balance Sheet</a></li>
                                    @can('view-charts-of-account')<li><a href="{{ url('chart-of-accounts') }}">Charts of Account</a></li>@endcan
                                </ul>
                            </li>
                        </ul>
                    </li>
                @endcan

                @can('view-procurement')
                    <li class="has-submenu">
                        <a href="javascript::void(0)"> <i class="mdi mdi-forklift"></i>Procurement
                        </a>
                        <ul class="submenu">
                            @can('view-lpos')<li><a href="{{ url('local-purchase-orders') }}">Local Purchase Orders</a></li>@endcan
                            @can('view-srns')<li><a href="{{ url('store-requisition-notes') }}">Store Requisition Notes</a></li>@endcan
                            <li><a href="{{ url('good-issue-notes') }}">Good Issue Note</a></li>
                        </ul>
                    </li>
                @endcan

                @can('view-settings')
                    <li class="has-submenu">
                        <a href="javascript::void(0)"> <i class="mdi mdi-settings"></i>Settings</a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    @can('view-users')<li><a href="{{ url('users') }}">Users Management</a></li>@endcan
                                    @can('view-roles')<li><a href="{{ url('roles') }}">Roles Management</a></li>@endcan
                                    @can('view-departments')<li><a href="{{ url('departments') }}">Departments</a></li>@endcan
                                </ul>
                            </li>
                            <li>
                                <ul>
                                    <li><a href="{{ url('system-settings') }}">System Settings</a></li>
                                    <li><a href="javascript::void(0)">System Backup</a></li>
                                    <li><a href="{{ url('my-account') }}">My Account</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                @endcan
            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->