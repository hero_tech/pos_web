<!-- Topbar Start -->
<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">

            <li class="dropdown notification-list">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle nav-link">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>

            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" href="{{ url('my-account') }}" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{ auth()->user()->avatar }}" alt="user-image" class="rounded-circle">
                    {{ auth()->user()->full_name }}
                </a>
            </li>

            <li class="dropdown notification-list">
                <!-- item-->
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <i class="mdi mdi-logout-variant"></i>
                    <span>Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </li>

        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="{{ url('/home') }}" class="logo text-center">
                <span class="logo-lg">
                    <img src="{{ asset('assets/images/carrymore_logo.png') }}" alt="" height="60">
                    <!-- <span class="logo-lg-text-light">Zircos</span> -->
                </span>
                <span class="logo-sm">
                    <!-- <span class="logo-sm-text-dark">Z</span> -->
                    <img src="{{ asset('assets/images/carrymore_logo.png') }}" alt="" height="60">
                </span>
            </a>
        </div>


        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">

            <li class="d-none d-sm-block">
                <form class="app-search">
                    <div class="app-search-box">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search...">
                            <div class="input-group-append">
                                <button class="btn" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!-- end Topbar -->