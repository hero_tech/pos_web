@extends('layouts.app', ['title' => 'Create Customers'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Customer management</a></li>
                        <li class="breadcrumb-item active">Customers</li>
                    </ol>
                </div>
                <h4 class="page-title">Create customers</h4>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="{{ url('customers') }}" method="post" class="offset-1">
                    @csrf
                    <div class="row form-group">
                        <div class="col-2">
                            <label>First Name *</label>
                        </div>
                        <div class="col-3">
                            <input type="text" name="first_name" class="form-control" required>
                        </div>
                        <div class="col-2">
                            <label>Last Name *</label>
                        </div>
                        <div class="col-3">
                            <input type="text" name="last_name" class="form-control" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label>Phone Number</label>
                        </div>
                        <div class="col-3">
                            <input type="text" name="phone" class="form-control">
                            <small>Format: 07xx xxx xxx</small>
                        </div>
                        <div class="col-2">
                            <label>Id Number</label>
                        </div>
                        <div class="col-3">
                            <input type="number" name="id_number" class="form-control" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label>Card Number</label>
                        </div>
                        <div class="col-3">
                            <input type="number" name="code" class="form-control" >
                        </div>
                        <div class="col-2">
                            <label>Email Address</label>
                        </div>
                        <div class="col-3">
                            <input type="email" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2 offset-4">
                            <button type="submit" class="btn btn-success"><i class="mdi mdi-account-plus"></i> Register Customer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection