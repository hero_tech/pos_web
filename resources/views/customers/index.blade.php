@extends('layouts.app', ['title' => 'View Customers'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Customer management</a></li>
                        <li class="breadcrumb-item active">Customers</li>
                    </ol>
                </div>
                <h4 class="page-title">View customers</h4>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>ID</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($customers as $k=>$customer)
                        <tr>
                            <td>{{ $k+1 }}</td>
                            <td>
                                @if ($customer->code == null)
                                    <code style="cursor:pointer;" data-toggle="modal" data-target="#set-code-{{ $customer->id }}">Not set</code>
                                @else
                                    <span style="cursor:pointer;" data-toggle="modal" data-target="#set-code-{{ $customer->id }}">{{ $customer->code }}</span>                                        
                                @endif

                                <div class="modal fade" id="set-code-{{ $customer->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form method="POST" action="{{ route('update-customer-code', [$customer]) }}">
                                                @csrf
                                                <div class="modal-header">
                                                    <h4 class="modal-title" align="center">Set {{ $customer->full_name }} Bar Code</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <input class="form-control" name="code" value="{{ $customer->code }}" required>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary waves-effect waves-light ">Set new code</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>    
                            </td>
                            <td>{{ $customer->full_name }}</td>
                            <td>{{ $customer->phone }}</td>
                            <td>{{ $customer->email }}</td>
                            <td>{{ $customer->id_number }}</td>
                            <td>
                                @if ($customer->status)
                                    <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px">Active</span>
                                @else
                                    <span class="btn btn-warning btn-rounded btn-xs" style="border-radius:15px">Disabled</span>
                                @endif
                            </td>
                            <td>{{ date('d-m-Y', strtotime($customer->created_at)) }}</td>
                            <td class="text-center">
                                <span class="btn btn-info btn-rounded btn-xs" style="border-radius:15px"  data-toggle="modal" data-target="#edit-supplier"><i class="fa fa-edit"></i> Edit</span>
                                <span class="btn btn-danger btn-rounded btn-xs"  style="border-radius:15px" data-toggle="modal" data-target="#del-supplier"><i class="fa fa-trash"></i> Delete</span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @endsection