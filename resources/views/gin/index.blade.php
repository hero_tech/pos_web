@extends('layouts.app', ['title' => 'GIN'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">GIN</li>
                    </ol>
                </div>
                <h4 class="page-title">Good Issue Notes</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>SRN No</th>
                            <th>Requested By</th>
                            <th>Status</th>
                            <th>Comment</th>
                            <th>Date Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($gins as $key => $gin)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td><a href="{{ route('store-requisition-notes.show', [$gin->srn]) }}"><code>{{ $gin->srn->number }}</code></a></td>
                                <td>{{ $gin->srn->user->full_name }}</td>
                                <td>
                                    @if ($gin->consent == null && $gin->user_id == auth()->user()->id)
                                        <span data-toggle="modal" data-target="#view-gin-status-{{ $gin->id }}" class="btn btn-info btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Pending</span>   
                                    @else
                                        @if ($gin->consent)
                                            <span class="btn btn-success btn-rounded btn-xs">Accepted</span>
                                        @else
                                            <span class="btn btn-danger btn-rounded btn-xs">Rejected</span>                                            
                                        @endif
                                    @endif
                                    <div class="modal fade" id="view-gin-status-{{ $gin->id }}" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top: 50px">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" align="center">{{ $gin->srn->number }} Good Issue Note Consent</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="{{ url('good-issue-notes', ['id' => $gin->id]) }}" method="POST">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="modal-body">
                                                        <div class="row form-group">                                                                 
                                                            <label class="col-6">
                                                                <input type="radio" class="option-input radio" name="consent" value="true"/>
                                                                <span style="color:green">Accept GIN</span>
                                                            </label>
                                                            <label class="col-6">
                                                                <input type="radio" class="option-input-reject radio" name="consent" value="false"/>
                                                                <span style="color:red">Reject GIN</span>
                                                            </label>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-12">
                                                                <textarea name="comment" rows="3" class="form-control" placeholder="Reason for rejecting GIN"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-info waves-effect">Acknowledge Consent</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @if ($gin->comment == null)
                                        <code>N/A</code>
                                    @else
                                        {{ $gin->comment }}
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y H:i A', strtotime($gin->created_at)) }}</td>
                                <td>
                                    @if ($gin->consent != null && $gin->consent)
                                        <a href="{{ route('download-good-issue-note', [$gin]) }}" class="btn btn-xs btn-primary"><i class="mdi mdi-download"></i> Download GIN</a>            
                                    @else
                                        <button class="btn btn-xs btn-orange" data-toggle="popover" data-placement="bottom" data-trigger="focus" data-content="You are not permitted to downlaod GIN if it has not been accepted" data-original-title="GIN Not Accepted"><i class="mdi mdi-download-off"></i> Download GIN</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection