<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ public_path('assets/css/bootstrap.min.css') }}">
    <title>Invoice - {{ $data->invoice_no }}</title>

    <style>
        @page{
            margin-top: 70px;
        }
        body {
            font-family: "Cambria";
            font-size: 14px;
            background-color: white;
            color: black
        }
        .logo {
            width: 30%;
            height: 70px;
            float: left;
        }

        .signature {
            width: 100px;
            height: 50px;
        }

        table, tr, td {
            padding: 0px;
            color: black
        }
        .header{
            height: 80px;
        }
        .invoice{
            width: 30%;
            float: left;
            text-align: center;
            margin-top: 10px;
            font-size: 30px;
            font-weight: bold
        }
        .status {
            width: 30%;
            float: left;
        }
        .logo {
            width: 30%;
            height: 70px;
            float: left;
        }
        .supplier .vendor{
            margin-bottom: 10px;
        }
        .supplier .details {
            width: 50%;
            float:left;
        }
        .supplier .shipto {
            width: 50%;
            float:right;
        }
        .info{
            margin-top: 5px;
        }
        
        .page_break { page-break-before: always; }
        .no_page_break { page-break-before: avoid; }
        hr{
            margin: 0px;
            color: grey;
            border-bottom: 1px solid grey;
        }
    </style>
</head>
        
<div class="header">
    <div class="logo">
        <img src="{{ public_path('storage/logo/carrymore_logo.png') }}" height="100%" width="100%">
    </div>
    <div class="invoice">
        <p>INVOICE</p>
    </div>
    <div class="status">
        @if ($data->invoice_status_id == App\InvoiceStatus::PENDING)
            @php
                $today = date("Y-m-d");
                $invoice_date = date("Y-m-d", strtotime($data->created_at));
            @endphp
            @if ($data->invoice_status_id == App\InvoiceStatus::PENDING)
                <img src="{{ public_path('assets/images/pending.png') }}" height="100%" width="100%">
            @else
                
            @endif
        @elseif($data->invoice_status_id == App\InvoiceStatus::PARTIALLY_PAID)
            <img src="{{ public_path('assets/images/partial_payment.png') }}" height="100%" width="100%">
        @else
            <img src="{{ public_path('assets/images/paid_fully.png') }}" height="100%" width="100%">            
        @endif
    </div>
</div>
<hr>

<div class="row supplier">
    <div class="details">
        <div class="vendor">Supplier Information</div>
        <div class="address">
            <address>
                <strong>{{ $data->supplier->company }}</strong><br>
                {{ $data->supplier->location }}<br>
                <abbr>Phone:</abbr> {{ $data->supplier->phone }}<br>
                <abbr>Email:</abbr> {{ $data->supplier->email }}
            </address>
        </div>
    </div>
    <div class="shipto">
        <div class="vendor">Invoice Information</div>
        <address>
            <strong>{{ $data->invoice_no }}</strong><br>
            <abbr>Supplier Inv No:</abbr> {{ $data->supplier_invoice_no }}<br>
            <abbr>Invoice Date:</abbr> {{ $data->created_at }}<br>
            <abbr>Prepared By:</abbr> {{ $data->user->full_name }}
        </address>
    </div>
</div>
<!-- end row -->

<div style="margin-top: 2px">&nbsp;</div>

<div>
    <table class="table table-bordered table-condensed table-striped" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Unit Cost</th>
                <th>Taxable</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data->items as $key => $item)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $item->product->name }}</td>
                    <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                    <td>{{ number_format($item->price, 2) }}</td>
                    <td>
                        @if ($item->product->vatable)
                            Yes
                        @else
                            No
                        @endif
                    </td>
                    <td>{{ number_format(($item->price * $item->quantity), 2) }}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="4"></td>
                <td>Sub-total</td>
                <td> {{ number_format(getInvoiceVat($data->id), 2) }} </td>
            </tr>
            <tr>
                <td colspan="4"></td>
                <td>Tax Rate</td>
                <td>{{ getVat() }}%</td>
            </tr>
            <tr>
                <td colspan="4"></td>
                <td>VAT</td>
                <td>{{ number_format(getInvoiceSubTotal($data->id), 2) }}</td>
            </tr>
            <tr>
                <td colspan="4"></td>
                <td>Total</td>
                <td>{{ number_format(getInvoiceVat($data->id) + getInvoiceSubTotal($data->id) , 2) }}</td>
            </tr>
        </tbody>
    </table>
</div>
    
