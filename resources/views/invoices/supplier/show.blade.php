@extends('layouts.app', ['title' => 'Invoice Details'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                        <li class="breadcrumb-item active">Invoices</li>
                    </ol>
                </div>
                <h4 class="page-title">Invoice details - {{ $invoice->invoice_no }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('invoices.index') }}" class="btn btn-info btn-sm"><i class="mdi mdi-view-list"></i> View All Invoices</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="card-box shadow-none border border-aqua">
                                <div class="member-card">
                                    <div class="avatar-xl member-thumb mb-3 mx-auto d-block">
                                        <img src="{{ asset('assets/images/invoice.svg') }}" class="rounded-circle img-thumbnail" alt="LPO">
                                    </div>

                                    <div class="text-center">
                                        <h5 class="font-18 mb-1">{{ $invoice->invoice_no }}</h5>
                                        <p class="text-muted mb-2">Supplier: <b>{{ $invoice->supplier->company }}</b></p>
                                    </div>

                                    <hr/>

                                    <div class="">
                                        <div class="row form-group text-muted font-14 ">
                                            <div class="col-7">
                                                <strong>Invoice Items</strong> 
                                            </div>
                                            <div class="col-5">
                                                <span>{{ count($invoice->items) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>Invoice Sub Total:</strong>
                                            </div>
                                            <div class="col-5">
                                                <span>{{ number_format(getInvoiceSubTotal($invoice->id), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>Invoice VAT :</strong> 
                                            </div>
                                            <div class="col-5">
                                                <span>{{ number_format(getInvoiceVat($invoice->id), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>Invoice Total :</strong> 
                                            </div>
                                            <div class="col-5">
                                                <span>{{ number_format(getInvoiceVat($invoice->id) + getInvoiceSubTotal($invoice->id), 2) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>Invoice Status :</strong> 
                                            </div>

                                            <div class="col-5">
                                                <span>
                                                    @if($invoice->invoice_status_id == App\InvoiceStatus::PENDING)
                                                        <span class="btn btn-danger btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Pending</span>
                                                    @elseif($invoice->invoice_status_id == App\InvoiceStatus::PARTIALLY_PAID)
                                                        <span class="btn btn-orange btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Partially Paid</span>
                                                    @else
                                                        <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Fully paid</span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row form-group text-muted font-14">
                                            <a href="{{ route('download-invoice', [$invoice]) }}" class="btn btn-xs btn-block btn-primary"><i class="mdi mdi-download"></i> Download Invoice Document</a>                                           
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                        <div class="col-9">
                            <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#items" role="tab">
                                        <span><i class="mdi mdi-format-list-bulleted text-danger"></i> Invoice Items</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#approvals" role="tab" >
                                        <span><i class="mdi mdi-cash text-success"></i> Invoice Payments</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane show active" id="items" role="tabpanel">
                                    <table class="table table-striped table-condensed table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Line Total</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($invoice->items as $count => $item)
                                                <tr>
                                                    <td>{{ $count+1 }}</td>
                                                    <td>{{ $item->product->name }}</td>
                                                    <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                                                    <td>{{ number_format($item->price, 2) }}</td>
                                                    <td>{{ number_format($item->quantity * $item->price) }}</td>
                                                </tr>                                                
                                            @endforeach                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection