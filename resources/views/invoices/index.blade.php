@extends('layouts.app', ['title' => 'Invoices'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Accounts</a></li>
                        <li class="breadcrumb-item active">Invoices</li>
                    </ol>
                </div>
                <h4 class="page-title">Invoices</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#sup-invoices" role="tab">
                            <span><i class="mdi mdi-truck-delivery text-danger"></i> Supplier Invoices</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#cus-invoices" role="tab" >
                            <span><i class="mdi mdi-file-account text-success"></i> Customer Invoices</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#invoices-status" role="tab" >
                            <span><i class="mdi mdi-format-list-checkbox text-pink"></i> Invoice Status</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="sup-invoices" role="tabpanel">
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>Invoice No</th>
                                <th>LPO No</th>
                                <th>Supplier</th>
                                <th>Items</th>
                                <th>Amount</th>
                                <th>Balance</th>
                                <th>Date</th>
                                <th>Prepared By</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($invoices as $invoice)
                                    <tr>
                                        <td>
                                            <a href="{{ route('supplier-invoice.show', [$invoice]) }}" > {{ $invoice->invoice_no }}</a>
                                        </td>
                                        <td>{{ $invoice->lpo->number }}</td>
                                        <td>{{ $invoice->supplier->company }}</td>
                                        <td>
                                            <code style="color: blue;cursor: pointer;" data-toggle="modal" data-target="#view-invoice-items{{ $invoice->id }}">{{ count($invoice->items) }} Items (view)</code>

                                            <div class="modal fade" id="view-invoice-items{{ $invoice->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                                <div class="modal-dialog modal-full" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">{{ $invoice->number }} items</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="dt-responsive table-responsive">
                                                                <table class="table table-striped table-condensed table-bordered">
                                                                    <thead>
                                                                        <th>#</th>
                                                                        <th>Name</th>
                                                                        <th>Quantity</th>
                                                                        <th>Unit Price</th>
                                                                        <th>Line Total</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($invoice->items as $count => $item)
                                                                            <tr>
                                                                                <td>{{ $count+1 }}</td>
                                                                                <td>{{ $item->product->name }}</td>
                                                                                <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                                                                                <td>{{ number_format($item->price, 2) }}</td>
                                                                                <td>{{ number_format($item->quantity * $item->price) }}</td>
                                                                            </tr>
                                                                        @endforeach

                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>Sub-total</td>
                                                                            <td><b>{{ number_format(getInvoiceSubTotal($invoice->id), 2) }}</b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>VAT</td>
                                                                            <td><b>{{ number_format(getInvoiceVat($invoice->id), 2) }}</b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>Total</td>
                                                                            <td><b>{{ number_format(getInvoiceVat($invoice->id) + getInvoiceSubTotal($invoice->id), 2) }}</b> </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ number_format($invoice->amount, 2) }}</td>
                                        <td>{{ number_format($invoice->balance, 2) }}</td>
                                        <td>{{ date('d-m-Y H:i A' ,strtotime($invoice->created_at)) }}</td>
                                        <td>{{ $invoice->user->full_name }}</td>
                                        <td>
                                            @if($invoice->invoice_status_id == App\InvoiceStatus::PENDING)
                                                <span class="btn btn-danger btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Pending</span>
                                            @elseif($invoice->invoice_status_id == App\InvoiceStatus::PARTIALLY_PAID)
                                                <span class="btn btn-orange btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Partially Paid</span>
                                            @else
                                                <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Fully paid</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="cus-invoices" role="tabpanel">
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>Invoice No</th>
                                <th>Customer</th>
                                <th>Items</th>
                                <th>Amount</th>
                                <th>Balance</th>
                                <th>Date</th>
                                <th>Prepared By</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($customer_invoices as $customer_invoice)
                                    <tr>
                                        <td>
                                            <a href="{{ route('customer-invoice.show', [$customer_invoice]) }}" > {{ $customer_invoice->invoice_no }}</a>
                                        </td>
                                        <td>{{ $customer_invoice->customer->full_name }}</td>
                                        <td>
                                            <code style="color: blue;cursor: pointer;" data-toggle="modal" data-target="#view-customer-invoice-items{{ $customer_invoice->id }}">{{ count($customer_invoice->items) }} Items (view)</code>

                                            <div class="modal fade" id="view-customer-invoice-items{{ $customer_invoice->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                                <div class="modal-dialog modal-full" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">{{ $customer_invoice->number }} items</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="dt-responsive table-responsive">
                                                                <table class="table table-striped table-condensed table-bordered">
                                                                    <thead>
                                                                        <th>#</th>
                                                                        <th>Name</th>
                                                                        <th>Quantity</th>
                                                                        <th>Unit Price</th>
                                                                        <th>Line Total</th>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($customer_invoice->items as $count => $item)
                                                                            <tr>
                                                                                <td>{{ $count+1 }}</td>
                                                                                <td>{{ $item->product->name }}</td>
                                                                                <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                                                                                <td>{{ number_format($item->price, 2) }}</td>
                                                                                <td>{{ number_format($item->quantity * $item->price) }}</td>
                                                                            </tr>
                                                                        @endforeach

                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>Sub-total</td>
{{--                                                                            <td><b>{{ number_format(getCustomerInvoiceSubTotal($customer_invoice->id), 2) }}</b> </td>--}}
                                                                            <td><b>200</b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>Discount</td>
{{--                                                                            <td><b>{{ number_format(getCustomerInvoiceDiscount($customer_invoice->id), 2) }}</b> </td>--}}
                                                                            <td><b>200</b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>VAT</td>
{{--                                                                            <td><b>{{ number_format(getCustomerInvoiceVat($customer_invoice->id), 2) }}</b> </td>--}}
                                                                            <td><b>300</b> </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td>Total</td>
{{--                                                                            <td><b>{{ number_format(getCustomerInvoiceSubTotal($customer_invoice->id) - getCustomerInvoiceDiscount($customer_invoice->id), 2) }}</b> </td>--}}
                                                                            <td><b>7688</b> </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ number_format($customer_invoice->amount, 2) }}</td>
                                        <td>{{ number_format($customer_invoice->balance, 2) }}</td>
                                        <td>{{ date('d-m-Y H:i A' ,strtotime($customer_invoice->created_at)) }}</td>
                                        <td>{{ $customer_invoice->user->full_name }}</td>
                                        <td>
                                            @if($customer_invoice->invoice_status_id == App\InvoiceStatus::PENDING)
                                            <span class="btn btn-danger btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Pending</span>
                                            @elseif($customer_invoice->invoice_status_id == App\InvoiceStatus::PARTIALLY_PAID)
                                                <span class="btn btn-orange btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Partially Paid</span>
                                            @else
                                                <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Fully paid</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="invoices-status" role="tabpanel">
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>#</th>
                                <th>Name</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($statuses as $key => $status)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $status->name }}</td>
                                        <td>{{ date('d-m-Y', strtotime($status->created_at)) }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

