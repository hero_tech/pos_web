@extends('layouts.app', ['title' => 'Bulk SMS Contacts'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bulk SMS</a></li>
                        <li class="breadcrumb-item active">Contacts</li>
                    </ol>
                </div>
                <h4 class="page-title">Bulk SMS Contacts & Groups</h4>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <ul class="nav nav-tabs tabs-bordered" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#contacts" role="tab">
                            <span><i class="mdi mdi-account text-info"></i> Contacts</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#groups" role="tab" >
                            <span><i class="mdi mdi-account-group text-danger"></i> Groups</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="contacts" role="tabpanel">
                        <button type="button"  data-toggle="modal" data-target="#add-contacts" style="border-radius:0px" class="btn btn-sm btn-info"><i class="mdi mdi-account-plus"></i> Add Contacts</button>
                        <button type="button"  data-toggle="modal" data-target="#upload-contacts" style="border-radius:0px" class="btn btn-sm btn-primary"><i class="mdi mdi-upload-multiple"></i> Upload Contacts</button>
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <hr>
                        <contacts></contacts>
                    </div>

                    <div class="tab-pane" id="groups" role="tabpanel">
                        <button type="button"  data-toggle="modal" data-target="#add-groups" style="border-radius:0px" class="btn btn-sm btn-info"><i class="mdi mdi-account-group"></i> Add Groups</button>
                        <hr>
                        <contact-groups></contact-groups>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-contacts" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px"  >
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add Contacts</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>                                                            
                
                <create-contacts></create-contacts>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="upload-contacts" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px"  >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Upload Contacts</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('import-contacts') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <p class="col-12 text-info">
                            <i class="mdi mdi-information"></i> 
                            Only comma delimited (.csv) documents are accepted.                               
                            <a href="{{ asset('assets/templates/import-contacts-template.csv') }}" download><code>Download template here</code></a>
                        </p>                                                          
                        <input type="file" class="dropify" name="import_file" data-max-file-size="8M" accept=".csv">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success waves-effect ">Upload Contacts</button>
                    </div>
                        
                </form>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="add-groups" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px"  >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add Groups</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>                                                            
                
                <create-groups></create-groups>
                
            </div>
        </div>
    </div>

@endsection