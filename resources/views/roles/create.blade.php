@extends('layouts.app', ['title' => 'Create Roles'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                        <li class="breadcrumb-item active">Roles</li>
                    </ol>
                </div>
                <h4 class="page-title">Create roles</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<form method="POST" action="{{ url('roles') }}">
						@csrf
						<div class="row form-group">
							<div class="col-md-3"><label>Role Name</label></div>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control" required>
							</div>
						</div>
						<div class="row form-group">
							<span class="col-md-2 text-info">Permissions</span>
						</div>
						<div class="form-group row">
							@foreach(App\Role::PERMISSIONS as $key => $permission)
								<div class="col-md-3">
									<h5>{{ ucwords($key) }}</h5>
									@foreach($permission as $name => $item)
                                    <div class="custom-control custom-checkbox checkbox-success">
                                        <input type="checkbox" id="role{{ $name}}" class="custom-control-input" name="permissions[{{$name}}]" value="{{ $item }}">
                                        <label class="custom-control-label" for="role{{ $name }}">&nbsp;{{ ucwords($name) }}</label>
                                    </div>
									@endforeach
								</div>					
							@endforeach
						</div>

						<div class="row form-group">
							<div class="col-md-4">&nbsp;</div>
							<div class="col-md-4">
								<button type="submit" class="btn btn-block btn-primary btn-sm"> Create New Role</button>
							</div>
						</div>
						
					</form>
                </div>
            </div>
        </div>
    </div>
@endsection