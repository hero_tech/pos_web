@extends('layouts.app', ['title' => $role->name])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                        <li class="breadcrumb-item active">Roles</li>
                    </ol>
                </div>
                <h4 class="page-title">{{ $role->name }}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-header">
                        <span>Permissions for {{ $role->name }}</span>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('update-role/'.$role->id)}}" method="POST" >
                            @csrf
                            @method('GET')
                            <div class="row form-group">
                                <div class="col-md-2"><label>Role Name</label></div>
                                <div class="col-md-9">
                                    <input type="text" name="name" class="form-control" value="{{ $role->name }}" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <span class="col-md-2 text-info">Permissions</span>
                            </div>
                            <div class="form-group row">
                                @foreach(App\Role::PERMISSIONS as $key => $permission)
    
                                    <div class="col-md-3">
                                        <h5>{{ ucwords($key) }}</h5>
                                        @foreach($permission as $name => $item)
                                                @if(in_array($name, array_keys($role->permissions)))
                                                    <div class="custom-control custom-checkbox checkbox-success">
                                                        <input type="checkbox" id="role{{ $name }}" class="custom-control-input" name="permissions[{{$name}}]" value="{{ $item }}" checked>
                                                        <label class="custom-control-label" for="role{{ $name }}">&nbsp;{{ ucwords($name) }} </label>
                                                    </div>
                                                @else
                                                    <div class="custom-control custom-checkbox checkbox-success">
                                                        <input type="checkbox" id="role{{ $name }}" class="custom-control-input" name="permissions[{{$name}}]" value="{{ $item }}">
                                                        <label class="custom-control-label" for="role{{ $name }}">&nbsp;{{ ucwords($name) }} </label>
                                                    </div>
                                                @endif
                                        @endforeach
                                    </div>					
                                @endforeach
                            </div>
    
                            <div class="row form-group">
                                <div class="col-md-4">&nbsp;</div>
                                <div class="col-md-5">
                                    <button type="submit" class="btn btn-primary btn-sm"> Update {{ $role->name }} Permissions</button>
                                </div>
                            </div>
                        </form>
    
                        
                        <div class="row">
                            <div class="col-md-6">
                                <span class="text-muted"> Users Belonging to this role </span>
                                <table class="table table-striped table-condensed table-bordered">
                                    <thead>
                                        <th>#</th>
                                        <th>User Name</th>
                                        <th>Actions</th>
                                    </thead>
                                    <tbody>
                                        @php($count = 1 )
                                        @foreach($role->users as $user)
                                        <tr>
                                            <td>{{ $count++ }}</td>
                                            <td>{{ $user->full_name }}</td>
                                            <td>
                                                <form action="{{ url('user-roles/detach') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                    <input type="hidden" name="role_id" value="{{ $role->id }}">
                                                    <a href="{{ url('users',['id' =>$user->id]) }}" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                                    <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <span class="text-muted">Attach users to this role</span>
                                <form method="POST" action="{{ url('user-roles/attach') }}">
                                    @csrf
                                    <input type="hidden" name="role_id" value="{{ $role->id }}">
                                    <div class="form-group">
                                        <select class="form-control" required="" tabindex="-1" name="user_id">
                                            <option value="">-- Select User --</option>
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">{{ $user->full_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                        <div class="form-group">
                                            <div class="offset-md-5">
                                                <button type="submit" class="btn btn-block btn-sm btn-success"><i class="fa fa-user-circle"></i> Assign user to {{ $role->name }} Role</button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection