@extends('layouts.app', ['title' => 'View Roles'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                        <li class="breadcrumb-item active">Roles</li>
                    </ol>
                </div>
                <h4 class="page-title">Roles</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card table-responsive">
                <div class="card-header">
					<a href="{{ route('roles.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus-circle"></i> Create New Role</a>
                </div>
                <div class="card-body">
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Role</th>
                            <th>Users</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $key => $role)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $role->name }} </td>
                            <td>
                                <code style="color: blue;cursor: pointer;" data-toggle="modal" data-target="#view-role-users{{ $role->id }}">{{ count($role->users) }} found (click to view)</code>

                                <div class="modal fade" id="view-role-users{{ $role->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" align="center">Users with {{ $role->name }} Role</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row form-group">
                                                    <div class="col-md-12">
                                                        <span class="text-muted">Attach users to this role</span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <form method="POST" action="{{ url('user-roles/attach') }}">
                                                            @csrf
                                                            <input type="hidden" name="role_id" value="{{ $role->id }}">
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    <select class="form-control" required="" name="user_id">
                                                                        <option value="">-- Select User --</option>
                                                                        @foreach($users as $user)
                                                                        <option value="{{$user->id}}">{{ $user->full_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-user-circle"></i> Assign User</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped table-condensed table-bordered">
                                                        <thead>
                                                            <th>#</th>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Mobile</th>
                                                            <th>Department</th>
                                                            <th>Action</th>
                                                        </thead>
                                                        <tbody>
                                                            @php( $count = 1 )
                                                            @foreach($role->users as $user)
                                                            <tr>
                                                                <td>{{ $count++ }}</td>
                                                                <td>{{ $user->full_name }}</td>
                                                                <td>{{ $user->email }}</td>
                                                                <td>{{ $user->mobile }}</td>
                                                                <td>{{ $user->department->name }}</td>
                                                                <td>
                                                                    <form action="{{ url('user-roles/detach') }}" method="POST">
                                                                        @csrf
                                                                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                                        <input type="hidden" name="role_id" value="{{ $role->id }}">
                                                                        <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div> 
                            </td>
                            <td>
                                <button type="button" class="btn btn-xs btn-success"><i class="fa fa-check"></i> Active</button>
                            </td>
                            <td>{{ date('d-M-Y', strtotime($role->created_at)) }}</td>
                            <td>
                                <form method="POST" action="{{ url('copy-role') }}">
                                    @csrf
                                    <input type="hidden" name="role_id" value="{{ $role->id }}">
                                    <button type="submit" class="btn btn-xs btn-primary"><i class=" fa fa-copy"></i> Copy</button>

                                    <a style="cursor: pointer" href="{{ url('roles/'.$role->id) }}" class="btn btn-xs btn-info"><i class=" fa fa-edit"></i> Edit</a>

                                </form>
                            </td>
                        </tr>         
                        @endforeach

                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>

@endsection