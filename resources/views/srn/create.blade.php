@extends('layouts.app', ['title' => 'Create SRN'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">SRN</li>
                    </ol>
                </div>
                <h4 class="page-title">Create Store Requisition Note</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('store-requisition-notes.index') }}" class="btn btn-success btn-sm" style="border-radius:0px;"><i class="mdi mdi-view-list"></i> S.R.N (s)</a>
                </div>
                <div class="card-body">
                    <create-store-requisition-note></create-store-requisition-note>
                </div>
            </div>
        </div>
    </div>

@endsection