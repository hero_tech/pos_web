@extends('layouts.app', ['title' => $srn->number])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">SRN</li>
                    </ol>
                </div>
                <h4 class="page-title">Store Requisition Note - {{ $srn->number }}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="card-header">
                    <a href="{{ route('store-requisition-notes.index') }}" class="btn btn-info btn-sm"><i class="mdi mdi-view-list"></i> View All SRN (s)</a>
                </div>
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="card-box shadow-none border border-aqua">
                                <div class="member-card">
                                    <div class="avatar-xl member-thumb mb-3 mx-auto d-block">
                                        <img src="{{ asset('assets/images/srn.png') }}" class="rounded-circle img-thumbnail" alt="SRN">
                                    </div>

                                    <div class="text-center">
                                        <h5 class="font-18 mb-1">{{ $srn->number }}</h5>
                                        <p class="text-muted mb-2"><b>{{ $srn->user->full_name }}</b></p>
                                    </div>

                                    <hr/>
                                    <div class="">
                                        <div class="row form-group text-muted font-14 ">
                                            <div class="col-7">
                                                <strong>SRN Items</strong> 
                                            </div>
                                            <div class="col-5">
                                                <span>{{ count($srn->items) }}</span>
                                            </div>
                                        </div>
                                        <div class="row form-group text-muted font-14">
                                            <div class="col-7">
                                                <strong>SRN Status :</strong> 
                                            </div>

                                            <div class="col-5">
                                                <span>
                                                    @if($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::PENDING)
                                                        <span class="btn btn-info btn-rounded btn-xs" style="border-radius:15px;">Pending</span>
                                                    @elseif($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::CANCELED)
                                                        <span class="btn btn-orange btn-rounded btn-xs" style="border-radius:15px;;">Canceled</span>
                                                    @elseif($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::REJECTED)
                                                        <span class="btn btn-danger btn-rounded btn-xs" style="border-radius:15px;;">Rejected</span>
                                                    @else
                                                        <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px;;">Approved</span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row form-group text-muted font-14">
                                            @if ($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::APPROVED)
                                                <a href="{{ route('download-store-requisition-note', [$srn]) }}" class="btn btn-xs btn-block btn-success"><i class="mdi mdi-download"></i> Download SRN Document</a>
                                            @else
                                                <button class="btn btn-xs btn-block btn-warning" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="" data-content="You are not permitted to downlaod an SRN if it has not been fully approved" data-original-title="SRN Not Approved"><i class="mdi mdi-download-off"></i> Download LPO Document </button>
                                                
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-9">
                            <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#srn-items" role="tab">
                                        <span><i class="mdi mdi-format-list-bulleted text-danger"></i> SRN Items</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#srn-approvals" role="tab" >
                                        <span><i class="mdi mdi-check-circle text-success"></i> SRN Approvals</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane show active" id="srn-items" role="tabpanel">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    @if (canEditSRN($srn->id) && $srn->user_id == auth()->user()->id) 
                                        <button type="button" data-toggle="modal" data-target="#add-srn-items" class="btn btn-sm btn-success" style="border-radius:0px"><i class=" fa fa-plus-circle"></i> Add more items</button>
                                        <hr>
                                    @endif
                                    <div class="dt-responsive table-responsive">
                                        <table class="table table-striped table-condensed table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Quantity</th>
                                                @if (canEditSRN($srn->id) && $srn->user_id == auth()->user()->id)                                                    
                                                    <th>Actions</th>
                                                @endif
                                            </thead>
                                            <tbody>
                                                @foreach ($srn->items as $count => $item)
                                                    <tr>
                                                        <td>{{ $count+1 }}</td>
                                                        <td>{{ $item->product->name }}</td>
                                                        <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                                                        
                                                        @if (canEditSRN($srn->id) && $srn->user_id == auth()->user()->id) 
                                                            <td>
                                                                <form action="{{ route('store-requisition-note-items.destroy', [$item]) }}" method="post">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button type="button" data-toggle="modal" data-target="#edit-srn-item-{{ $item->id }}" class="btn btn-xs btn-info" style="border-radius:0px"><i class=" fa fa-edit"></i> Edit</button>
                                                                    <button type="submit" class="btn btn-xs btn-danger delete" style="border-radius:0px"><i class="fa fa-trash-alt"></i> Delete</button>
                                                                </form>

                                                                <div class="modal fade" id="edit-srn-item-{{ $item->id }}" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px;">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h4 class="modal-title" align="center">Edit {{ $item->product->name }}</h4>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                    <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form action="{{ route('store-requisition-note-items.update', [$item]) }}" method="POST">
                                                                                @csrf
                                                                                @method('PUT')
                                                                                <input type="hidden" name="srn" value="{{ $srn->id }}">
                                                                                <div class="modal-body">
                                                                                    <div class="row form-group">
                                                                                        <div class="col-2">
                                                                                            <label>Product</label>
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                            <select name="product_id" class="form-control">
                                                                                                @foreach ($products as $product)
                                                                                                    <option value="{{ $product->id}}" @if($product->id == $item->product_id) selected @endif>{{ $product->name }}</option>
                                                                                                @endforeach
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row form-group">
                                                                                        <div class="col-2">
                                                                                            <label>Quantity</label>
                                                                                        </div>
                                                                                        <div class="col-10">
                                                                                            <input type="number" name="quantity" class="form-control" value="{{ $item->quantity }}" required>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-success waves-effect" >Edit Item</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="srn-approvals" role="tabpanel">
                                    <approve-srn :srn="{{ $srn->id }}"></approve-srn>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-srn-items" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top:50px"  >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">Add More Items - {{ $srn->number }}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">                                                              
                    <add-srn-items :srn={{ $srn->id }}></add-srn-items>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection