@extends('layouts.app', ['title' => 'Store Requisition Note'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Procurement</a></li>
                        <li class="breadcrumb-item active">SRN</li>
                    </ol>
                </div>
                <h4 class="page-title">Store Requisition Notes</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#srns" role="tab" aria-controls="srns" aria-selected="true">
                            <span class="d-block d-sm-none"><i class="mdi mdi-truck-delivery"></i></span>
                            <span class="d-none d-sm-block"><i class="mdi mdi-view-list text-secondary"></i> Store Requisition Notes</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#srn-approvers" role="tab" aria-controls="srn-approvers" aria-selected="false">
                            <span class="d-block d-sm-none"><i class="fa fa-check"></i></span>
                            <span class="d-none d-sm-block"><i class="mdi mdi-check-circle text-success"></i> Approvers</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#srn-statuses" role="tab" aria-controls="srn-statuses" aria-selected="false">
                            <span class="d-block d-sm-none"><i class="fa fa-info-circle"></i></span>
                            <span class="d-none d-sm-block"><i class="mdi mdi-information text-info"></i>SRN Statuses</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane show active" id="srns" role="tabpanel" aria-labelledby="srns">
                        <div class="form-group">
                            <a href="{{ route('store-requisition-notes.create') }}" class="btn btn-success btn-sm" style="border-radius:0px;"><i class="fa fa-plus-circle"></i> Create New SRN</a>
                        </div>
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>SRN No</th>
                                    <th>Prepared By</th>
                                    <th>Requested Items</th>
                                    <th>Approval Status</th>
                                    <th>Date Created</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($srns as $key => $srn)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $srn->number }}</td>
                                        <td>{{ $srn->user->full_name }}</td>
                                        <td>
                                            <code style="color: blue;cursor: pointer;" data-toggle="modal" data-target="#view-srn-items{{ $srn->id }}">{{ count($srn->items) }} (click to view)</code>
                                            
                                            <div class="modal fade" id="view-srn-items{{ $srn->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                                <div class="modal-dialog modal-full" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">{{ $srn->number }} requested items</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="dt-responsive table-responsive">
                                                                <table class="table table-striped table-condensed table-bordered datatables_responsive">
                                                                    <thead>
                                                                        <th>#</th>
                                                                        <th>Name</th>
                                                                        <th>Quantity Requested</th>
                                                                        <th>Quantity <small>(Supermarket)</small></th>
                                                                        <th>Quantity <small>(Store)</small></th>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($srn->items as $count => $item)
                                                                            <tr>
                                                                                <td>{{ $count + 1 }}</td>
                                                                                <td>{{ $item->product->name }}</td>
                                                                                <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                                                                                <td>{{ number_format(srnSupermarketQuantity($item->product->id)) }}</td>
                                                                                <td>{{ number_format(srnStoreQuantity($item->product->id)) }}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
        
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            @if($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::PENDING)
                                                <span data-toggle="modal" data-target="#view-srn-status-{{ $srn->id }}" class="btn btn-info btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Pending Approval</span>
                                            @elseif($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::CANCELED)
                                                <span data-toggle="modal" data-target="#view-srn-status-{{ $srn->id }}" class="btn btn-orange btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Canceled</span>
                                            @elseif($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::REJECTED)
                                                <span data-toggle="modal" data-target="#view-srn-status-{{ $srn->id }}" class="btn btn-danger btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Rejected</span>
                                            @else
                                                <span data-toggle="modal" data-target="#view-srn-status-{{ $srn->id }}" class="btn btn-success btn-rounded btn-xs" style="border-radius:15px;cursor:pointer;">Approved</span>
                                            @endif   

                                            <div class="modal fade" id="view-srn-status-{{ $srn->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                                <div class="modal-dialog modal-full" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" align="center">{{ $srn->number }} Approval Status</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">                                                             
                                                            <approve-srn 
                                                                :srn="{{ $srn->id }}">
                                                            </approve-srn>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </td>
                                        <td>{{ date('d-m-Y H:i A', strtotime($srn->created_at)) }}</td>
                                        <td>
                                            <a href="{{ route('store-requisition-notes.show', [$srn]) }}" class="btn btn-xs btn-info"><i class=" fa fa-edit"></i> View More</a>
    
                                            @if($srn->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::APPROVED)
                                                <a href="{{ route('download-store-requisition-note', [$srn]) }}" class="btn btn-xs btn-primary"><i class="mdi mdi-download"></i> Download</a>            
                                            @else
                                                <button type="button" class="btn btn-xs btn-warning" data-toggle="popover" data-placement="bottom" data-trigger="focus" title="" data-content="You are not permitted to downlaod an LPO if it has not been fully approved" data-original-title="LPO Not Approved"><i class="mdi mdi-download-off"></i> Download</button>                                            
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="srn-approvers" role="tabpanel" aria-labelledby="srn-approvers">
                        <div class="form-group">
                            <button class="btn btn-success width-xs" style="border-radius:0px;"  data-toggle="modal" data-target="#srn-approver"><i class="fa fa-plus-circle"></i> New SRN Approver</a>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <store-requisition-note-approvers></store-requisition-note-approvers>
                    </div>

                    <div class="tab-pane" id="srn-statuses" role="tabpanel" aria-labelledby="srn-statuses">
                        <div class="form-group">
                            <button class="btn btn-success width-xs" style="border-radius:0px;"  data-toggle="modal" data-target="#srn-statuses"><i class="fa fa-plus-circle"></i> LPO Status</a>
                        </div>
                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Date Created</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($statuses as $key => $status)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $status->name }}</td>
                                        <td>{{ $status->status }}</td>
                                        <td>{{ $status->created_at }}</td>
                                        <td>
                                            <form action="">
                                                <a href="" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                                                <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="srn-approver" tabindex="-1"  data-backdrop="static" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" align="center">New SRN Approver</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('srn-approvers') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="col-12 form-group">
                            <select name="user_id" class="form-control" required>
                                <option value="">-- Select Approver --</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-12">
                            <input type="number" name="sequence" class="form-control" placeholder="Sequence" required>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Create new SRN Approver</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection