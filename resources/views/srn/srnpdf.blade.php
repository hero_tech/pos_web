<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ public_path('assets/css/bootstrap.min.css') }}">
    <title>Store Requisition Note - {{ $data->number }}</title>

    <style>
        @page{
            margin-top: 70px;
        }
        body {
            font-family: "Cambria";
            font-size: 14px;
            background-color: white;
            color: black
        }
        .logo {
            width: 250px;
            max-width: 50%;
            height: 70px;
            float: left;
        }

        .signature {
            width: 100px;
            height: 50px;
        }

        table, tr, td {
            padding: 0px;
            color: black
        }
        .header{
            height: 80px;
        }
        .lpo_number{
            float: right;
            margin-top: 10px;
        }
        .supplier .vendor{
            margin-bottom: 10px;
        }
        .supplier .details {
            width: 100%;
            margin-left: 10px;
        }
        .info{
            margin-top: 5px;
        }
        
        .page_break { page-break-before: always; }
        .no_page_break { page-break-before: avoid; }
        hr{
            color: grey;
            border-bottom: 1px solid grey;
        }
    </style>
</head>
        
    <div class="header">
        <div class="logo">
            <img src="{{ public_path('storage/logo/carrymore_logo.png') }}" height="100%" width="100%">
        </div>
        <div class="lpo_number">
            <p>
                SRN Number:<strong> {{ $data->number }} </strong><br>
                SRN Date: <strong>{{ date('d-m-Y H:i A', strtotime($data->created_at)) }}</strong>
            </p>
        </div>
    </div>
    <hr>
    <div class="row supplier">
        <div class="details">
            <div class="vendor">User Request Info</div>
            <div class="address">
                <address>
                    <strong>{{ $data->user->full_name }}</strong><br>
                    {{ $data->user->department->name }}<br>
                    <abbr title="Phone">Phone:</abbr> {{ $data->user->mobile }}<br>
                    <abbr title="Phone">Email:</abbr> {{ $data->user->email }}
                </address>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="info">
        <p>I hereby request the issuance of the following items to replenish the supermarket.</p>
    </div>

    <div>
        <table class="table table-bordered table-condensed table-striped" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data->items as $key => $item)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $item->product->name }}</td>
                        <td>{{ number_format($item->quantity).' '.$item->product->unitOfMeasure->name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-12" style="page-break-inside: avoid">

        <div class="col-12" style="margin-top: 15px;">
            <table width="100%">
                <tr>
                    <td width="15%"><b>Prepared By:</b></td>
                    <td style="border-bottom: 0.5px solid #000">{{ $prepared_by->full_name }}</td>
                    <td width="8%"><b>Sign:</b></td>
                    <td width="20%" style="border-bottom: 0.5px solid #000" width="20%"><img class="signature" src="{{ public_path('storage/signatures/'.$prepared_by->signature) }}" height="100%" width="100%"></td>
                    <td width="8%"><b>Date:</b></td>
                    <td style="border-bottom: 0.5px solid #000" width="20%">{{ date('d/m/Y', strtotime($data->created_at)) }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                @foreach ($approvers as $approver)
                    <tr>
                        <td>
                            <b>
                                @if(lastApprover($approver->approver->user->id, $data->id) == 0)
                                    Approved By:
                                @else
                                    Authorised By:
                                @endif
                            </b>
                        </td>
                        <td style="border-bottom: 0.5px solid #000">{{ $approver->approver->user->full_name }}</td>
                        <td><b>Sign:</b></td>
                        <td style="border-bottom: 0.5px solid #000">
                            @if($approver->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::APPROVED)
                                <img class="signature" src="{{ public_path('storage/signatures/'.$approver->approver->user->signature) }}" height="100%" width="100%">
                            @endif
                        </td>
                        <td><b>Date:</b></td>
                        <td style="border-bottom: 0.5px solid #000">
                            @if($approver->store_requisition_note_status_id == App\StoreRequisitionNoteStatus::APPROVED)
                                {{ date('d/m/Y', strtotime($data->updated_at)) }}
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                @endforeach
                
                
            </table>
        </div>
        
    </div>     
