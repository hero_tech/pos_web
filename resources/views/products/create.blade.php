@extends('layouts.app', ['title' => 'Register Products'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Inventory management</a></li>
                        <li class="breadcrumb-item active">Products</li>
                    </ol>
                </div>
                <h4 class="page-title">Register products</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    
                <form action="{{ url('products') }}" method="POST" class="offset-1">
                    @csrf
                    <div class="row form-group">
                        <div class="col-2">
                            <label>Product Category</label>
                        </div>
                        <div class="col-3">
                            <select name="product_category_id" class="form-control" required>
                                <option value="">Select Product Category</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-2">
                            <label>Unit of measure</label>
                        </div>
                        <div class="col-3">
                            <select name="unit_of_measure_id" class="form-control" required>
                                <option value="">Select unit of measure</option>
                                @foreach ($units as $unit)
                                    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">

                        <div class="col-2">
                            <label>Product name</label>
                        </div>
                        <div class="col-3">
                            <input type="text" name="name" class="form-control" required>
                        </div>


                        <div class="col-2">
                            <label>Product code</label>
                        </div>
                        <div class="col-3">
                            <input type="text" name="code" class="form-control">
                        </div>

                    </div>
                    <div class="row form-group">

                        <div class="col-2">
                            <label>Product cost</label>
                        </div>
                        <div class="col-3">
                            <input type="number" name="cost" class="form-control">
                        </div>

                        <div class="col-2">
                            <label>Discount allowed</label>
                        </div>
                        <div class="col-3">
                            <input type="number" name="discount_allowed" class="form-control" required>
                        </div>
                    </div>

                    <div class="row form-group">

                        <div class="col-2">
                            <label>Product Quantity</label>
                        </div>
                        <div class="col-3">
                            <input type="number" name="quantity" class="form-control" required>
                        </div>

                        <div class="col-2">
                            <label>Notify</label>
                        </div>
                        <div class="col-3">
                            <input type="number" name="notify" class="form-control" required>
                            <span class="help-block"><small>Get notified when product quantity reaches this number.</small></span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-2">
                            <label>Product Vatable</label>
                        </div>
                        <div class="col-3">
                            <input type="checkbox" name="vatable" id="vatable" data-switch="success" value="1" checked/>
                            <label for="vatable" data-on-label="Yes" data-off-label="No"></label>
                        </div>
                        <div class="col-2">
                            <label>Product Exempted</label>
                        </div>
                        <div class="col-3">
                            <input type="checkbox" name="exempted" id="exempted" data-switch="success" value="1"/>
                            <label for="exempted" data-on-label="Yes" data-off-label="No"></label>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-4 offset-4">
                            <button type="submit" class="btn btn-success"><i class="mdi mdi-plus-circle-multiple-outline"></i> Register New Product</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection