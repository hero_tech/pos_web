@extends('layouts.app', ['title' => 'View Products'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Inventory management</a></li>
                        <li class="breadcrumb-item active">Products</li>
                    </ol>
                </div>
                <h4 class="page-title">View products</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Unit</th>
                            <th>Cost</th>
                            <th>Discount <br> <small>Allowed</small> </th>
                            <th>Quantity<br> <small>Store</small></th>
                            <th>Quantity <br><small>Sales</small></th>
                            <th>Notify</th>
                            <th>Vatable</th>
                            <th>Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key => $product)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>
                                    @if ($product->code == null)
                                        <code style="cursor:pointer;" data-toggle="modal" data-target="#set-code-{{ $product->id }}">Not set</code>
                                    @else
                                        <span style="cursor:pointer;" data-toggle="modal" data-target="#set-code-{{ $product->id }}">{{ $product->code }}</span>                                        
                                    @endif

                                    <div class="modal fade" id="set-code-{{ $product->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <form method="POST" action="{{ route('update-product-code', [$product]) }}">
                                                    @csrf
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" align="center">Set {{ $product->name }} Bar Code</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input class="form-control" name="code" value="{{ $product->code }}" required>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Set new code</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>{{ $product->unitOfMeasure->name }}</td>
                                <td>
                                    @can('update-selling-prices')
                                        <span style="cursor:pointer; color:blue;" data-toggle="modal" data-target="#set-price-{{ $product->id }}">{{ number_format($product->cost, 2) }}</span>
                                    @else()
                                        {{ number_format($product->cost, 2) }}
                                    @endcan

                                    <div class="modal fade" id="set-price-{{ $product->id }}" tabindex="-1"  data-backdrop="static" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <form method="POST" action="{{ url('set-selling-price', ['id' => $product->id]) }}">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" align="center">Set {{ $product->name }} selling price</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input class="form-control" name="cost" value="{{ $product->cost }}" required>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Set new price</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $product->discount_allowed }}%</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->sales_quantity }}</td>
                                <td>{{ $product->notify }}</td>
                                <td>
                                    @if ($product->vatable)
                                        <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px">Yes</span>
                                    @else
                                        <span class="btn btn-warning btn-rounded btn-xs" style="border-radius:15px">No</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($product->status)
                                        <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px">Active</span>
                                    @else
                                        <span class="btn btn-warning btn-rounded btn-xs" style="border-radius:15px">Disabled</span>
                                    @endif
                                </td>
                                <td>
                                    <span class="btn btn-info btn-rounded btn-xs" style="border-radius:15px"  data-toggle="modal" data-target="#edit-supplier"><i class="fa fa-edit"></i> Edit</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection