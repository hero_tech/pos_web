<!DOCTYPE html>
<html lang="en">    
    <head>
        <meta charset="utf-8" />
        <title>Carrymore Supermarket - Reset Password</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Responsive bootstrap 4 admin template" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/images/carrymore_logo.png') }}">

        <!-- App css -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
        <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-stylesheet" />
    
    </head>
    <body>
        
        <div class="account-pages mt-5 mb-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="text-center account-logo-box">
                                <div class="mt-2 mb-2">
                                    <a href="javascript::void(0)" class="text-success">
                                        <span><img src="{{ asset('assets/images/carrymore_logo.png') }}" alt="" height="80"></span>
                                    </a>
                                </div>
                            </div>
                            <p class="text-muted col-md-12">
                                Hello {{ auth()->user()->first_name }}. To protect your account, kindly reset your password. 
                                It's just one time thing.
                            </p>

                            <div class="card-body">
                                <form method="POST" action="{{ route('reset-password') }}">
                                    @csrf

                                    <div class="form-group row form-primary">
                                        <label for="old-password" class="col-md-12 col-form-label">{{ __('Old Password') }}</label>
    
                                        <div class="col-md-12">
                                            <input id="old-password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required >
                                            
                                            @error('old_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row form-primary">
                                        <label for="password" class="col-md-12 col-form-label">{{ __('New Password') }}</label>
    
                                        <div class="col-md-12">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
    
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
    
                                    <div class="form-group row form-primary">
                                        <label for="password-confirm" class="col-md-12 col-form-label">{{ __('Confirm Password') }}</label>
    
                                        <div class="col-md-12">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-sm btn-rounded btn-primary">
                                                <i class="fa fa-key"></i>
                                                {{ __('Reset Account Password') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </body>    
</html>
