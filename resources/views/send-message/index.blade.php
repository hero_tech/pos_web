@extends('layouts.app', ['title' => 'Send Message'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bulk SMS</a></li>
                        <li class="breadcrumb-item active">Send Message</li>
                    </ol>
                </div>
                <h4 class="page-title">Send Messages</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered " role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#analysis" role="tab">
                            <span><i class="mdi mdi-chart-arc text-info"></i> Quick Info</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#send-message" role="tab" >
                            <span><i class="mdi mdi-send-circle text-success"></i> Send Message</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="analysis" role="tabpanel">
                        <div class="row">
                            <div class="col-3">
                                <div class="card widget-box-one border border-primary bg-soft-primary">
                                    <div class="card-body">
                                        <div class="float-right avatar-lg rounded-circle mt-3">
                                            <i class="mdi mdi-scale-balance font-30 widget-icon rounded-circle avatar-title text-primary"></i>
                                        </div>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-muted" title="Statistics">Message Balance</p>
                                            <h2><span data-plugin="counterup">{{ $messagesBalance }}</span></h2>
                                            <p class="text-muted" style="font-size:13px"><span class="font-weight-medium">Updated:</span> {{ date('d-m-Y H:i A') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="card widget-box-one border border-success bg-soft-success">
                                    <div class="card-body">
                                        <div class="float-right avatar-lg rounded-circle mt-3">
                                            <i class="mdi mdi-send-circle font-30 widget-icon rounded-circle avatar-title text-success"></i>
                                        </div>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-muted" title="Statistics">Messages Sent</p>
                                            <h2><span data-plugin="counterup">{{ $messagesSent }}</span></h2>
                                            <p class="text-muted" style="font-size:13px"><span class="font-weight-medium">Updated:</span> {{ date('d-m-Y H:i A') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="card widget-box-one border border-purple bg-soft-purple">
                                    <div class="card-body">
                                        <div class="float-right avatar-lg rounded-circle mt-3">
                                            <i class="mdi mdi-account-multiple font-30 widget-icon rounded-circle avatar-title text-purple"></i>
                                        </div>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-muted" title="Statistics">Total Contacts</p>
                                            <h2><span data-plugin="counterup">{{ $totalContacts }}</span></h2>
                                            <p class="text-muted" style="font-size:13px"><span class="font-weight-medium">Updated:</span> {{ date('d-m-Y H:i A') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="card widget-box-one border border-pink bg-soft-pink">
                                    <div class="card-body">
                                        <div class="float-right avatar-lg rounded-circle mt-3">
                                            <i class="mdi mdi-account-group font-30 widget-icon rounded-circle avatar-title text-pink"></i>
                                        </div>
                                        <div class="wigdet-one-content">
                                            <p class="m-0 text-muted" title="Statistics">Total Groups</p>
                                            <h2><span data-plugin="counterup">{{ $totalGroups }}</span></h2>
                                            <p class="text-muted" style="font-size:13px"><span class="font-weight-medium">Updated:</span> {{ date('d-m-Y H:i A') }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <p>Time based analysis of messages sent.</p>
                        <div id="messagesAnalysis" style="height:500px"></div>
                    </div>
                    <div class="tab-pane" id="send-message" role="tabpanel">
                        <send-message></send-message>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        am4core.ready(function() {

            // Themes begin
            am4core.useTheme(am4themes_material);
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart = am4core.create("messagesAnalysis", am4charts.XYChart);

            chart.data  = {!! json_encode($messageAnalysis)!!};

            // Set input format for the dates
            chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

            // Create axes
            var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

            // Create series
            var series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "value";
            series.dataFields.dateX = "date";
            series.tooltipText = "{value}"
            series.strokeWidth = 2;
            series.minBulletDistance = 15;

            // Drop-shaped tooltips
            series.tooltip.background.cornerRadius = 20;
            series.tooltip.background.strokeOpacity = 0;
            series.tooltip.pointerOrientation = "vertical";
            series.tooltip.label.minWidth = 40;
            series.tooltip.label.minHeight = 40;
            series.tooltip.label.textAlign = "middle";
            series.tooltip.label.textValign = "middle";

            // Make bullets grow on hover
            var bullet = series.bullets.push(new am4charts.CircleBullet());
            bullet.circle.strokeWidth = 2;
            bullet.circle.radius = 4;
            bullet.circle.fill = am4core.color("#fff");

            var bullethover = bullet.states.create("hover");
            bullethover.properties.scale = 1.3;

            // Make a panning cursor
            chart.cursor = new am4charts.XYCursor();
            chart.cursor.behavior = "panXY";
            chart.cursor.xAxis = dateAxis;
            chart.cursor.snapToSeries = series;

            // Create vertical scrollbar and place it before the value axis
            chart.scrollbarY = new am4core.Scrollbar();
            chart.scrollbarY.parent = chart.leftAxesContainer;
            chart.scrollbarY.toBack();

            // Create a horizontal scrollbar with previe and place it underneath the date axis
            chart.scrollbarX = new am4charts.XYChartScrollbar();
            chart.scrollbarX.series.push(series);
            chart.scrollbarX.parent = chart.bottomAxesContainer;

            dateAxis.start = 0.79;
            dateAxis.keepSelection = true;

        }); // end am4core.ready()
    </script>
@endsection