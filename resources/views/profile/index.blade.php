@extends('layouts.app', ['title' => 'My Account'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div>
                <h4 class="page-title">My account</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="text-center card-box shadow-none border border-secoundary">
                                <div class="member-card">
                                    <div class="avatar-xl member-thumb mb-3 mx-auto d-block">
                                        <img src="{{ auth()->user()->avatar }}" class="rounded-circle img-thumbnail" alt="profile-image">
                                        <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>
                                    </div>

                                    <div class="">
                                        <h5 class="font-18 mb-1">{{ auth()->user()->full_name }}</h5>
                                        <p class="text-muted mb-2">{{ auth()->user()->job_title }}</p>
                                    </div>

                                    <hr/>

                                    <div class="text-left">
                                        <p class="text-muted font-13"><strong>First Name :</strong> <span class="ml-4">{{ auth()->user()->first_name }}</span></p>
                                        <p class="text-muted font-13"><strong>Last Name :</strong> <span class="ml-4">{{ auth()->user()->last_name }}</span></p>
                                        <p class="text-muted font-13"><strong>Mobile :</strong><span class="ml-4">{{ auth()->user()->mobile }}</span></p>
                                        <p class="text-muted font-13"><strong>Username :</strong> <span class="ml-4">{{ auth()->user()->username }}</span></p>
                                        <p class="text-muted font-13"><strong>Email :</strong> <span class="ml-4">{{ auth()->user()->email }}</span></p>
                                    </div>


                                </div>

                            </div>
                        </div>
                        <div class="col-9">
                            <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#general" role="tab">
                                        <span><i class="mdi mdi-information text-info"></i> General Details</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#security" role="tab" >
                                        <span><i class="mdi mdi-lock-alert text-danger"></i> Security</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane show active" id="general" role="tabpanel" >

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form action="{{ url('my-account') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row form-group">
                                            <div class="col-2"><label for="first-name">First Name</label></div>
                                            <div class="col-4">
                                                <input type="text" name="first_name" id="first-name" class="form-control" value="{{ auth()->user()->first_name }}" required>
                                            </div>
                                            <div class="col-2"><label for="last-name">Last Name</label></div>
                                            <div class="col-4">
                                                <input type="text" name="last_name" id="last-name" class="form-control" value="{{ auth()->user()->last_name }}" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-2"><label for="phone">Mobile Number</label></div>
                                            <div class="col-4">
                                                <input type="text" name="mobile" id="phone" class="form-control" value="{{ auth()->user()->mobile }}" required>
                                            </div>
                                            <div class="col-2"><label for="email">Email Address</label></div>
                                            <div class="col-4">
                                                <input type="text" name="email" id="email" class="form-control" value="{{ auth()->user()->email }}" required>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-2"><label for="signature">Signature</label></div>
                                            <div class="col-4">
                                                <input type="file" name="signature" id="signature" class="dropify" data-max-file-size="1M" data-default-file="{{ auth()->user()->user_signature }}" required>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-4 offset-4">
                                                <button type="submit" class="btn btn-sm btn-success"><i class="mdi mdi-update"></i> Update Account info</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="security" role="tabpanel" >
                                    Security
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection