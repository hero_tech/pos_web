@extends('layouts.app', ['title' => 'Create Supplier'])

@section('content')

  <!-- start page title -->
  <div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Supplier management</a></li>
                    <li class="breadcrumb-item active">Create Supplier</li>
                </ol>
            </div>
            <h4 class="page-title">Create suppliers</h4>
        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <register-supplier></register-supplier>
        </div>
    </div>
</div>

@endsection
