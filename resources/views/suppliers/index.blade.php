@extends('layouts.app', ['title' => 'Suppliers'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Supplier management</a></li>
                        <li class="breadcrumb-item active">Suppliers</li>
                    </ol>
                </div>
                <h4 class="page-title">Suppliers</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Location</th>
                            <th>Address</th>
                            <th>Contacts</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($suppliers as $key => $supplier)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $supplier->company }}</td>
                                <td>{{ $supplier->phone }}</td>
                                <td>{{ $supplier->email }}</td>
                                <td>{{ $supplier->location }}</td>
                                <td>{{ $supplier->address }}</td>
                                <td>
                                    <code style="cursor: pointer;" data-toggle="modal" data-target="#view-contacts-{{ $supplier->id }}">
                                        {{ count($supplier->people) }} found
                                    </code>
                                    <div class="modal fade" id="view-contacts-{{ $supplier->id }}" data-backdrop="static" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-full" role="document">
                                            <div class="modal-content">
                                    
                                                <div class="modal-header">
                                                    <h4 class="modal-title" align="center">{{ $supplier->company }} contact persons(s)</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true" style="color:red;">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-bordered table-condensed table-striped" width="100%">
                                                        <thead>
                                                            <th>#</th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>Job Title</th>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($supplier->people as $count => $person)
                                                                <tr>
                                                                    <td>{{ $count+1 }}</td>
                                                                    <td>{{ $person->full_name }}</td>
                                                                    <td>{{ $person->phone }}</td>
                                                                    <td>{{ $person->email }}</td>
                                                                    <td>{{ $person->job_title }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @if($supplier->status)
                                        <span class="btn btn-success btn-rounded btn-xs" style="border-radius:15px">Active</span>
                                    @else
                                        <span class="btn btn-warning btn-rounded btn-xs" style="border-radius:15px">Disabled</span>
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y', strtotime($supplier->created_at)) }}</td>
                                <td>{{ $supplier->description }}</td>
                                <td>
                                    <span class="btn btn-info btn-rounded btn-xs" style="border-radius:15px"  data-toggle="modal" data-target="#edit-supplier-{{ $supplier->id }}"><i class="fa fa-edit"></i> Edit</span>
                                    <span class="btn btn-danger btn-rounded btn-xs"  style="border-radius:15px" data-toggle="modal" data-target="#del-supplier-{{$supplier->id }}"><i class="fa fa-trash"></i> Delete</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
