@extends('layouts.app', ['title' => 'System Settings'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                        <li class="breadcrumb-item active">System Settings</li>
                    </ol>
                </div>
                <h4 class="page-title">System Settings</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <ul class="nav nav-tabs tabs-bordered" role="tablist" id="myTab">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#constants" role="tab">
                            <span><i class="mdi mdi-math-compass"></i> Constants</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane show active" id="constants" role="tabpanel">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <th>#</th>
                                <th>Contant Code</th>
                                <th>Contant Name</th>
                                <th>Contant Value</th>
                                <th>Date Created</th>
                                <th>Edit</th>
                            </thead>
                            <tbody>
                                @foreach ($constants as $key => $constant)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $constant->code }}</td>
                                        <td>{{ $constant->name }}</td>
                                        <td>{{ $constant->value }}</td>
                                        <td>{{ date("d-m-Y", strtotime($constant->created_at)) }}</td>
                                        <td>
                                            <span data-toggle="modal" data-target="#edit-constant-{{ $constant->id }}" class="btn btn-info btn-rounded btn-xs" style="border-radius:15px;cursor:pointer">Edit Constant</span>   

                                            <div class="modal fade" id="edit-constant-{{ $constant->id }}" tabindex="-1"  data-backdrop="static" role="dialog" style="margin-top: 45px">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" align="center">Edit {{ $constant->name }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{ url('system-settings', ['id' => $constant->id]) }}" method="POST">
                                                            @csrf
                                                            @method('PUT')
                                                            <div class="modal-body">
                                                                <div class="row form-group">
                                                                    <div class="col-4">
                                                                        <label for="name">Constant Name</label>
                                                                    </div>
                                                                    <div class="col-8">
                                                                        <input type="text" name="name" id="name" class="form-control" value="{{ $constant->name }}">
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-4">
                                                                        <label for="value">Constant Value</label>
                                                                    </div>
                                                                    <div class="col-8">
                                                                        <input type="number" name="value" id="value" class="form-control" value="{{ $constant->value }}">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-dark waves-effect " data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-success waves-effect">Update Constant</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection