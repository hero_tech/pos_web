<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CarryMore Suppermarket - Guest </title>
    <link rel="shortcut icon" href="{{ asset('assets/images/carrymore_logo.png') }}">
    
    @include('layouts.common.styles')
    
</head>
<body data-layout="horizontal">
    <div id="wrapper">
        <header id="topnav">
            @include('layouts.common.topbar')
        </header>
        <div class="content-page" style="background:white">
            <div class="content">
                <!-- Start Content-->
                <div class="container-fluid">
                    <div class="col-6 offset-3">
                        <img src="{{ asset('images/welcome.jpg') }}">
                    </div>
                    <div class="col-6 offset-3">
                        <p>
                            Hello {{ auth()->user()->first_name }},<br><br>
                            On behalf of all of us, welcome onboard! We believe you will be a terrific asset to our team, and we look forward to your input! <br>
                            Kindly wait as we process your details and you will be notified when the system is set-up for you.<br>
                            <br>
                            Besh wishes.
                        </p>
                    </div>
                </div>
            </div>
            <!-- Footer Start -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <script>document.write(new Date().getFullYear())</script> &copy; System developed by <a href="https://afrinettelecom.com" target="_blank">Afrinet Telecom Ltd</a>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->
        </div>
    </div>
</body>
</html>
