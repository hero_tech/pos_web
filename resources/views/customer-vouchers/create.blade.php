@extends('layouts.app', ['title' => 'Create Customer Vouchers'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Customer Mngmt</a></li>
                        <li class="breadcrumb-item active">Customer Vouchers</li>
                    </ol>
                </div>
                <h4 class="page-title">Create Customer Vouchers</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <form action="{{ url('customer-vouchers') }}" method="post">
                    @csrf
                    <div class="row form-group">
                        <div class="col-2">
                            <label for="">Customer Name</label>
                        </div>
                        <div class="col-4">
                            <input type="text" name="customer_name" class="form-control" required>
                        </div>
                        <div class="col-2">
                            <label for="">Customer Phone</label>
                        </div>
                        <div class="col-4">
                            <input type="number" name="customer_phone" class="form-control" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <label for="">Purchased By</label>
                        </div>
                        <div class="col-4">
                            <input type="text" name="purchased_by" class="form-control" required>
                        </div>
                        <div class="col-2">
                            <label for="">Voucher Amount</label>
                        </div>
                        <div class="col-4">
                            <input type="number" name="voucher_amount" class="form-control" required>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-4 offset-4">
                            <button type="submit" class="btn btn-block btn-sm btn-success">Create Customer Gift Voucher</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection