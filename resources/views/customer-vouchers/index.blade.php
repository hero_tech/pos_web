@extends('layouts.app', ['title' => 'Customer Vouchers'])

@section('content')

     <!-- start page title -->
     <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Customer Mngmt</a></li>
                        <li class="breadcrumb-item active">Customer Vouchers</li>
                    </ol>
                </div>
                <h4 class="page-title">Customer Vouchers</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="form-group">
                    <a href="{{ route('customer-vouchers.create') }}" class="btn btn-success btn-sm" style="border-radius:0px;"><i class="fa fa-gifts"></i> Create New Voucher</a>
                </div>
                <table class="table table-striped table-bordered dt-responsive nowrap datatables_responsive" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Customer</th>
                            <th>Amount</th>
                            <th>Balance</th>
                            <th>Purchased By</th>
                            <th>Purchased At</th>
                            <th>Expires At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($vouchers as $key => $voucher)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $voucher->code }}</td>
                                <td>{{ $voucher->customer_name }}</td>
                                <td>{{ number_format($voucher->voucher_amount, 2) }}</td>
                                <td>{{ number_format($voucher->voucher_balance, 2) }}</td>
                                <td>{{ $voucher->purchased_by }}</td>
                                <td>{{ date("d-M-Y H:m A", strtotime($voucher->created_at)) }}</td>
                                <td>{{ date("d-M-Y H:m A", strtotime($voucher->expires_at)) }}</td>
                                <td>
                                    <a href="" class="btn btn-xs btn-primary"><i class="mdi mdi-more"></i> View More</a>
                                    <a href="" class="btn btn-xs btn-pink"><i class="mdi mdi-download"></i> Download Voucher</a>            
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection