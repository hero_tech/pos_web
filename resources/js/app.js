/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('point-of-sales', require('./components/PointOfSales.vue').default);


Vue.component('create-local-purchase-order', require('./components/CreateLocalPurchaseOrder.vue').default);
Vue.component('local-purchase-order-approvers', require('./components/LocalPurchaseOrderApprovers.vue').default);
Vue.component('approve-lpo', require('./components/ApproveLpo.vue').default);
Vue.component('add-lpo-items', require('./components/AddLpoItems.vue').default);

Vue.component('receive-product', require('./components/ReceiveProduct.vue').default);
Vue.component('register-supplier', require('./components/RegisterSupplier.vue').default);

Vue.component('store-requisition-note-approvers', require('./components/StoreRequisitionNoteApprovers.vue').default);
Vue.component('create-store-requisition-note', require('./components/CreateStoreRequisitionNote.vue').default);
Vue.component('approve-srn', require('./components/ApproveSrn.vue').default);
Vue.component('add-srn-items', require('./components/AddSrnItems.vue').default);

Vue.component('create-contacts', require('./components/CreateContacts.vue').default);
Vue.component('contacts', require('./components/Contacts.vue').default);

Vue.component('create-groups', require('./components/CreateGroups.vue').default);
Vue.component('contact-groups', require('./components/ContactGroups.vue').default);

Vue.component('send-message', require('./components/SendMessage.vue').default);

Vue.component('create-message-schedule', require('./components/CreateMessageSchedule.vue').default);
Vue.component('edit-message-schedule', require('./components/EditMessageSchedule.vue').default);
Vue.component('add-message-schedule-contacts', require('./components/AddMessageScheduleContacts.vue').default);
Vue.component('add-message-schedule-groups', require('./components/AddMessageSchedulegroups.vue').default);


Vue.component('create-custom-message-schedule', require('./components/CreateCustomMessageSchedule.vue').default);
Vue.component('edit-custom-message-schedule', require('./components/EditCustomMessageSchedule.vue').default);
Vue.component('add-custom-schedule-contacts', require('./components/AddCustomScheduleContacts.vue').default);
Vue.component('add-custom-schedule-groups', require('./components/AddCustomScheduleGroups.vue').default);


Vue.component('add-account', require('./components/AddAccount.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});